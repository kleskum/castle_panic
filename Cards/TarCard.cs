﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace CastlePanic
{
    public class TarCard : SpecialAttackCard
    {
        public TarCard(string[] types, Color[] colors)
            : base("Tar", types, colors)
        {
            myDescription = "Place Tar Token on 1 Monster.  That Monster does not" +
                Environment.NewLine + "move this turn.";
        }

        public override bool PlaySpecial(Game game, Monster monster)
        {
            if (CanHit(game, monster))
            {
                monster.Tarred = true;
                return true;
            }
            else
            {
                return false;
            }
        }

        public override Card Clone()
        {
            return new TarCard(Types, Colors);
        }
    }
}
