﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class DrawCardsCard : SpecialCard
    {
        private int numCards;

        public DrawCardsCard(int count) : base("Draw " + count + " Cards")
        {
            numCards = count;
            myDescription = "Play this card to add " + count + " cards to your hand, even" +
                Environment.NewLine + "if it exceeds the normal hand size.";
        }

        public override bool Play(Game game)
        {
            for (int i = 0; i < numCards; i++)
            {
                game.CurrentPlayer.Draw();
            }
            return true;
        }

        public override Card Clone()
        {
            return new DrawCardsCard(numCards);
        }
    }
}
