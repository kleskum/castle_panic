﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public abstract class SpecialCard : Card
    {
        private string myName;

        public SpecialCard(string name)
        {
            myName = name;
        }

        public string Name
        {
            get { return myName; }
        }

        public abstract bool Play(Game game);
    }
}
