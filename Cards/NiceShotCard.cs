﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class NiceShotCard : SpecialCard
    {
        public NiceShotCard()
            : base("Nice Shot")
        {
            myDescription = "Play this card WITH any hit card to slay the hit Monster.";
        }

        public override bool Play(Game game)
        {
            game.SaveCard(this);
            return true;
        }

        public override Card Clone()
        {
            return new NiceShotCard();
        }
    }
}
