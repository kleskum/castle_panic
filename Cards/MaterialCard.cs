﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class MaterialCard : Card
    {
        private string myName;

        public MaterialCard(string name)
        {
            myName = name;
        }

        public string Name
        {
            get { return myName; }
        }

        public virtual bool Play(Game game, Ring ring)
        {            
            // make sure material doesn't already exist
            foreach (Card card in game.SavedCards)
            {
                if (card is MaterialCard)
                {
                    MaterialCard material = card as MaterialCard;
                    if (material.Name == this.Name)
                    {
                        return false;
                    }
                }
            }            

            // first check if all exist
            bool haveAll = true;
            bool haveOne = false;
            foreach (string materialName in game.Configuration.WallMaterials)
            {
                bool haveThis = false;
                foreach (Card card in game.SavedCards)
                {
                    if (card is MaterialCard)
                    {
                        MaterialCard material = card as MaterialCard;
                        if (material.Name == materialName)
                        {
                            haveThis = true;
                            haveOne = true;
                        }
                    }
                }
                if (this.Name == materialName)
                {
                    haveThis = true;
                }
                haveAll = haveAll && haveThis;
            }

            if (haveOne)
            {
                // make sure not null
                if (ring == null)
                {
                    return false;
                }

                // make sure a wall doesn't already exist
                foreach (Token token in ring.Tokens)
                {
                    if (token is Wall)
                    {
                        return false;
                    }
                }                
            }

            // ready to go
            if (haveAll)
            {
                // remove 1 of each
                foreach (string materialName in game.Configuration.WallMaterials)
                {
                    bool removed = false;
                    for (int i = game.SavedCards.Length - 1; i >= 0 && !removed; i--)
                    {
                        Card card = game.SavedCards[i];
                        if (card is MaterialCard)
                        {
                            MaterialCard material = card as MaterialCard;
                            if (material.Name == materialName)
                            {
                                // remove material
                                game.ClearCard(material);
                                removed = true;
                            }
                        }
                    }
                }

                // build wall
                Wall wall = new Wall();
                wall.Place(ring);
            }
            else
            {
                game.SaveCard(this);
            }

            return true;
        }

        public override Card Clone()
        {
            return new MaterialCard(myName);
        }
    }
}
