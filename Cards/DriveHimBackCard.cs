﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace CastlePanic
{
    public class DriveHimBackCard : SpecialAttackCard
    {
        public DriveHimBackCard(string[] types, Color[] colors)
            : base("Drive Him Back!", types, colors)
        {
            myDescription = "Move 1 Monster back into the Forest, keeping it in the same" +
                Environment.NewLine + "numbered arc.";
        }

        public override bool PlaySpecial(Game game, Monster monster)
        {
            if (CanHit(game, monster))
            {
                Arc arc = monster.Ring.Arc;
                monster.Ring.RemoveToken(monster);
                monster.Place(arc.Rings[0]);
                return true;
            }
            else
            {
                return false;
            }
        }

        public override Card Clone()
        {
            return new DriveHimBackCard(Types, Colors);
        }
    }
}
