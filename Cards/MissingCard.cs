﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class MissingCard : SpecialCard
    {
        public MissingCard()
            : base("Missing")
        {
            myDescription = "Do not draw any Monster tokens this turn.";
        }

        public override bool Play(Game game)
        {
            game.SaveCard(this);
            return true;
        }

        public override Card Clone()
        {
            return new MissingCard();
        }
    }
}
