﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace CastlePanic
{
    public abstract class SpecialAttackCard : AttackCard
    {
        private string myName;

        public SpecialAttackCard(string name, string[] types, Color[] colors)
            : base(types, colors)
        {
            myName = name;
        }

        public string Name
        {
            get { return myName; }
        }
       
        public abstract bool PlaySpecial(Game game, Monster monster);
    }
}
