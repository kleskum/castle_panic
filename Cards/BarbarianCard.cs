﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace CastlePanic
{
    public class BarbarianCard : SpecialAttackCard
    {
        public BarbarianCard(string[] types, Color[] colors)
            : base("Barbarian", types, colors)
        {
            myDescription = "Slay 1 Monster anywhere, including the Castle ring," +
                Environment.NewLine + "but not the Forest.";
        }

        public override bool PlaySpecial(Game game, Monster monster)
        {
            if (CanHit(game, monster))
            {
                monster.Slay();
                return true;
            }
            else
            {
                return false;
            }
        }

        public override Card Clone()
        {
            return new BarbarianCard(Types, Colors);
        }
    }
}
