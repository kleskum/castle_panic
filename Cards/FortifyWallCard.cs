﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class FortifyWallCard : MaterialCard
    {
        public FortifyWallCard()
            : base("Fortify Wall")
        {
            myDescription = "Place Fortify token on 1 Wall.  When hit, token" +
                Environment.NewLine + "damages Monsters and stops Boulders.";
        }

        public override bool Play(Game game, Ring ring)
        {
            if (ring == null)
            {
                return false;
            }

            // check for existing wall
            foreach (Token token in ring.Tokens)
            {
                if (token is Wall)
                {
                    // fortify the wall
                    FortifiedWall fortifiedWall = new FortifiedWall();
                    fortifiedWall.Place(ring);
                    return true;
                }
            }
            return false;
        }

        public override Card Clone()
        {
            return new FortifyWallCard();
        }
    }
}
