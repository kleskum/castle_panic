﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class ScavengeCard : SpecialCard
    {
        public ScavengeCard()
            : base("Scavenge")
        {
            myDescription = "Search the discard pile for 1 card of your choice and add" +
                Environment.NewLine + "it to your hand.";
        }

        public override bool Play(Game game)
        {
            if (game.Deck.DiscardCount == 0)
            {
                return false;
            }
           
            // have player choose card from discard
            Card card = game.Input.ChooseCardFromDiscard();
            if (card != null)
            {
                game.CurrentPlayer.Scavenge(card);
                return true;
            }
            else
            {
                return false;
            }            
        }

        public override Card Clone()
        {
            return new ScavengeCard();
        }
    }
}
