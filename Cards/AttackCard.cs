﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace CastlePanic
{
    public class AttackCard : Card
    {
        private List<string> myTypes = new List<string>();
        private List<Color> myColors = new List<Color>();

        public AttackCard(string type, Color color)
            : this(new string[] { type }, new Color[] { color })
        {
        }

        public AttackCard(string type, Color[] colors)
            : this(new string[] { type }, colors)
        {
        }

        public AttackCard(string[] types, Color color)
            : this(types, new Color[] { color })
        {
        }

        public AttackCard(string[] types, Color[] colors)
        {
            foreach (string type in types)
            {
                myTypes.Add(type);
            }
            foreach (Color color in colors)
            {
                myColors.Add(color);
            }
        }

        public string[] Types
        {
            get { return myTypes.ToArray(); }
        }

        public Color[] Colors
        {
            get { return myColors.ToArray(); }
        }

        public bool ContainsType(string type)
        {
            return myTypes.Contains(type);
        }

        public bool ContainsColor(Color color)
        {
            return myColors.Contains(color);
        }

        protected bool CanHit(Game game, Monster monster)
        {
            return myTypes.Contains(monster.Ring.Name) &&
                myColors.Contains(monster.Ring.Arc.Color);
        }

        public bool Play(Game game, Monster monster)
        {
            // check if valid
            if (CanHit(game, monster))
            {
                // check for nice shot
                Card niceShot = null;
                foreach (Card card in game.SavedCards)
                {
                    if (card is NiceShotCard)
                    {
                        niceShot = card;
                    }
                }

                // hit or slay the monster
                int score = 0;
                if (niceShot != null)
                {
                    score = monster.Slay();
                    game.ClearCard(niceShot);
                }
                else
                {
                    score = monster.Hit();                    
                }

                if (monster.Health <= 0)
                {
                    game.Input.AddMessage("Slayed " + monster.Name);
                }
                else
                {
                    game.Input.AddMessage("Hit " + monster.Name);
                }

                // update player's score
                game.CurrentPlayer.Score += score;
                return true;
            }
            else
            {
                return false;
            }
        }

        public override Card Clone()
        {
            return new AttackCard(myTypes.ToArray(), myColors.ToArray());
        }
    }
}
