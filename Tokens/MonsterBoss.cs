﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public abstract class MonsterBoss : Monster
    {
        protected string myDescription = "";

        public MonsterBoss(string name, int health) : base(name, health)
        {
        }

        public abstract void DoAction(Game game);
    }
}
