﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class Healer : MonsterBoss
    {
        public Healer()
            : base("Healer", 2)
        {
            myDescription = "His mysterious potion rejuvenates his allies.  Roll the die," +
                " and place the Healer in the Forest.  All Monsters on the board regain 1" +
                " point of damage.  If a Monster is already at its full health, nothing happens.";
        }

        public override void DoAction(Game game)
        {
            // heal all monsters
            HealEffect effect = new HealEffect(1);
            effect.DoAction(game);
        }

        public override Token Clone()
        {
            return new Healer();
        }
    }
}
