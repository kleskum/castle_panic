﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class ArcMoveEffect : Effect
    {
        private bool clockwise;

        public ArcMoveEffect(bool cw) : base("Monsters Move " + (!cw ? "Counter-" : "") + "Clockwise")
        {
            clockwise = cw;
            myDescription = "All Monsters move 1 space " + (!clockwise ? "counter-" : "") +
                "clockwise but stay in the same ring.  This includes Monsters in the Castle" +
                " and Forest rings.";
        }

        public override void DoAction(Game game)
        {
            List<Token> movedTokens = new List<Token>();

            // loop through arcs
            foreach (Arc arc in game.Board.Arcs)
            {
                // loop through rings
                foreach (Ring ring in arc.Rings)
                {
                    // loop through tokens
                    for (int i = ring.Tokens.Length - 1; i >= 0; i--)
                    {
                        Token token = ring.Tokens[i];

                        // find monsters
                        if (token is Monster && !movedTokens.Contains(token))
                        {
                            // move monster
                            Monster monster = token as Monster;
                            if (clockwise)
                            {
                                monster.Move(MoveDirection.Clockwise);
                            }
                            else
                            {
                                monster.Move(MoveDirection.CounterClockwise);
                            }
                            movedTokens.Add(token);
                        }
                    }
                }
            }
        }

        public override Token Clone()
        {
            return new ArcMoveEffect(clockwise);
        }
    }
}
