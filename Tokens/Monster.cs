﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{    
    public class Monster : Token
    {
        private string myName;
        private int maxHealth;
        private int myHealth;
        private Ring myRing = null;
        private bool imTarred = false;

        public Monster(string name, int health)
        {
            myName = name;
            maxHealth = health;
            myHealth = health;
        }

        public Ring Ring
        {
            get { return myRing; }
        }

        public string Name
        {
            get { return myName; }
        }

        public int Health
        {
            get { return myHealth; }
        }

        public bool Tarred
        {
            get { return imTarred; }
            set { imTarred = value; }
        }

        public void Reset()
        {
            myRing = null;
            Tarred = false;
            myHealth = maxHealth;
        }

        public void Place(Ring ring)
        {
            myRing = ring;
            ring.AddToken(this);
        }

        /// <summary>
        /// Hit the monster and return the score.
        /// </summary>
        /// <returns></returns>
        public int Hit()
        {
            myHealth--;
            if (myHealth == 0)
            {
                // monster died
                Ring.RemoveToken(this);
                myRing = null;

                // return reward
                if (this is MonsterBoss)
                {
                    return 4;
                }
                else
                {
                    return maxHealth;
                }
            }
            return 0;
        }

        public int Slay()
        {
            while (Health > 0)
            {
                Hit();
            }
            if (this is MonsterBoss)
            {
                return 4;
            }
            else
            {
                return maxHealth;
            }
        }

        public void Heal()
        {
            if (myHealth < maxHealth)
            {
                myHealth++;
            }
        }

        public void Move()
        {
            Move(MoveDirection.Normal);
        }        

        public void Move(MoveDirection direction)
        {
            if (imTarred)
            {
                return;
            }
            if (Ring != null)
            {
                Ring nextRing = null;

                // move towards castle
                int ringIndex = Ring.Index;
                int arcIndex = Ring.Arc.Index;

                if (direction != MoveDirection.Normal || ringIndex == Ring.Arc.Rings.Length - 1)
                {
                    // in castle, move around arc                    

                    // check direction
                    if (direction == MoveDirection.CounterClockwise)
                    {
                        if (arcIndex == 0)
                        {
                            // first arc, move back to last
                            nextRing = Ring.Arc.Board.Arcs[Ring.Arc.Board.Arcs.Length - 1].Rings[ringIndex];
                        }
                        else
                        {
                            // move to previous arc
                            nextRing = Ring.Arc.Board.Arcs[arcIndex - 1].Rings[ringIndex];
                        }
                    }
                    else
                    {
                        if (arcIndex == Ring.Arc.Board.Arcs.Length - 1)
                        {
                            // last arc, move back to first
                            nextRing = Ring.Arc.Board.Arcs[0].Rings[ringIndex];
                        }
                        else
                        {
                            // move to next arc
                            nextRing = Ring.Arc.Board.Arcs[arcIndex + 1].Rings[ringIndex];
                        }
                    }
                }
                else
                {
                    // move to next ring
                    nextRing = Ring.Arc.Rings[ringIndex + 1];
                }

                // get next arc index
                int nextArcIndex = nextRing.Arc.Index;

                // check for wall and tower
                Token wall = null;
                Token tower = null;
                foreach (Token token in nextRing.Tokens)
                {
                    // look for wall if moving straight
                    if (token is Wall && arcIndex == nextArcIndex)
                    {
                        wall = token;
                    }
                    // look for tower
                    if (token is Tower)
                    {
                        tower = token;
                    }
                }

                // destroy wall or tower if exists
                if (wall != null)
                {
                    Hit();
                    nextRing.RemoveToken(wall);
                    nextRing = Ring;
                    return;
                }
                else if (tower != null)
                {
                    Hit();
                    nextRing.RemoveToken(tower);
                }

                // move the monster
                if (Ring != null)
                {
                    Ring.RemoveToken(this);
                    nextRing.AddToken(this);
                    myRing = nextRing;
                }
            }
        }

        public override Token Clone()
        {
            return new Monster(myName, maxHealth);
        }
    }
}
