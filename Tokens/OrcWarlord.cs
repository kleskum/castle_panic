﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class OrcWarlord : MonsterBoss
    {
        public OrcWarlord()
            : base("Orc Warlord", 3)
        {
            myDescription = "This fierce warrior leads his troops into battle.  Roll the die," +
                " and place the Orc Warlord in the same color as the Orc Warlord 1 ring" +
                " closer to the Castle or 1 space clockwise if they are in the Castle ring.";
        }

        public override void DoAction(Game game)
        {
            // same as color move effect
            ColorMoveEffect effect = new ColorMoveEffect(Ring.Arc.Color);
            effect.DoAction(game);
        }

        public override Token Clone()
        {
            return new OrcWarlord();
        }
    }
}
