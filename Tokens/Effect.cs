﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public abstract class Effect : Token
    {
        private string myName;
        protected string myDescription;

        public Effect(string name)
        {
            myName = name;
        }

        public string Name
        {
            get { return myName; }
        }

        public abstract void DoAction(Game game);
    }
}
