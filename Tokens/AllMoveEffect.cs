﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class AllMoveEffect : Effect
    {
        private int myMove;

        public AllMoveEffect(int move) : base("All Monsters Move")
        {
            myMove = move;
        }

        public override void DoAction(Game game)
        {
            List<Token> movedTokens = new List<Token>();

            // loop through arcs
            foreach (Arc arc in game.Board.Arcs)
            {
                // loop through rings
                foreach (Ring ring in arc.Rings)
                {
                    // loop through tokens
                    for (int j = ring.Tokens.Length - 1; j >= 0; j--)
                    {
                        Token token = ring.Tokens[j];

                        // find monsters
                        if (token is Monster && !movedTokens.Contains(token))
                        {
                            for (int i = 0; i < myMove; i++)
                            {
                                ((Monster)token).Move();
                                movedTokens.Add(token);
                            }
                        }
                    }
                }                
            }
        }

        public override Token Clone()
        {
            return new AllMoveEffect(myMove);
        }
    }
}
