﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class Wall : Token
    {
        public Wall()
        {
        }

        public override Token Clone()
        {
            return new Wall();
        }
    }
}
