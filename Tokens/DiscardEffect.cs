﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class DiscardEffect : Effect
    {
        private int numCards;

        public DiscardEffect(int count) : base("Discard " + count)
        {
            numCards = count;
        }

        public override void DoAction(Game game)
        {
            // loop through players
            foreach (Player player in game.Players)
            {
                for (int i = 0; i < numCards; i++)
                {                    
                    // make sure player has cards
                    if (player.Hand.Length > 0)
                    {
                        // request card
                        Card card = null;
                        while (card == null)
                        {
                            game.Input.AddMessage("Choose a card to discard.");
                            card = game.Input.RequestCardToDiscard();
                        }

                        // discard
                        player.Discard(card);
                    }
                }
            }
        }

        public override Token Clone()
        {
            return new DiscardEffect(numCards);
        }
    }
}
