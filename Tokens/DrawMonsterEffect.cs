﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class DrawMonsterEffect : Effect
    {
        private int numMonsters;

        public DrawMonsterEffect(int count) : base("Draw " + count + " Monsters")
        {
            numMonsters = count;
        }

        public override void DoAction(Game game)
        {
            for (int i = 0; i < numMonsters; i++)
            {
                game.DrawMonsterToken();
            }
        }

        public override Token Clone()
        {
            return new DrawMonsterEffect(numMonsters);
        }
    }
}
