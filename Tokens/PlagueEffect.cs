﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class PlagueEffect : Effect
    {
        public string myType;

        public PlagueEffect(string type) : base("Plague! " + type)
        {
            myType = type;
            myDescription = "All Players must discard all cards for " + type + " from their hands.";
        }

        public override void DoAction(Game game)
        {
            // loop through players
            foreach (Player player in game.Players)
            {
                // loop through cards
                for (int i = player.Hand.Length - 1; i >= 0; i--)
                {
                    Card card = player.Hand[i];

                    // check for attack cards
                    if (card is AttackCard)
                    {
                        // check for match
                        AttackCard attack = card as AttackCard;
                        if (attack.Types.Length == 1 && attack.ContainsType(myType))
                        {
                            // discard card
                            player.Discard(attack);
                        }
                    }
                }
            }
        }

        public override Token Clone()
        {
            return new PlagueEffect(myType);
        }
    }
}
