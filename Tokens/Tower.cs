﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class Tower : Token
    {
        public override Token Clone()
        {
            return new Tower();
        }
    }
}
