﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class TrollMage : MonsterBoss
    {
        public TrollMage()
            : base("Troll Mage", 3)
        {
            myDescription = "This giant emboldens all Monsters to press the attack.  Roll the" +
                " diek, and place the Troll Mage in the Forest.  Then move all Monsters on the" +
                " board, including the Troll Mage, 1 ring closer to the Castle or 1 space" +
                " clockwise if they are in the Castle ring.";
        }

        public override void DoAction(Game game)
        {
            // move all monsters
            AllMoveEffect effect = new AllMoveEffect(1);
            effect.DoAction(game);
        }

        public override Token Clone()
        {
            return new TrollMage();
        }
    }
}
