﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace CastlePanic
{
    public class ColorMoveEffect : Effect
    {
        private Color myColor;

        public ColorMoveEffect(Color color) : base(color.Name + " Monsters Move 1")
        {
            myColor = color;
            myDescription = "All Monsters in the " + color.Name +
                " arc move 1 ring closer to the castle.";
        }

        public override void DoAction(Game game)
        {
            List<Token> movedTokens = new List<Token>();

            // loop through arcs
            foreach(Arc arc in game.Board.Arcs)
            {
                // match color
                if (arc.Color == myColor)
                {
                    // loop through rings
                    foreach(Ring ring in arc.Rings)
                    {
                        // loop through tokens
                        for (int i = ring.Tokens.Length - 1; i >= 0; i--)
                        {
                            Token token = ring.Tokens[i];

                            // find monsters
                            if (token is Monster && !movedTokens.Contains(token))
                            {
                                ((Monster)token).Move();
                                movedTokens.Add(token);
                            }
                        }
                    }
                }
            }
        }

        public override Token Clone()
        {
            return new ColorMoveEffect(myColor);
        }
    }
}
