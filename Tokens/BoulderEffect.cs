﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class BoulderEffect : Effect
    {
        public BoulderEffect() : base("Boulder")
        {
        }

        public override void DoAction(Game game)
        {
            int arcIndex = game.Die.Roll();
            game.Input.AddMessage("Rolled a " + arcIndex);
            Arc arc = game.Board.Arcs[arcIndex - 1];

            // move downward
            for (int j = 0; j < arc.Rings.Length; j++)
            {
                Ring ring = arc.Rings[j];

                // first look for wall
                for (int i = ring.Tokens.Length - 1; i >= 0; i--)
                {
                    Token token = ring.Tokens[i];
                    if (token is Wall)
                    {
                        ring.RemoveToken(token);
                        game.Input.AddMessage("Boulder destroyed wall at " + 
                            arc.Number);
                        return;
                    }
                }                
                // next look for tower
                for (int i = ring.Tokens.Length - 1; i >= 0; i--)
                {
                    Token token = ring.Tokens[i];
                    if (token is Tower)
                    {
                        ring.RemoveToken(token);
                        game.Input.AddMessage("Boulder destroyed tower at " +
                            arc.Number);
                        return;
                    }
                }
                // now slay monsters
                for (int i = ring.Tokens.Length - 1; i >= 0; i--)
                {
                    Token token = ring.Tokens[i];
                    if (token is Monster)
                    {
                        Monster monster = token as Monster;
                        monster.Slay();
                        game.Input.AddMessage("Boulder slayed " + monster.Name);
                    }
                }
            }

            // uh oh... move across arc
            arcIndex += game.Board.Arcs.Length / 2;
            if (arcIndex > game.Board.Arcs.Length)
            {
                arcIndex -= game.Board.Arcs.Length;
            }
            arc = game.Board.Arcs[arcIndex - 1];

            // move upward
            for (int j = arc.Rings.Length - 1; j >= 0; j--)
            {
                Ring ring = arc.Rings[j];

                // always slay monsters
                for (int i = ring.Tokens.Length - 1; i >= 0; i--)
                {
                    Token token = ring.Tokens[i];
                    if (token is Monster)
                    {
                        Monster monster = token as Monster;
                        monster.Slay();
                        game.Input.AddMessage("Boulder slayed " + monster.Name);
                    }
                }
                // then look for tower
                for (int i = ring.Tokens.Length - 1; i >= 0; i--)
                {
                    Token token = ring.Tokens[i];
                    if (token is Tower)
                    {
                        ring.RemoveToken(token);
                        game.Input.AddMessage("Boulder destroyed tower at " +
                            arc.Number);
                        return;
                    }
                }
                // finally look for wall
                for (int i = ring.Tokens.Length - 1; i >= 0; i--)
                {
                    Token token = ring.Tokens[i];
                    if (token is Wall)
                    {
                        ring.RemoveToken(token);
                        game.Input.AddMessage("Boulder destroyed wall at " +
                            arc.Number);
                        return;
                    }
                }
            }

        }

        public override Token Clone()
        {
            return new BoulderEffect();
        }
    }
}
