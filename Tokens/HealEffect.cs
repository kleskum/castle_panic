﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class HealEffect : Effect
    {
        private int myHeal;

        public HealEffect(int heal) : base("Heal " + heal)
        {
            myHeal = heal;
        }

        public override void DoAction(Game game)
        {
            // loop through arcs
            foreach (Arc arc in game.Board.Arcs)
            {
                // loop through rings
                foreach (Ring ring in arc.Rings)
                {
                    // loop through tokens
                    foreach (Token token in ring.Tokens)
                    {
                        // find monsters
                        if (token is Monster)
                        {
                            for (int i = 0; i < myHeal; i++)
                            {
                                ((Monster)token).Heal();
                            }
                        }
                    }
                }
            }
        }

        public override Token Clone()
        {
            return new HealEffect(myHeal);
        }
    }
}
