﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class GoblinKing : MonsterBoss
    {
        public GoblinKing()
            : base("Goblin King", 2)
        {
            myDescription = "This royal Monster arrives with loyal subjects at his side." +
                "  Roll the die, and place the Goblin King in the Forest.  Then draw and" +
                " and resolve 3 more Monster tokens.";
        }

        public override void DoAction(Game game)
        {
            // same as draw monster effect
            DrawMonsterEffect effect = new DrawMonsterEffect(3);
            effect.DoAction(game);
        }

        public override Token Clone()
        {
            return new GoblinKing();
        }
    }
}
