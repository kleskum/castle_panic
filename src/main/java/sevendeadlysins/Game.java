package sevendeadlysins;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Game {

    public enum Role {
        Greed,
        Pride,
        Lust,
        Wrath,
        Envy,
        Sloth,
        Gluttony
    }

    public static class Player {
        public int food = 7;
        public int money = 7;
        public int greed = 0;
        public int pride = 0;
        public int lust = 0;
        public int wrath = 0;
        public int envy = 0;
        public int sloth = 0;
        public int gluttony = 0;
        public int score =0;
    }

    private Random r = new Random();
    private int player = 0;
    private boolean clockwise = true;
    private Player[] players = new Player[] { new Player(), new Player(), new Player() };
    private List<Role> roles = new ArrayList<Role>();

    public static void main(String[] args) {
        System.out.println("Go");
        new Game().run();
        System.out.println("Done");
    }

    private void mostFood() {
        int max = 0;
        for(Player player : players) {
            max = Math.max(max, player.food);
        }
        for(Player player : players) {
            player.score += player.food == max ? 3 : 0;
        }
    }

    private void mostMoney() {
        int max = 0;
        for(Player player : players) {
            max = Math.max(max, player.money);
        }
        for(Player player : players) {
            player.score += player.money == max ? 3 : 0;
        }
    }

    private void leastResources() {
        int min = Integer.MAX_VALUE;
        for(Player player : players) {
            min = Math.min(min, player.money + player.food);
        }
        for(Player player : players) {
            player.score += player.money + player.food == min ? 5 : 0;
        }
    }

    private void mostOfRole(Role role) {
        int max = 0;
        for(Player player : players) {
            switch(role) {
                case Greed:
                    max = Math.max(max, player.greed);
                    break;
                case Pride:
                    max = Math.max(max, player.pride);
                    break;
                case Lust:
                    max = Math.max(max, player.lust);
                    break;
                case Wrath:
                    max = Math.max(max, player.wrath);
                    break;
                case Envy:
                    max = Math.max(max, player.envy);
                    break;
                case Sloth:
                    max = Math.max(max, player.sloth);
                    break;
                case Gluttony:
                    max = Math.max(max, player.gluttony);
                    break;
            }
        }
        for(Player player : players) {
            switch(role) {
                case Greed:
                    player.score += player.greed == max ? 1 : 0;
                    break;
                case Pride:
                    player.score += player.pride == max ? 1 : 0;
                    break;
                case Lust:
                    player.score += player.lust == max ? 1 : 0;
                    break;
                case Wrath:
                    player.score += player.wrath == max ? 1 : 0;
                    break;
                case Envy:
                    player.score += player.envy == max ? 1 : 0;
                    break;
                case Sloth:
                    player.score += player.sloth == max ? 1 : 0;
                    break;
                case Gluttony:
                    player.score += player.gluttony == max ? 1 : 0;
                    break;
            }
        }
    }

    private void run() {
        // seven rounds
        for(int i = 0; i < 7; i++) {
            // seven roles for the round
            for(Role role : Role.values()) {
                roles.add(role);
            }

            // until all roles taken
            while(roles.size() > 0) {
                // player takes a random action
                Role role = roles.get(r.nextInt(roles.size()));
                doRole(role);
                switch(role) {
                    case Greed:
                        players[player].greed++;
                        break;
                    case Pride:
                        players[player].pride++;
                        break;
                    case Lust:
                        players[player].lust++;
                        break;
                    case Wrath:
                        players[player].wrath++;
                        break;
                    case Envy:
                        players[player].envy++;
                        break;
                    case Sloth:
                        players[player].sloth++;
                        break;
                    case Gluttony:
                        players[player].gluttony++;
                        break;
                }

                roles.remove(role);

                if(roles.size() > 0) {
                    if (clockwise) {
                        player++;
                        if (player >= players.length) {
                            player = 0;
                        }
                    } else {
                        player--;
                        if (player < 0) {
                            player = players.length - 1;
                        }
                    }
                }
            }
            clockwise = !clockwise;
        }

        for(Role role : Role.values()) {
            mostOfRole(role);
        }
        mostFood();
        mostMoney();
        leastResources();

        int total = 0;
        for(int i = 0; i < players.length; i++) {
            System.out.println("Player " + i + ":");
            System.out.println("    Food: " + players[i].food);
            System.out.println("   Money: " + players[i].money);
            System.out.println("   Greed: " + players[i].greed);
            System.out.println("    Lust: " + players[i].lust);
            System.out.println("   Pride: " + players[i].pride);
            System.out.println("   Wrath: " + players[i].wrath);
            System.out.println("   Sloth: " + players[i].sloth);
            System.out.println("    Envy: " + players[i].envy);
            System.out.println("   Glutn: " + players[i].gluttony);
            System.out.println("   Score: " + players[i].score);

            total += players[i].food;
            total += players[i].money;
        }
        System.out.println("Total: " + total);
    }

    private int chooseOtherPlayer() {
        int p = player;
        while(p == player) {
            p = r.nextInt(players.length);
        }
        return p;
    }

    private void doRole(Role role) {
        switch(role) {
            case Greed:
                players[player].money++;
                break;
            case Pride:
                // choose a random role (not pride)
                Role pride = role;
                while(pride == Role.Pride) {
                    pride = Role.values()[r.nextInt(Role.values().length)];
                }
                doRole(pride);
                break;
            case Lust:
                boolean lust = r.nextInt() % 2 == 0;
                if(lust) {
                    players[player].money--;
                    players[chooseOtherPlayer()].money++;
                } else {
                    players[player].food--;
                    players[chooseOtherPlayer()].food++;
                }
                break;
            case Wrath:
                int p = chooseOtherPlayer();
                for(int i = 0; i < 2; i++) {
                    if (r.nextInt() % 2 == 0) {
                        players[p].money--;
                    } else {
                        players[p].food--;
                    }
                }
                break;
            case Envy:
                boolean envy = r.nextInt() % 2 == 0;
                if(envy) {
                    players[player].money++;
                    players[chooseOtherPlayer()].money--;
                } else {
                    players[player].food++;
                    players[chooseOtherPlayer()].food--;
                }
                break;
            case Sloth:
                // nothing
                break;
            case Gluttony:
                players[player].food++;
                break;
        }
        roles.remove(role);
    }
}
