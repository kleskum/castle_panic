package pippark;

public class FormView implements View {
    private final GameForm myForm;

    public FormView(GameForm form) {
        this.myForm = form;
    }

    public void reset() {
        myForm.setSelectedMeeple(null);
        myForm.setSelectedRide(null);
    }

    public void addMessage(String msg) {
        synchronized (this) {
            myForm.addMessage(msg);
            myForm.repaint();
        }
    }
}
