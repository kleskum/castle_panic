package pippark;

import javafx.util.Pair;
import pippark.action.Action;
import pippark.action.LeaveParkAction;
import pippark.action.MoveMeepleAction;
import pippark.mat.Meeple;
import pippark.ride.Ride;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CpuInput implements Input {

    private Game game;

    public CpuInput(Game game) {
        this.game = game;
    }

    private Action lastAction = null;

    private Action getBestAction() {
        List<Action> actions = game.getAvailableActions();
        Map<Action, Integer> choices = new HashMap<Action, Integer>();

//        Action bestAction = null;
        int bestScore = 0;
//        int time = Integer.MAX_VALUE;

        for(Action action : actions) {
            Game branch = new Game(game.getGameState().deepCopy());
            recurseAction(branch, action, action, new ArrayList<Action>(), choices, 0);
//            action.execute(branch);
//            int score = branch.getBestScore(branch.getGameState().adult2, branch.getGameState().child2, branch.getGameState().objectives2);
//            int time1 = branch.getGameState().adult1.getDiceTotal();
//            int time2 = branch.getGameState().child2.getDiceTotal();
//            if(score > bestScore && time1+time2 < time) {
//                bestScore = score;
//                bestAction = action;
//                time = time1+time2;
//            }
//            break;
        }
//        return bestAction;

        bestScore = Integer.MIN_VALUE;
        Action bestAction = null;
        for(Action a : choices.keySet()) {
            if(choices.get(a) > bestScore) {
                bestAction = a;
                bestScore = choices.get(a);
            }
        }
        return bestAction;
    }

    public Meeple chooseMeeple(List<Meeple> options) {
        if(options.size() == 0) {
            return options.get(0);
        }
        lastAction = getBestAction();
        if(lastAction instanceof MoveMeepleAction) {
            return ((MoveMeepleAction)lastAction).getMeeple();
        } else if(lastAction instanceof LeaveParkAction) {
            return ((LeaveParkAction)lastAction).getMeeple();
        }
        return null;
    }

    public Ride chooseRide(Meeple meeple, List<Ride> options) {
        Action action = null;
        if(options.size() == 0) {
            return options.get(0);
        } else if(lastAction != null) {
            action = lastAction;
        } else {
            action = getBestAction();
        }
        if(action instanceof MoveMeepleAction) {
            Ride ride = ((MoveMeepleAction)action).getRide();
            lastAction = null;
            return ride;
        } else {
            return null;
        }
    }

    private void recurseAction(Game g, Action firstAction, Action action, List<Action> route, Map<Action, Integer> choices, int depth) {
//        if(choices.size() > 10000) {
            // that's enough
//            return;
//        }

        action.execute(g);
        route.add(action);

        List<Action> actions = g.getAvailableActions();

        if(actions.size() == 0 || depth > 10) {
            // maximize score
            int score1 = g.getBestScore(g.getGameState().adult1, g.getGameState().child1, g.getGameState().objectives1);
            int score2 = g.getBestScore(g.getGameState().adult2, g.getGameState().child2, g.getGameState().objectives2);
            int newScore = score2-score1;
            if(!choices.containsKey(firstAction) || newScore > choices.get(firstAction)) {
                choices.put(action, newScore);
            }
//            choices.add(new Pair<Integer, Action[]>(score2-score1, route.toArray(new Action[0])));
            return;
        } else {
            for (Action a : actions) {
                Game branch = new Game(g.getGameState().deepCopy());
                recurseAction(branch, firstAction, a, new ArrayList<Action>(route), choices, depth+1);
            }
        }
    }
}
