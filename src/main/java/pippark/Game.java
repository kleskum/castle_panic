package pippark;

import pippark.action.Action;
import pippark.action.LeaveParkAction;
import pippark.action.MoveMeepleAction;
import pippark.dice.Die;
import pippark.dice.DieColor;
import pippark.mat.*;
import pippark.objective.Objective;
import pippark.ride.Ride;
import pippark.ride.RideType;

import java.awt.*;
import java.util.*;
import java.util.List;

public class Game {
    static final int MAX_TIME = 16;

    private GameState gameState;
    private View view;
    private Map<PlayerColor, Input> input = new HashMap<PlayerColor, Input>();

    public Game(View view) {
        this.gameState = new GameState();
        this.view = view;
    }

    public void setInput(Input input1, Input input2) {
        input.put(PlayerColor.Red, input1);
        input.put(PlayerColor.Blue, input2);
    }

    public Game(GameState state) {
        this(new HiddenView());
        this.gameState = state;
    }

    public GameState getGameState() {
        return gameState;
    }

    public View getView() {
        return view;
    }

    public void setup() {
        List<Ride> possibleRides = new ArrayList<Ride>();

        for(DieColor dieColor : DieColor.values()) {
            possibleRides.add(new Ride(RideType.Kid, dieColor));
            possibleRides.add(new Ride(RideType.Adult, dieColor));
            possibleRides.add(new Ride(RideType.Family, dieColor));
            possibleRides.add(new Ride(RideType.Family, dieColor));
        }
        Collections.shuffle(possibleRides);

        // deal out 9 rides
        for(int r = 0; r < 3; r++) {
            for(int c = 0; c < 3; c++) {
                Ride ride = possibleRides.remove(0);
                ride.place(r, c);
                gameState.rides.add(ride);
            }
        }

        List<Objective> possibleObjectives = new ArrayList<Objective>();
        for(int i = 0; i <  3; i++){
            int adultCount = 2;
            int kidCount = 2;
            if(i == 1) {
                kidCount = 1;
            } else if(i == 2) {
                adultCount = 1;
            }
            possibleObjectives.add(new Objective(adultCount, DieColor.Red, kidCount, DieColor.Blue));
            possibleObjectives.add(new Objective(adultCount, DieColor.Red, kidCount, DieColor.Yelow));
            possibleObjectives.add(new Objective(adultCount, DieColor.Red, kidCount, DieColor.Green));
            possibleObjectives.add(new Objective(adultCount, DieColor.Blue, kidCount, DieColor.Red));
            possibleObjectives.add(new Objective(adultCount, DieColor.Blue, kidCount, DieColor.Yelow));
            possibleObjectives.add(new Objective(adultCount, DieColor.Blue, kidCount, DieColor.Green));
            possibleObjectives.add(new Objective(adultCount, DieColor.Yelow, kidCount, DieColor.Red));
            possibleObjectives.add(new Objective(adultCount, DieColor.Yelow, kidCount, DieColor.Blue));
            possibleObjectives.add(new Objective(adultCount, DieColor.Yelow, kidCount, DieColor.Green));
            possibleObjectives.add(new Objective(adultCount, DieColor.Green, kidCount, DieColor.Red));
            possibleObjectives.add(new Objective(adultCount, DieColor.Green, kidCount, DieColor.Yelow));
            possibleObjectives.add(new Objective(adultCount, DieColor.Green, kidCount, DieColor.Blue));
        }

        Collections.shuffle(possibleObjectives);
        for(int i = 0; i < 3; i++) {
            gameState.objectives1.add(possibleObjectives.remove(0));
            gameState.objectives2.add(possibleObjectives.remove(0));
        }
    }

    public List<Ride> getRides() {
        return gameState.rides;
    }

    public List<Meeple> getAllMeeples() {
        List<Meeple> meeples = new ArrayList<Meeple>();
        meeples.add(gameState.adult1);
        meeples.add(gameState.child1);
        meeples.add(gameState.adult2);
        meeples.add(gameState.child2);
        return meeples;
    }

    public boolean gameOver() {
        return false;
    }

    public boolean roundOver() {
        return getNextActiveMeeple().size() == 0;
    }

    public void playRound() {
        while(!roundOver()) {
            view.reset();

            // get the next possible meeple
            final ActiveMeeple activeMeeple = getNextActiveMeeple();

            // choose a meeple
            Meeple nextMeeple;
            if(activeMeeple.size() == 1) {
                nextMeeple = activeMeeple.get(0);
            } else {
                nextMeeple = input.get(activeMeeple.playerColor()).chooseMeeple(activeMeeple);
            }
            view.addMessage("Selected: " + nextMeeple.getColor() + " " + nextMeeple.getMeepleType().name());

            List<Ride> possibleRides = getPossibleRides(nextMeeple);
            if (possibleRides.size() > 0) {
                // choose a ride for the meeple and move there
                Ride ride = input.get(nextMeeple.getColor()).chooseRide(nextMeeple, possibleRides);
                new MoveMeepleAction(nextMeeple, ride, activeMeeple.usesTieBreaker()).execute(this);
            } else {
                // must leave the park
                new LeaveParkAction(nextMeeple).execute(this);
            }

            gameState.score1 = getBestScore(gameState.adult1, gameState.child1, gameState.objectives1);
            gameState.score2 = getBestScore(gameState.adult2, gameState.child2, gameState.objectives2);
        }
    }

    public List<Action> getAvailableActions() {
        List<Action> actions = new ArrayList<Action>();
        ActiveMeeple activeMeeple = getNextActiveMeeple();
        for(Meeple meeple : activeMeeple) {
            List<Ride> possibleRides = getPossibleRides(meeple);
            if(possibleRides.size() == 0) {
                actions.add(new LeaveParkAction(meeple));
            } else {
                for (Ride ride : possibleRides) {
                    actions.add(new MoveMeepleAction(meeple, ride, activeMeeple.usesTieBreaker()));
                }
            }
        }
        return actions;
    }

    public void playGame() {
        // set up the game
        setup();

        // play until game over
        while (!gameOver()) {
            playRound();

            return;

            // score the round;
//            for (Meeple meeple : getAllMeeples()) {
//                meeple.reset();
//            }
//            for (Ride ride : gameState.rides) {
//                ride.reset();
//            }
        }
    }

    public ActiveMeeple getNextActiveMeeple() {
        // first find all meeple with the lowest time
        int min = Integer.MAX_VALUE;
        ActiveMeeple next = new ActiveMeeple();
        Set<Color> colors = new HashSet<Color>();
        for(Meeple meeple : getAllMeeples()) {
            final int meepleValue = meeple.getDiceTotal();
            // make sure the meeple hasn't left the park
            if(meepleValue == 0 || meeple.getRide() != null) {
                if (meepleValue < min) {
                    next.clear();
                    colors.clear();
                    min = meepleValue;
                    next.add(meeple);
                    colors.add(meeple.getColor().color);
                } else if (meepleValue == min) {
                    next.add(meeple);
                    colors.add(meeple.getColor().color);
                }
            }
        }

        // check for tie breaker
        if(colors.size() > 1) {
            // remove meeples not of tie breaker color
            for(int i = next.size()-1; i >= 0; i--) {
                if(!next.get(i).getColor().equals(gameState.tieBreaker)) {
                    next.remove(i);
                }
            }
            next.setTieBreaker(true);
        }

        return next;
    }

    public boolean hasPenalty(Ride ride1, Ride ride2) {
        if(ride1 == null || ride2 == null) {
            return false;
        }
        return Math.abs(ride1.getColumn() - ride2.getColumn()) > 1 || Math.abs(ride1.getRow() - ride2.getRow()) > 1;
    }

    public List<Ride> getPossibleRides(Meeple meeple) {
        List<Ride> possibleRides = new ArrayList<Ride>();
        if(meeple != null) {
            final int meepleTime = meeple.getDiceTotal();
            for (Ride ride : gameState.rides) {
                if (ride.getDice().length > 0) {
                    int penalty = hasPenalty(ride, meeple.getRide()) ? 1 : 0;
                    int nextDie = ride.getDice()[0].face();
                    if (meepleTime + Math.min(6, penalty + nextDie) <= MAX_TIME) {
                        if(ride.getRideType() == RideType.Family ||
                                (ride.getRideType() == RideType.Adult && meeple.isAdult()) ||
                                (ride.getRideType() == RideType.Kid && !meeple.isAdult())) {
                            possibleRides.add(ride);
                        }
                    }
                }
            }
            // remove occupied rides
            for(Meeple m : getAllMeeples()) {
                possibleRides.remove(m.getRide());
            }
        }
        return possibleRides;
    }

    public void moveTieBreaker() {
        if(gameState.tieBreaker.equals(PlayerColor.Blue)) {
            gameState.tieBreaker = PlayerColor.Red;
        } else {
            gameState.tieBreaker = PlayerColor.Blue;
        }
    }

    public Meeple getMeeple(Meeple meeple) {
        for(Meeple m : getAllMeeples()) {
            if(m.equals(meeple)) {
                return m;
            }
        }
        return null;
    }

    public Ride getRide(Ride ride) {
        for(Ride r : getRides()) {
            if(r.equals(ride)) {
                return r;
            }
        }
        return null;
    }

    public int getBestScore(Meeple adult, Meeple child, List<Objective> objectives) {
        // find all arrangement of dice

        List<Objective> options = new ArrayList<Objective>(objectives);
        options.add(new Objective(0, null, 0, null));
        List<List<Die>> adultPlacement = new ArrayList<List<Die>>();
        List<List<Die>> childPlacement = new ArrayList<List<Die>>();
        for(Objective option : options) {
            adultPlacement.add(new ArrayList<Die>());
            childPlacement.add(new ArrayList<Die>());
        }
        BestScore best = new BestScore();
        dfs(true, adult, child, new ArrayList<Die>(adult.getDice()), options, adultPlacement, childPlacement, best);
        return best.score;
    }

    private static class BestScore {
        public int score = 0;
    }

    private void dfs(boolean isAdult, Meeple adult, Meeple child, List<Die> dice, List<Objective> options,
                     List<List<Die>> adultPlacmeent, List<List<Die>> childPlacement, BestScore best) {
        if(dice.size() == 0) {
            if(isAdult) {
                dfs(!isAdult, adult, child, child.getDice(), options, adultPlacmeent, childPlacement, best);
            } else {
                // what is your score?
                int score = 0;
                for(int i = 0; i < options.size(); i++) {
                    Objective objective = options.get(i);
                    boolean hasAdultObjective = has(adultPlacmeent.get(i), objective.adultDice);
                    boolean hasChildObjective = has(childPlacement.get(i), objective.kidDice);

                    if(hasAdultObjective) {
                        score += objective.adultDice.size() == 2 ? 5 : 2;
                    }
                    if(hasChildObjective) {
                        score += objective.kidDice.size() == 2 ? 5 : 2;
                    }
                    if(hasAdultObjective && hasChildObjective) {
                        score++;
                    }
                }

                int remaining = adultPlacmeent.get(3).size() + childPlacement.get(3).size();
                if(remaining >= 5) {
                    score += 6;
                } else if(remaining >= 3) {
                    score += 3;
                } else if(remaining >= 1) {
                    score += 1;
                }

                best.score = Math.max(score, best.score);
                return;
            }
        } else {
            for(int x = 0; x < dice.size(); x++) {
                Die die = dice.get(x);
                for(int i = 0; i < options.size(); i++) {
                    Objective option = options.get(i);
                    if(isAdult) {
                        if(option.adultDice.contains(die.getColor()) || option.adultDice.size() == 0) {
                            adultPlacmeent.get(i).add(die);
                            dfs(isAdult, adult, child, dice.subList(x+1, dice.size()), options, adultPlacmeent, childPlacement, best);
                            adultPlacmeent.get(i).remove(die);
                        }
                    } else {
                        if(option.kidDice.contains(die.getColor()) || option.kidDice.size() == 0) {
                            childPlacement.get(i).add(die);
                            dfs(isAdult, adult, child, dice.subList(x+1, dice.size()), options, adultPlacmeent, childPlacement, best);
                            childPlacement.get(i).remove(die);
                        }
                    }
                }
            }
        }
    }

    private boolean has(List<Die> dice, List<DieColor> objectiveDice) {
        if(objectiveDice.size() == 0 || dice.size() == 0) {
            return false;
        }
        List<DieColor> copy = new ArrayList<DieColor>(objectiveDice);
        for(Die die : dice) {
            copy.remove(die.getColor());
        }
        return copy.size() == 0;
    }

}
