package pippark;

import pippark.mat.Meeple;
import pippark.ride.Ride;

import java.util.List;

public class FormInput implements Input {
    private final GameForm myForm;

    public FormInput(GameForm form) {
        this.myForm = form;
    }

    public void reset() {
        myForm.setSelectedMeeple(null);
        myForm.setSelectedRide(null);
    }

    public Meeple chooseMeeple(List<Meeple> options) {
        synchronized (myForm) {
            try {
                while(myForm.getSelectedMeeple() == null && !options.contains(myForm.getSelectedMeeple())) {
                    myForm.wait();
                }
                return myForm.getSelectedMeeple();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public Ride chooseRide(Meeple meeple, List<Ride> options) {
        synchronized (myForm) {
            try {
                myForm.setSelectedMeeple(meeple);
                while (myForm.getSelectedRide() == null && !options.contains(myForm.getSelectedRide())) {
                    myForm.wait();
                }
                return myForm.getSelectedRide();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }
}
