package pippark;

import castlepanic.Renderer;
import pippark.dice.Die;
import pippark.dice.DieColor;
import pippark.mat.ActiveMeeple;
import pippark.mat.Meeple;
import pippark.mat.MeepleType;
import pippark.mat.PlayerColor;
import pippark.objective.Objective;
import pippark.ride.Ride;

import javax.swing.*;
import java.awt.*;
import java.util.Collections;

public class PanelRenderer extends JPanel implements Renderer {

    private GameForm gameForm;

    public PanelRenderer(GameForm gameForm) {
        this.gameForm = gameForm;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (gameForm.Game != null) {
            int i = 0;
            for(int r = 0; r < 3; r++) {
                for(int c = 0; c < 3; c++) {
                    Ride ride = gameForm.Game.getRides().get(i);
                    Rectangle rideBounds = getRideBounds(ride);

                    g.setColor(Color.white);
                    g.fillRect(rideBounds.x, rideBounds.y, rideBounds.width, rideBounds.height);

                    g.setColor(Color.black);
                    g.drawRect(rideBounds.x, rideBounds.y, rideBounds.width, rideBounds.height);


                    Font font = new Font("Arial", Font.PLAIN, 16);
                    {
                        g.setFont(font);
                        g.setColor(Color.black);
                        g.drawString(ride.getRideType().name(), 60 + c*300, 70 + r*175);
                    }

                    for(int d = 0; d < ride.getDice().length; d++) {
                        Die die = ride.getDice()[d];
                        g.setColor(die.getColor().color);
                        g.fillRect(60 + c*300 + 10 + d*60, 80 + r*175 + 10, 50, 50);
                        g.setColor(Color.black);
                        g.drawRect(60 + c*300 + 10 + d*60, 80 + r*175 + 10, 50, 50);

                        g.drawString(Integer.toString(die.face()), 60 + c*300 + 30 + d*60, 90 + r*175 + 30);
                    }
                    i++;
                }
            }
        }

        g.setColor(Color.white);
        g.fillRect(50, 575, 410, 210);

        g.setColor(Color.black);
        g.drawRect(50, 575, 410, 210);
        g.drawLine(50, 680, 460, 680);

        g.setColor(Color.white);
        g.fillRect(515, 575, 410, 210);

        g.setColor(Color.black);
        g.drawRect(515, 575, 410, 210);
        g.drawLine(515, 680, 925, 680);

        Rectangle r;
        for(int j = 0; j < 16; j++) {
            r = getValueBounds(PlayerColor.Red, MeepleType.Adult, j+1);
            g.drawRect(r.x, r.y, r.width, r.height);
            g.drawString(Integer.toString(j+1), r.x + 15, r.y + 20);

            r = getValueBounds(PlayerColor.Red, MeepleType.Child, j+1);
            g.drawRect(r.x, r.y, r.width, r.height);
            g.drawString(Integer.toString(j+1), r.x + 15, r.y + 20);

            r = getValueBounds(PlayerColor.Blue, MeepleType.Adult, j+1);
            g.drawRect(r.x, r.y, r.width, r.height);
            g.drawString(Integer.toString(j+1), r.x + 15, r.y + 20);

            r = getValueBounds(PlayerColor.Blue, MeepleType.Child, j+1);
            g.drawRect(r.x, r.y, r.width, r.height);
            g.drawString(Integer.toString(j+1), r.x + 15, r.y + 20);
        }

        for(Meeple meeple : gameForm.Game.getAllMeeples()) {
            int runningTotal = 0;
            for (Die die : meeple.getDice()) {
                runningTotal += die.face();
                r = getValueBounds(meeple.getColor(), meeple.getMeepleType(), runningTotal);
                g.setColor(die.getColor().color);
                g.fillRect(r.x, r.y, r.width, r.height);
            }
        }

        ActiveMeeple activeMeeple = gameForm.Game.getNextActiveMeeple();
        for(Meeple meeple : gameForm.Game.getAllMeeples()) {

            Rectangle bounds = getMeepleBounds(meeple);

            g.setColor(meeple.getColor().color);
            g.fillOval(bounds.x, bounds.y, bounds.width, bounds.height);

            if(activeMeeple.contains(meeple)) {
                g.setColor(gameForm.getSelectedMeeple() == meeple ? Color.yellow : Color.black);
                ((Graphics2D)g).setStroke(new BasicStroke(10));
                g.drawOval(bounds.x, bounds.y, bounds.width, bounds.height);
                ((Graphics2D)g).setStroke(new BasicStroke(1));
            }
        }

        if(gameForm.getSelectedMeeple() != null) {
            for (Ride ride : gameForm.Game.getPossibleRides(gameForm.getSelectedMeeple())) {
                Rectangle rideBounds = getRideBounds(ride);
                ((Graphics2D)g).setStroke(new BasicStroke(10));
                g.setColor(ride == gameForm.getSelectedRide() ? Color.yellow : Color.black);
                g.drawRect(rideBounds.x, rideBounds.y, rideBounds.width, rideBounds.height);
                ((Graphics2D)g).setStroke(new BasicStroke(1));
            }
        }

        int x = 5;
        for(Objective objective : gameForm.Game.getGameState().objectives1) {
            g.setColor(Color.white);
            g.fillRect(x, 10,   120, 30);
            g.setColor(Color.black);
            g.drawRect(x, 10,   120, 30);
            g.setColor(Color.gray);
            g.drawLine(x + 60, 10, x+60, 40);

            for(int i = 0; i < objective.adultDice.size(); i++) {
                DieColor die = objective.adultDice.get(i);
                g.setColor(die.color);
                g.fillRect(x + 5 + i*30, 15, 20, 20);
            }
            for(int i = 0; i < objective.kidDice.size(); i++) {
                DieColor die = objective.kidDice.get(i);
                g.setColor(die.color);
                g.fillRect(x + 65 + i*30, 15, 20, 20);
            }
            x += 125;
        }
        g.setColor(Color.black);
        g.drawString(Integer.toString(gameForm.Game.getGameState().score1), x + 10, 25);

        x = 540;
        for(Objective objective : gameForm.Game.getGameState().objectives2) {
            g.setColor(Color.white);
            g.fillRect(x, 10,   120, 30);
            g.setColor(Color.black);
            g.drawRect(x, 10,   120, 30);
            g.setColor(Color.gray);
            g.drawLine(x + 60, 10, x+60, 40);

            for(int i = 0; i < objective.adultDice.size(); i++) {
                DieColor die = objective.adultDice.get(i);
                g.setColor(die.color);
                g.fillRect(x + 5 + i*30, 15, 20, 20);
            }
            for(int i = 0; i < objective.kidDice.size(); i++) {
                DieColor die = objective.kidDice.get(i);
                g.setColor(die.color);
                g.fillRect(x + 65 + i*30, 15, 20, 20);
            }
            x += 125;
        }
        g.setColor(Color.black);
        g.drawString(Integer.toString(gameForm.Game.getGameState().score2), x + 10, 25);
    }

    public Rectangle getValueBounds(PlayerColor color, MeepleType type, int value) {
        int y = type == MeepleType.Child ? 100 : 0;
        return new Rectangle((color == PlayerColor.Red ? 60 : 525) + (value-1) % 8 * 50, value > 8 ? 635+y : 585+y, 40, 40);
    }

    public Rectangle getRideBounds(Ride ride) {
        int index = gameForm.Game.getRides().indexOf(ride);
        int r = index / 3;
        int c = index - (r * 3);
        return new Rectangle(50 + c*300, 50 + r*175, 275, 150);
    }

    public Rectangle getMeepleBounds(Meeple meeple) {
        int x = 5;
        int y = 0;
        int sz = meeple.getMeepleType() == MeepleType.Adult ? 50 : 30;

        if(meeple.getRide() == null) {
            // draw on mat
            if(meeple.getColor() == PlayerColor.Blue) {
                x += 465;
            }
            y = meeple.getMeepleType() == MeepleType.Adult ? 600 : 710;
        } else {
            Rectangle rideBounds = getRideBounds(meeple.getRide());
            x = rideBounds.x + 50;
            y = rideBounds.y + 100;
        }
        return new Rectangle(x, y, sz, sz);
    }
}
