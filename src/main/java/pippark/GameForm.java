package pippark;


import castlepanic.component.Token;
import pippark.mat.Meeple;
import pippark.ride.Ride;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class GameForm extends JFrame implements MouseListener, MouseMotionListener, Runnable {
    private boolean isRunning = true;
    private int fps = 30;
    private int windowWidth = 1200;
    private int windowHeight = 800;
    private Insets insets;

    public Game Game = null;

    private boolean selectNothing = false;
    private Meeple selectedMeeple = null;
    private Ride selectedRide = null;

    private PanelRenderer panelRenderer1;
    private JEditorPane editorPane;
    private JScrollPane editorScrollPane;

    private JToolTip tip = new JToolTip();
    private Popup popup = null;

    private boolean startGame = false;

    public GameForm() {
        Game = new Game(new FormView(this));
        Game.setInput(new FormInput(this), new CpuInput(Game));

        panelRenderer1 = new PanelRenderer(this);
        this.setLayout(new BorderLayout());
        this.add(panelRenderer1, BorderLayout.CENTER);
        editorPane = new JEditorPane();
        editorPane.setEditable(false);
        editorScrollPane = new JScrollPane(editorPane);
        editorScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        editorScrollPane.setPreferredSize(new Dimension(250, 145));
        editorScrollPane.setMinimumSize(new Dimension(10, 10));
        this.add(editorScrollPane, BorderLayout.EAST);
        this.pack();

        Thread thread = new Thread(this);
        thread.start();
    }

    public void start() {
        setTitle("Pip Park");
        setSize(windowWidth, windowHeight);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);

        insets = getInsets();
        setSize(insets.left + windowWidth + insets.right, insets.top + windowHeight + insets.bottom);

        this.addMouseListener(this);
        this.addMouseMotionListener(this);

        while(isRunning) {
            long time = System.currentTimeMillis();

            //  delay for each frame  -   time it took for one frame
            time = (1000 / fps) - (System.currentTimeMillis() - time);

            if (time > 0) {
                try {
                    Thread.sleep(time);
                } catch(Exception e) {

                }
            }
        }
        setVisible(false);
    }

    private int GetNumberPlayers() {
        int numPlayers = 1;
        return numPlayers;
    }

    public void addMessage(String msg) {
        System.out.println(msg);
        try {
            Document doc = editorPane.getDocument();
            doc.insertString(doc.getLength(), msg + "\n", null);
            JScrollBar vertical = editorScrollPane.getVerticalScrollBar();
            vertical.setValue( vertical.getMaximum() );
        } catch(BadLocationException exc) {
            exc.printStackTrace();
        }
    }

    public void run() {
        while (true) {
            Game.playGame();
            if (!startGame) {
                synchronized (this) {
                    try {
                        this.wait();
                    } catch (Exception ex) {
                        throw new RuntimeException(ex);
                    }
                }
            }
            startGame = false;
        }
    }

    public void clearNoSelection() {
        selectNothing = false;
    }
    public boolean getNoSelection() {
        return selectNothing;
    }

    public void mouseClicked(MouseEvent e) {
        synchronized (this) {
            for (Meeple meeple : Game.getNextActiveMeeple()) {
                if (panelRenderer1.getMeepleBounds(meeple).contains(e.getX(), e.getY() - 20) && selectedMeeple == null) {
                    selectedRide = null;
                    selectedMeeple = meeple;
                    this.repaint();
                    this.notify();
                    return;
                }
            }

            if (selectedMeeple != null) {
                for (Ride ride : Game.getPossibleRides(selectedMeeple)) {
                    if (panelRenderer1.getRideBounds(ride).contains(e.getX(), e.getY() - 20)) {
                        selectedRide = ride;
                        this.repaint();
                        this.notify();
                    }
                }
            }
        }
    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public void mouseDragged(MouseEvent e) {

    }

    public void mouseMoved(MouseEvent e) {

    }

    public void setSelectedMeeple(Meeple meeple) {
        this.selectedMeeple = meeple;
    }

    public Meeple getSelectedMeeple() {
        return selectedMeeple;
    }

    public void setSelectedRide(Ride ride) {
        this.selectedRide = ride;
    }

    public Ride getSelectedRide() {
        return selectedRide;
    }
}

