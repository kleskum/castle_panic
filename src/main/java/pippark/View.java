package pippark;

public interface View {
    void reset();
    void addMessage(String msg);
}
