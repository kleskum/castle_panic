package pippark.dice;

import java.awt.*;

public enum DieColor {
    Red(Color.red),
    Green(Color.green),
    Blue(Color.cyan),
    Yelow(Color.yellow);

    public final Color color;
    DieColor(Color color) {
        this.color = color;
    }
}
