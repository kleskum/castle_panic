package pippark.dice;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Die implements Serializable {
    private final DieColor color;
    private final List<Integer> sides = Arrays.asList(1, 2, 3, 4, 5, 6);
    private Integer face = null;

    public Die(DieColor color) {
        this.color = color;
    }

    public Die roll() {
        face = null;
        Collections.shuffle(sides);
        return this;
    }

    public void setFace(int face) {
        this.face = face;
    }

    public int face() {
        return face != null ? face : sides.get(0);
    }

    public DieColor getColor() {
        return color;
    }


}
