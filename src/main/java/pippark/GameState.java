package pippark;

import pippark.mat.Meeple;
import pippark.mat.MeepleType;
import pippark.mat.PlayerColor;
import pippark.objective.Objective;
import pippark.ride.Ride;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class GameState implements Serializable {
    public List<Ride> rides = new ArrayList<Ride>();

    public Meeple adult1 = new Meeple(true, MeepleType.Adult, PlayerColor.Red);
    public Meeple child1 = new Meeple(false, MeepleType.Child, PlayerColor.Red);
    public List<Objective> objectives1 = new ArrayList<Objective>();

    public Meeple adult2 = new Meeple(true, MeepleType.Adult, PlayerColor.Blue);
    public Meeple child2 = new Meeple(false, MeepleType.Child, PlayerColor.Blue);
    public List<Objective> objectives2 = new ArrayList<Objective>();

    public PlayerColor tieBreaker = PlayerColor.Red;

    public int score1 = 0;
    public int score2 = 0;

    public GameState deepCopy() {
        try {
            //Serialization of object
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bos);
            out.writeObject(this);

            //De-serialization of object
            ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
            ObjectInputStream in = new ObjectInputStream(bis);
            return (GameState) in.readObject();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
