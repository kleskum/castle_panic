package pippark.mat;

import java.awt.*;

public enum PlayerColor {
    Red(Color.red),
    Blue(Color.blue);

    public Color color;

    PlayerColor(Color color) {
        this.color = color;
    }
}
