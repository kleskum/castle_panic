package pippark.mat;

import pippark.dice.Die;
import pippark.ride.Ride;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Meeple implements Serializable {
    private final boolean isAdult;
    private final MeepleType meepleType;
    private final PlayerColor color;
    private Ride ride = null;
    private List<Die> dice = new ArrayList<Die>();

    public Meeple(boolean isAdult, MeepleType meepleType, PlayerColor color) {
        this.isAdult = isAdult;
        this.meepleType = meepleType;
        this.color = color;
    }

    public boolean isAdult() {
        return isAdult;
    }

    public MeepleType getMeepleType() {
        return meepleType;
    }

    public Ride getRide() {
        return ride;
    }

    public void setRide(Ride ride) {
        this.ride = ride;
    }

    public PlayerColor getColor() {
        return color;
    }

    public void addDie(Die die) {
        dice.add(die);
    }

    public List<Die> getDice() {
        return dice;
    }

    public int getDiceTotal() {
        int t = 0;
        for(Die die : dice) {
            t+=die.face();
        }
        return t;
    }

    public void reset() {
        ride = null;
        dice.clear();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Meeple meeple = (Meeple) o;

        if (isAdult != meeple.isAdult) return false;
        if (meepleType != meeple.meepleType) return false;
        return color == meeple.color;
    }

    @Override
    public int hashCode() {
        int result = (isAdult ? 1 : 0);
        result = 31 * result + (meepleType != null ? meepleType.hashCode() : 0);
        result = 31 * result + (color != null ? color.hashCode() : 0);
        return result;
    }
}
