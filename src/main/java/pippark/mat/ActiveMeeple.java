package pippark.mat;

import java.util.ArrayList;

public class ActiveMeeple extends ArrayList<Meeple> {
    private boolean tieBreaker = false;

    public boolean usesTieBreaker() {
        return tieBreaker;
    }

    public void setTieBreaker(boolean tieBreaker) {
        this.tieBreaker = tieBreaker;
    }

    public PlayerColor playerColor() {
        return get(0).getColor();
    }
}
