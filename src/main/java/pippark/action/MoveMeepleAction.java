package pippark.action;

import pippark.Game;
import pippark.dice.Die;
import pippark.mat.Meeple;
import pippark.ride.Ride;

public class MoveMeepleAction implements Action {
    private Meeple m;
    private Ride r;
    private boolean usesTieBreaker;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoveMeepleAction that = (MoveMeepleAction) o;

        if (usesTieBreaker != that.usesTieBreaker) return false;
        if (m != null ? !m.equals(that.m) : that.m != null) return false;
        return r != null ? r.equals(that.r) : that.r == null;
    }

    @Override
    public int hashCode() {
        int result = m != null ? m.hashCode() : 0;
        result = 31 * result + (r != null ? r.hashCode() : 0);
        result = 31 * result + (usesTieBreaker ? 1 : 0);
        return result;
    }

    public MoveMeepleAction(Meeple meeple, Ride ride, boolean usesTieBreaker) {
        this.m = meeple;
        this.r = ride;
        this.usesTieBreaker = usesTieBreaker;
    }

    public Meeple getMeeple() {
        return m;
    }

    public Ride getRide() {
        return r;
    }

    public void execute(Game game) {
        Meeple meeple = game.getMeeple(m);
        Ride ride = game.getRide(r);

        if(ride.getDice().length == 0) {
            System.out.println("What?:");
            return;
        }
        Die die = ride.removeDie();
        if (game.hasPenalty(ride, meeple.getRide())) {
            die.setFace(Math.min(6, die.face() + 1));
        }
        meeple.setRide(ride);
        meeple.addDie(die);
        game.getView().addMessage("Moved: " + meeple.getColor() + " " + meeple.getMeepleType().name());

        if (usesTieBreaker) {
            game.moveTieBreaker();
        }
    }
}
