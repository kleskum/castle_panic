package pippark.action;

import pippark.Game;
import pippark.mat.Meeple;

public class LeaveParkAction implements Action {
    private Meeple meeple;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LeaveParkAction that = (LeaveParkAction) o;

        return meeple != null ? meeple.equals(that.meeple) : that.meeple == null;
    }

    @Override
    public int hashCode() {
        return meeple != null ? meeple.hashCode() : 0;
    }

    public LeaveParkAction(Meeple meeple) {
        this.meeple = meeple;
    }

    public Meeple getMeeple() {
        return meeple;
    }

    public void execute(Game game) {
        game.getMeeple(meeple).setRide(null);
        game.getView().addMessage("Left: " + meeple.getColor() + " " + meeple.getMeepleType().name());
    }
}
