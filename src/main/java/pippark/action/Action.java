package pippark.action;

import pippark.Game;

public interface Action {
    void execute(Game game);
}
