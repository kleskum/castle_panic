package pippark;

import pippark.mat.Meeple;
import pippark.ride.Ride;

import java.util.List;

public interface Input {
    Meeple chooseMeeple(List<Meeple> options);
    Ride chooseRide(Meeple meeple, List<Ride> options);
}
