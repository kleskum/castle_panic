package pippark.objective;

import pippark.dice.DieColor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Objective implements Serializable {
    public List<DieColor> adultDice = new ArrayList<DieColor>();
    public List<DieColor> kidDice = new ArrayList<DieColor>();

    public Objective(int adultCount, DieColor adultColor, int kidCount, DieColor kidColor) {
        for(int i = 0; i < adultCount; i++) {
            adultDice.add(adultColor);
        }
        for(int i = 0; i < kidCount; i++) {
            kidDice.add(kidColor);
        }
    }
}
