package pippark.ride;

import pippark.dice.Die;
import pippark.dice.DieColor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Ride implements Serializable {
    private final RideType rideType;
    private final DieColor dieColor;
    private int row;
    private int column;
    private Die[] dice;

    public Ride(RideType rideType, DieColor color) {
        this.rideType = rideType;
        this.dieColor = color;
        reset();
    }

    public Die removeDie() {
        List<Die> temp = Arrays.asList(dice);
        Die die = temp.get(0);
        dice = temp.subList(1, temp.size()).toArray(new Die[0]);
        return die;
    }

    public RideType getRideType() {
        return rideType;
    }

    public Die[] getDice() {
        return dice;
    }

    public void place(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public void reset() {
        dice = new Die[] {
                new Die(dieColor).roll(),
                new Die(dieColor).roll(),
                new Die(dieColor).roll(),
                new Die(dieColor).roll()
        };
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ride ride = (Ride) o;

        if (row != ride.row) return false;
        if (column != ride.column) return false;
        if (rideType != ride.rideType) return false;
        return dieColor == ride.dieColor;
    }

    @Override
    public int hashCode() {
        int result = rideType != null ? rideType.hashCode() : 0;
        result = 31 * result + (dieColor != null ? dieColor.hashCode() : 0);
        result = 31 * result + row;
        result = 31 * result + column;
        return result;
    }
}
