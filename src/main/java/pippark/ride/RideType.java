package pippark.ride;

public enum RideType {
    Kid,
    Adult,
    Family
}
