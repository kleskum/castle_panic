package pippark;

import pippark.mat.Meeple;
import pippark.ride.Ride;

import java.util.List;
import java.util.Random;

public class RandomInput implements Input {
    private Random r = new Random();

    public Meeple chooseMeeple(List<Meeple> options) {
        // choose a random meeple
        return options.get(r.nextInt(options.size()));
    }

    public Ride chooseRide(Meeple meeple, List<Ride> options) {
        // choose a random ride
        return options.get(r.nextInt(options.size()));
    }
}
