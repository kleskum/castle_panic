package tinyepic.quest.player;

import tinyepic.quest.map.Castle;
import tinyepic.quest.map.MapRegion;
import tinyepic.quest.map.PlayerCoior;
import tinyepic.quest.map.Portal;
import tinyepic.quest.map.Temple;

public class Hero {
    private final String name;
    private final PlayerCoior color;
    private MapRegion region;

    public Hero(String name, PlayerCoior color, MapRegion startRegion) {
        this.name = name;
        this.color = color;
        this.region = startRegion;
        startRegion.moveHere(this);
    }

    public PlayerCoior getColor() {
        return color;
    }

    public MapRegion getRegion() {
        return region;
    }

    public boolean inMyCastle() {
        return region instanceof Castle && ((Castle)region).getColor() == getColor();
    }

    public boolean fightingGoblin() {
        return region instanceof Portal && ((Portal)region).getGoblin() != null;
    }

    public boolean inTemple() {
        return region instanceof Temple;
    }

    public void place(MapRegion region) {
        this.region.removeHero(this);
        this.region = region;
        this.region.moveHere(this);
    }

    @Override
    public String toString() {
        return name;
    }
}
