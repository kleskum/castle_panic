package tinyepic.quest.player;

import tinyepic.quest.item.Shield;
import tinyepic.quest.item.Staff;
import tinyepic.quest.item.Sword;
import tinyepic.quest.map.MapRegion;
import tinyepic.quest.map.PlayerCoior;
import tinyepic.quest.map.TempleEmblem;

import java.util.ArrayList;
import java.util.List;

public class Player {
    private final PlayerCoior color;
    private List<Hero> heroes = new ArrayList();

    private int maxHealth = 6;
    private int maxEnergy = 3;

    private int health = 6;
    private int energy = 3;
    private int magic = 0;

    private final LegendaryItem sword;
    private final LegendaryItem shield;
    private final LegendaryItem staff;

    public Player(PlayerCoior color, MapRegion startRegion) {
        this.color = color;
        for(int i = 1; i <= 3; i++) {
            heroes.add(new Hero(i + "", color, startRegion));
        }

        switch(color) {
            case Green:
                sword = new LegendaryItem(new Sword(), TempleEmblem.Tree, TempleEmblem.Water);
                shield = new LegendaryItem(new Shield(), TempleEmblem.Rock, TempleEmblem.Fire);
                staff = new LegendaryItem(new Staff(), TempleEmblem.Sand, TempleEmblem.Ice);
                break;
            case Blue:
                sword = null;
                shield = null;
                staff = null;
                break;
            case Yellow:
                sword = null;
                shield = null;
                staff = null;
                break;
            case Red:
                sword = null;
                shield = null;
                staff = null;
                break;
            default:
                throw new RuntimeException("Unknown player color: " + color);
        }
    }

    public PlayerCoior getColor() {
        return color;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public int getMaxEnergy() {
        return maxEnergy;
    }

    public void setMaxEnergy(int maxEnergy) {
        this.maxEnergy = maxEnergy;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public void damage() {
        health = Math.max(health-1, 0);
    }

    public void heal() {
        health = Math.min(health+1, maxHealth);
    }

    public void energize() {
        energy = Math.min(energy+1, maxEnergy);
    }

    public boolean exhausted() {
        return health <= 0;
    }

    public List<Hero> getHeroes() {
        return heroes;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Health: " + health + " of " + maxHealth + "\n");
        sb.append("Energy: " + energy + " of " + maxEnergy + "\n");
        sb.append(" Magic: " + magic + "\n");
        sb.append(" Sword: " + sword  + "\n");
        sb.append("Shield: " + shield  + "\n");
        sb.append(" Staff: " + staff  + "\n");
        return sb.toString();
    }
}
