package tinyepic.quest.player;

import tinyepic.quest.item.Item;
import tinyepic.quest.map.TempleEmblem;

public class LegendaryItem {
    private final Item item;
    private final TempleEmblem firstTemple;
    private final TempleEmblem secondTemple;
    private int progress = 0;

    public LegendaryItem(Item item, TempleEmblem firstTemple, TempleEmblem secondTemple) {
        this.item = item;
        this.firstTemple = firstTemple;
        this.secondTemple = secondTemple;
    }

    public Item getItem() {
        return item;
    }

    public TempleEmblem getFirstTemple() {
        return firstTemple;
    }

    public TempleEmblem getSecondTemple() {
        return secondTemple;
    }

    public int getProgress() {
        return progress;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(item.getClass().getSimpleName() + ": ");
        switch(progress) {
            case 2:
                sb.append("Complete");
                break;
            case 0:
                sb.append("Need " + firstTemple.name() + " then " + secondTemple.name());
                break;
            case 1:
                sb.append("Need " + secondTemple.name());
        }
        return sb.toString();
    }
}
