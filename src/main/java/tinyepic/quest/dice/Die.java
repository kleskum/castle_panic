package tinyepic.quest.dice;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Die {
    private final List<DieSide> sides = Arrays.asList(DieSide.values());

    public void roll() {
        Collections.shuffle(sides);
    }

    public DieSide face() {
        return sides.get(0);
    }
}
