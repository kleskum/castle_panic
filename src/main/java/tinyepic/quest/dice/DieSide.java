package tinyepic.quest.dice;

public enum DieSide {
    Goblin,
    Energy,
    Torch,
    Scroll,
    Punch,
    Mushroom
}
