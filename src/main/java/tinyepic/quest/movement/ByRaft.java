package tinyepic.quest.movement;

import tinyepic.quest.Grid;
import tinyepic.quest.GridPoint;
import tinyepic.quest.map.MapCard;
import tinyepic.quest.player.Hero;

import java.util.ArrayList;
import java.util.List;

public class ByRaft extends MovementCard {
    public List<MapCard> getMapCardOptions(Hero hero, Grid grid) {
        List<MapCard> cards = new ArrayList<MapCard>();
        GridPoint point = grid.getPoint(hero.getRegion().getMapCard());
        for(int row = 0; row < 5; row++) {
            if(row != point.getRow()) {
                MapCard mapCard = grid.getCard(new GridPoint(row, point.getColumn()));
                if (mapCard != null) {
                    cards.add(mapCard);
                }
            }
        }
        return cards;
    }
}
