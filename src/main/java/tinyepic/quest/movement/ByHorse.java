package tinyepic.quest.movement;

import tinyepic.quest.Grid;
import tinyepic.quest.GridPoint;
import tinyepic.quest.map.MapCard;
import tinyepic.quest.player.Hero;

import java.util.ArrayList;
import java.util.List;

public class ByHorse extends MovementCard {
    public List<MapCard> getMapCardOptions(Hero hero, Grid grid) {
        List<MapCard> cards = new ArrayList<MapCard>();
        GridPoint point = grid.getPoint(hero.getRegion().getMapCard());
        for(int column = 0; column < 5; column++) {
            if(column != point.getColumn()) {
                MapCard mapCard = grid.getCard(new GridPoint(point.getRow(), column));
                if (mapCard != null) {
                    cards.add(mapCard);
                }
            }
        }
        return cards;
    }
}
