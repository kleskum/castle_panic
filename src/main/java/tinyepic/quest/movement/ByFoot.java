package tinyepic.quest.movement;

import tinyepic.quest.Grid;
import tinyepic.quest.GridPoint;
import tinyepic.quest.map.MapCard;
import tinyepic.quest.player.Hero;

import java.util.ArrayList;
import java.util.List;

public class ByFoot extends MovementCard {
    public List<MapCard> getMapCardOptions(Hero hero, Grid grid) {
        List<MapCard> cards = new ArrayList<MapCard>();
        GridPoint point = grid.getPoint(hero.getRegion().getMapCard());
        MapCard p1 = grid.getCard(new GridPoint(point.getRow()-1, point.getColumn()-1));
        MapCard p2 = grid.getCard(new GridPoint(point.getRow()-1, point.getColumn()));
        MapCard p3 = grid.getCard(new GridPoint(point.getRow()-1, point.getColumn()+1));
        MapCard p4 = grid.getCard(new GridPoint(point.getRow(), point.getColumn()-1));
        MapCard p5 = grid.getCard(new GridPoint(point.getRow(), point.getColumn()+1));
        MapCard p6 = grid.getCard(new GridPoint(point.getRow()+1, point.getColumn()-1));
        MapCard p7 = grid.getCard(new GridPoint(point.getRow()+1, point.getColumn()));
        MapCard p8 = grid.getCard(new GridPoint(point.getRow()+1, point.getColumn()+1));
        if(p1 != null) cards.add(p1);
        if(p2 != null) cards.add(p2);
        if(p3 != null) cards.add(p3);
        if(p4 != null) cards.add(p4);
        if(p5 != null) cards.add(p5);
        if(p6 != null) cards.add(p6);
        if(p7 != null) cards.add(p7);
        if(p8 != null) cards.add(p8);
        return cards;
    }
}
