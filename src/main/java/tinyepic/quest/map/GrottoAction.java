package tinyepic.quest.map;

import tinyepic.quest.Game;

public interface GrottoAction {
    boolean isValid(Game game);
    void doAction(Game game);
}
