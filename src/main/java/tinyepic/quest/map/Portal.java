package tinyepic.quest.map;

public class Portal extends MapRegion {
    private Goblin goblin = new Goblin();

    public Goblin getGoblin() {
        return goblin;
    }

    @Override
    public String toString() {
        return "Portal:" + getGoblin().getHealth() + " ";
    }
}
