package tinyepic.quest.map;

import tinyepic.quest.player.Hero;

import java.util.ArrayList;
import java.util.List;

public abstract class MapRegion {
    private MapCard mapCard;
    private List<Hero> heroes = new ArrayList<Hero>();

    public MapCard getMapCard() {
        return mapCard;
    }

    public void setMapCard(MapCard mapCard) {
        this.mapCard = mapCard;
    }

    public List<Hero> getHeroes() {
        return heroes;
    }

    public void removeHero(Hero hero) {
        heroes.remove(hero);
    }

    public void moveHere(Hero hero) {
        heroes.add(hero);
    }
}
