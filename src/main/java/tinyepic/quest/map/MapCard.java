package tinyepic.quest.map;

public class MapCard {
    private final MapRegion leftRegion;
    private final MapRegion rightRegion;

    public MapCard(MapRegion leftRegion, MapRegion rightRegion) {
        this.leftRegion = leftRegion;
        this.rightRegion = rightRegion;

        this.leftRegion.setMapCard(this);
        this.rightRegion.setMapCard(this);
    }

    public MapRegion getLeftRegion() {
        return leftRegion;
    }

    public MapRegion getRightRegion() {
        return rightRegion;
    }

    @Override
    public String toString() {
        StringBuilder sbl = new StringBuilder();
        StringBuilder sbr = new StringBuilder();
        for(int i = 0; i < 3; i++) {
            sbl.append(getLeftRegion().getHeroes().size() > i ? getLeftRegion().getHeroes().get(i).toString() : "_");
            sbr.append(getRightRegion().getHeroes().size() > i ? getRightRegion().getHeroes().get(i).toString() : "_");
        }

        return "[" + getLeftRegion() + sbl.toString() + "|" + getRightRegion() + sbr.toString() + "]";
    }
}
