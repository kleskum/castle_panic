package tinyepic.quest.map;

public class Grotto extends MapRegion {
    private final GrottoAction action;

    public Grotto(GrottoAction action) {
        this.action = action;
    }

    @Override
    public String toString() {
        return "Grotto:  ";
    }
}
