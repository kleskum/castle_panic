package tinyepic.quest.map;

public class Goblin {
    private int health = 5;
    private boolean passive = true;

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public boolean isPassive() {
        return passive;
    }

    public void setPassive(boolean passive) {
        this.passive = passive;
    }

    public void punch() {
        health = Math.max(health-1, 0);
    }
}
