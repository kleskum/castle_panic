package tinyepic.quest.map;

public class Castle extends MapRegion {
    private final PlayerCoior color;

    public Castle(PlayerCoior color) {
        this.color = color;
    }

    public PlayerCoior getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "Castle:" + color.name().substring(0, 1) + " ";
    }
}
