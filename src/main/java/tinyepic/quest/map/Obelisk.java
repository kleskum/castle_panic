package tinyepic.quest.map;

public class Obelisk extends MapRegion {
    private final int spellLevel;

    public Obelisk(int spellLevel) {
        this.spellLevel = spellLevel;
    }

    public int getSpellLevel() {
        return spellLevel;
    }

    @Override
    public String toString() {
        return "Magic:" + (spellLevel < 10 ? "0" + spellLevel : "10") + " ";
    }
}
