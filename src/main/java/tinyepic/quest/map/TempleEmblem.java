package tinyepic.quest.map;

public enum TempleEmblem {
    Water,
    Fire,
    Rock,
    Ice,
    Tree,
    Sand
}
