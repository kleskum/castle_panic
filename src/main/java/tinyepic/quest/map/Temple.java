package tinyepic.quest.map;

import tinyepic.quest.dice.DieSide;
import tinyepic.quest.player.Hero;

import java.util.HashMap;
import java.util.Map;

public class Temple extends MapRegion {
    private final TempleEmblem templeEmblem;
    private final DieSide dieRequirement;

    private Map<Hero, Integer> progress = new HashMap<Hero, Integer>();

    public Temple(TempleEmblem templeEmblem, DieSide dieRequirement) {
        this.templeEmblem = templeEmblem;
        this.dieRequirement = dieRequirement;
    }

    public TempleEmblem getTempleEmblem() {
        return templeEmblem;
    }

    public DieSide getDieRequirement() {
        return dieRequirement;
    }

    @Override
    public void moveHere(Hero hero) {
        super.moveHere(hero);
        progress.put(hero, 0);
    }

    @Override
    public void removeHero(Hero hero) {
        super.removeHero(hero);
        progress.remove(hero);
    }

    public void advance(Hero hero) {
        int forge = progress.get(hero);
        progress.put(hero, Math.min(forge+1, 4));
    }

    public int getProgress(Hero hero) {
        return progress.get(hero);
    }

    @Override
    public String toString() {
        return "Tmp:" + templeEmblem.name().substring(0, 1) + dieRequirement.name().substring(0, 1) + "-" +
                (getHeroes().size() > 0 ? progress.get(getHeroes().get(0)) : "X") + " ";
    }
}
