package tinyepic.quest;

import tinyepic.quest.map.MapRegion;
import tinyepic.quest.player.Hero;

public interface Input {
    Hero chooseHero();
    MapRegion chooseMapRegion();
}
