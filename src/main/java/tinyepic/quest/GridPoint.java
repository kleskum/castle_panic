package tinyepic.quest;

public class GridPoint {
    private final int row;
    private final int column;

    public GridPoint(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GridPoint gridPoint = (GridPoint) o;

        if (row != gridPoint.row) return false;
        return column == gridPoint.column;

    }

    @Override
    public int hashCode() {
        int result = row;
        result = 31 * result + column;
        return result;
    }
}
