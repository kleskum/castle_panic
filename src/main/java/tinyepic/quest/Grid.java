package tinyepic.quest;

import tinyepic.quest.map.MapCard;

import java.util.HashMap;
import java.util.Map;

public class Grid {
    private final Map<GridPoint, MapCard> pointToCard = new HashMap<GridPoint, MapCard>();
    private final Map<MapCard, GridPoint> cardToPoint = new HashMap<MapCard, GridPoint>();

    public void put(GridPoint gridPoint, MapCard mapCard) {
        pointToCard.put(gridPoint, mapCard);
        cardToPoint.put(mapCard, gridPoint);
    }

    public int orthogonalDistance(MapCard card1, MapCard card2) {
        GridPoint point1 = cardToPoint.get(card1);
        GridPoint point2 = cardToPoint.get(card2);

        int minRow = Math.min(point1.getRow(), point2.getRow());
        int maxRow = Math.max(point1.getRow(), point2.getRow());

        int minColumn = Math.min(point1.getColumn(), point2.getColumn());
        int maxColumn = Math.max(point1.getColumn(), point2.getColumn());

        return (maxRow - minRow) + (maxColumn - minColumn);
    }

    public GridPoint getPoint(MapCard mapCard) {
        return cardToPoint.get(mapCard);
    }

    public MapCard getCard(GridPoint gridPoint) {
        return pointToCard.get(gridPoint);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(int r = 0; r < 5; r++) {
            for(int c = 0; c < 5; c++) {
                MapCard mapCard = getCard(new GridPoint(r, c));
                if(mapCard != null) {
                    sb.append(mapCard.toString());
                } else {
                    String space = "             ";
                    sb.append(space + " " +  space);
                }
            }
            sb.append("\n\n");
        }
        return sb.toString();
    }
}
