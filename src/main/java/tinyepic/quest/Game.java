package tinyepic.quest;

import tinyepic.quest.dice.Die;
import tinyepic.quest.dice.DieSide;
import tinyepic.quest.map.Castle;
import tinyepic.quest.map.Grotto;
import tinyepic.quest.map.GrottoAction;
import tinyepic.quest.map.MapCard;
import tinyepic.quest.map.MapRegion;
import tinyepic.quest.map.Obelisk;
import tinyepic.quest.map.PlayerCoior;
import tinyepic.quest.map.Portal;
import tinyepic.quest.map.Temple;
import tinyepic.quest.map.TempleEmblem;
import tinyepic.quest.movement.ByFoot;
import tinyepic.quest.movement.ByGryphon;
import tinyepic.quest.movement.ByHorse;
import tinyepic.quest.movement.ByRaft;
import tinyepic.quest.movement.ByShip;
import tinyepic.quest.movement.MovementCard;
import tinyepic.quest.player.Hero;
import tinyepic.quest.player.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Game {
    private Input input;

    private List<Die> dice = new ArrayList<Die>();
    private List<Player> players = new ArrayList<Player>();

    private Grid grid = new Grid();
    private Player activePlayer;
    private int round = 1;
    private MagicLevel magicLevel = new MagicLevel();
    private List<MovementCard> movement = new ArrayList<MovementCard>();

    public void setup() {
        List<MapCard> mapCards = new ArrayList<MapCard>();
        List<MapCard> castleCards = new ArrayList<MapCard>();

        // Dice
        for(int i = 0; i < 5; i++) {
            dice.add(new Die());
        }

        // Map
        mapCards.add(new MapCard(new Portal(), new Grotto(new GrottoAction() {
            public boolean isValid(Game game) {
                return true;
            }
            public void doAction(Game game) {
                // gain 2 health for each hero outside your castle
                for(Hero hero : game.getActivePlayer().getHeroes()) {
                    if(!hero.inMyCastle()) {
                        game.getActivePlayer().heal();
                        game.getActivePlayer().heal();
                    }
                }
            }
        })));
        mapCards.add(new MapCard(new Portal(), new Grotto(new GrottoAction() {
            public boolean isValid(Game game) {
                return true;
            }
            public void doAction(Game game) {
                // move another hero up 3 map cards
                Hero hero = input.chooseHero();
                for(int i = 0; i < 3; i++) {
                    MapRegion mapRegion = input.chooseMapRegion();
                    if (mapRegion == hero.getRegion()) {
                        return;
                    } else if (grid.orthogonalDistance(hero.getRegion().getMapCard(), mapRegion.getMapCard()) == 1) {
                        hero.place(mapRegion);
                    }
                }
            }
        })));
        mapCards.add(new MapCard(new Portal(), new Grotto(new GrottoAction() {
            public boolean isValid(Game game) {
                for(Hero hero : game.getActivePlayer().getHeroes()) {
                    if(hero.fightingGoblin()) {
                        return true;
                    }
                }
                return false;
            }
            public void doAction(Game game) {
                // deal 1 punch to a goblin
                Hero hero = null;
                while(hero == null || hero.getColor() != game.getActivePlayer().getColor() || !hero.fightingGoblin()) {
                    hero = input.chooseHero();
                }
                ((Portal)hero.getRegion()).getGoblin().punch();
            }
        })));
        mapCards.add(new MapCard(new Portal(), new Grotto(new GrottoAction() {
            public boolean isValid(Game game) {
                return true;
            }
            public void doAction(Game game) {
                // conjure 2 magic movement
            }
        })));
        mapCards.add(new MapCard(new Portal(), new Grotto(new GrottoAction() {
            public boolean isValid(Game game) {
                return true;
            }
            public void doAction(Game game) {
                // gain 2 energy for each hero outside your castle
                for(Hero hero : game.getActivePlayer().getHeroes()) {
                    if(!hero.inMyCastle()) {
                        game.getActivePlayer().energize();
                        game.getActivePlayer().energize();
                    }
                }
            }
        })));
        mapCards.add(new MapCard(new Portal(), new Grotto(new GrottoAction() {
            public boolean isValid(Game game) {
                return true;
            }
            public void doAction(Game game) {
                // roll dice and move for each torch/scroll
            }
        })));
        mapCards.add(new MapCard(new Portal(), new Grotto(new GrottoAction() {
            public boolean isValid(Game game) {
                for(Hero hero : game.getActivePlayer().getHeroes()) {
                    if(hero.inTemple() && ((Temple)hero.getRegion()).getProgress(hero) != 4) {
                        return true;
                    }
                }
                return false;
            }
            public void doAction(Game game) {
                // advance 1 temple space

            }
        })));

        mapCards.add(new MapCard(new Obelisk(1), new Temple(TempleEmblem.Rock, DieSide.Torch)));
        mapCards.add(new MapCard(new Obelisk(2), new Temple(TempleEmblem.Tree, DieSide.Scroll)));
        mapCards.add(new MapCard(new Obelisk(3), new Temple(TempleEmblem.Sand, DieSide.Torch)));
        mapCards.add(new MapCard(new Obelisk(5), new Temple(TempleEmblem.Fire, DieSide.Scroll)));
        mapCards.add(new MapCard(new Obelisk(6), new Temple(TempleEmblem.Water, DieSide.Scroll)));
        mapCards.add(new MapCard(new Obelisk(8), new Temple(TempleEmblem.Ice, DieSide.Torch)));

        Castle greenCastle = new Castle(PlayerCoior.Green);
        Castle redCastle = new Castle(PlayerCoior.Red);
        Castle yellowCastle = new Castle(PlayerCoior.Yellow);
        Castle blueCastle = new Castle(PlayerCoior.Blue);

        castleCards.add(new MapCard(new Obelisk(4), greenCastle));
        castleCards.add(new MapCard(new Obelisk(7), redCastle));
        castleCards.add(new MapCard(new Obelisk(9), yellowCastle));
        castleCards.add(new MapCard(new Obelisk(10), blueCastle));

        arrangeMap(mapCards, castleCards);

        setupMovement();

        players.add(new Player(PlayerCoior.Green, greenCastle));
        activePlayer = players.get(0);
    }

    public void arrangeMap(List<MapCard> mapCards, List<MapCard> castleCards) {
        Collections.shuffle(mapCards);
        Collections.shuffle(castleCards);

        grid.put(new GridPoint(0, 2), mapCards.remove(0));
        grid.put(new GridPoint(0, 3), mapCards.remove(0));

        grid.put(new GridPoint(1, 0), mapCards.remove(0));
        grid.put(new GridPoint(1, 1), castleCards.remove(0));
        grid.put(new GridPoint(1, 2), mapCards.remove(0));
        grid.put(new GridPoint(1, 3), castleCards.remove(0));

        grid.put(new GridPoint(2, 0), mapCards.remove(0));
        grid.put(new GridPoint(2, 1), mapCards.remove(0));
        grid.put(new GridPoint(2, 2), mapCards.remove(0));
        grid.put(new GridPoint(2, 3), mapCards.remove(0));
        grid.put(new GridPoint(2, 4), mapCards.remove(0));

        grid.put(new GridPoint(3, 1), castleCards.remove(0));
        grid.put(new GridPoint(3, 2), mapCards.remove(0));
        grid.put(new GridPoint(3, 3), castleCards.remove(0));
        grid.put(new GridPoint(3, 4), mapCards.remove(0));

        grid.put(new GridPoint(4, 1), mapCards.remove(0));
        grid.put(new GridPoint(4, 2), mapCards.remove(0));
    }

    public Player getActivePlayer() {
        return activePlayer;
    }

    private void setupMovement() {
        movement.clear();
        movement.add(new ByFoot());
        movement.add(new ByGryphon());
        movement.add(new ByRaft());
        movement.add(new ByHorse());
        movement.add(new ByShip());
        Collections.shuffle(movement);
    }

    private enum GamePhase {
        Day,
        Night
    }
    private GamePhase phase = GamePhase.Day;

    private int getChoice(Scanner scanner, int min, int max) {
        while(true) {
            try {
                String choice = scanner.next().trim();
                int iChoice = Integer.parseInt(choice);
                if(iChoice >= min && iChoice <= max) {
                    return iChoice;
                }
            } catch (Exception ex) {

            }
        }
    }

    public void play() {
        Scanner s = new Scanner(System.in);

        // 1 player game
        while(round <= 5) {
            System.out.println("Round " + round);
            System.out.println(magicLevel);
            System.out.println(grid + "\n");
            for(Player player : players) {
                System.out.println(player);
            }

            switch (phase) {
                case Day:
                    // choose movement
                    MovementCard choice1 = movement.remove(0);
                    MovementCard choice2 = movement.remove(0);
                    System.out.println("1) by " + choice1.getClass().getSimpleName().replace("By", ""));
                    System.out.println("2) by " + choice2.getClass().getSimpleName().replace("By", ""));
                    System.out.print("Move: ");
                    MovementCard choice = getChoice(s, 1, 2) == 1 ? choice1 : choice2;
                    movement.add(0, choice == choice1 ? choice2 : choice1);

                    // choose hero
                    System.out.print("Hero 1, 2 or 3: ");
                    Hero playerHero = activePlayer.getHeroes().get(getChoice(s, 1, 3) - 1);

                    // get movement choices
                    List<MapCard> mapCardOptions = choice.getMapCardOptions(playerHero, grid);
                    for (int i = 1; i <= mapCardOptions.size(); i++) {
                        System.out.println(i + ") " + mapCardOptions.get(i - 1));
                    }
                    System.out.println((mapCardOptions.size() + 1) + ") Idle");
                    System.out.print("To: ");
                    int card = getChoice(s, 1, mapCardOptions.size() + 1);
                    if (card == mapCardOptions.size() + 1) {
                        // heal
                    } else {
                        // choose a region
                        MapCard mapCard = mapCardOptions.get(card - 1);
                        System.out.println("1) " + mapCard.getLeftRegion());
                        System.out.println("2) " + mapCard.getRightRegion());

                        // place hero
                        playerHero.place(getChoice(s, 1, 2) == 1 ? mapCard.getLeftRegion() : mapCard.getRightRegion());
                    }

                    if (movement.size() < 2) {
                        // move to night
                        phase = GamePhase.Night;
                        setupMovement(); // reset movement for next round
                    }

                    break;
                case Night:
                    System.out.println("1) Adventure");
                    System.out.println("2) Rest");

                    int adventure = getChoice(s, 1, 2);
                    if (adventure == 2) {
                        phase = GamePhase.Day;
                        nextRound();
                    } else {
                        for (Die die : dice) {
                            die.roll();
                            System.out.print(die.face() + " ");
                        }
                        System.out.println();

                        // goblins first
                        for (int i = 0; i < magicLevel.getDamageDice(getDieSideCount(DieSide.Goblin)); i++) {
                            for (int j = 0; j < magicLevel.getDamagePerGoblin(); j++) {
                                activePlayer.damage();
                            }
                        }
                        if (activePlayer.exhausted()) {
                            // womp womp
                            phase = GamePhase.Day;
                            nextRound();
                        }

                        // next energy
                        for (int i = 0; i < magicLevel.getEnergy(getDieSideCount(DieSide.Energy)); i++) {
                            activePlayer.energize();
                        }

                        // next mushroom
                        for (int i = 0; i < getDieSideCount(DieSide.Mushroom); i++) {
                            magicLevel.conjure();
                            for (int j = 0; j < magicLevel.getDamagePerMushroom(); j++) {
                                activePlayer.damage();
                            }
                        }
                    }
            }
        }
    }

    private void nextRound() {
        round++;
    }

    private int getDieSideCount(DieSide dieSide) {
        int count = 0;
        for(Die die : dice) {
            if(die.face() == dieSide) {
                count++;
            }
        }
        return count;
    }



    public static void main(String[] args) {
        Game game = new Game();
        game.setup();
        game.play();
    }
}
