package tinyepic.quest;

public class MagicLevel {
    private int conjure = 1;
    private int magic = 0;
    private final int maxConjure = 10;

    @Override
    public String toString() {
        return "Mushroom at " + conjure + " of " + maxConjure + " which is magic level " + magic;
    }

    public int getDamageDice(int goblinCount) {
        // these are rules for a 1 player game
        switch(magic) {
            case 0:
                return goblinCount - 1;
            case 1:
                if (goblinCount < 2) {
                    return goblinCount;
                } else {
                    return goblinCount - 1;
                }
            default:
                return goblinCount;
        }
    }

    public int getDamagePerGoblin() {
        switch(magic) {
            case 3:
                return 2;
            default:
                return 1;
        }
    }

    public int getDamagePerMushroom() {
        switch(magic) {
            case 1:
                return 1;
            default:
                return 0;
        }
    }

    public int getEnergy(int energyCount) {
        return energyCount;
    }

    public void conjure() {
        conjure = Math.min(conjure+1, maxConjure);
    }
}
