package castlepanic;

import castlepanic.base.board.Ring;
import castlepanic.base.tokens.Monster;
import castlepanic.base.tokens.Structure;
import castlepanic.component.Card;
import castlepanic.component.Player;
import castlepanic.component.Token;

import java.util.List;

public interface Input {
    void redraw();
    void addMessage(String msg);
    Card requestCard(); // assumes current player
    Card requestCard(Player player);
    Card requestAnyCard(); // owner doesn't matter
    Trade requestTrade();
    Card requestCardToPlay();
    <T extends Token> T chooseToken(Class<T> clazz);
    Ring chooseRing();
    Card chooseCardFromDiscard();
    Card chooseCard(List<Card> cards);
    boolean getNoSelection();
    void resetNoSelect();
    void notifyMonsterSlayed(Monster monster);
    void notifyStructureDestroyed(Structure structure);
    boolean askYesNoQuestion(String question);
}

