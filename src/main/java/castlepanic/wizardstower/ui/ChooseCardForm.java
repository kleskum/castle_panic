package castlepanic.wizardstower.ui;

import castlepanic.PanelRenderer;
import castlepanic.component.Card;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

public class ChooseCardForm extends JDialog implements MouseListener {
    private List<Card> cards;
    public Card SelectedCard = null;

    public ChooseCardForm(JFrame parent, final List<Card> cards) {
        super(parent, "Choose 1", true);
        setTitle("Castle Panic");
        setResizable(false);
        this.cards = cards;

        Canvas canvas = new Canvas() {
            @Override
            public void paint(Graphics g) {
                super.paint(g);

                int x = 20;
                int y = 20;
                int cardWidth = 60;
                int cardHeight = 80;

                for (Card card : cards) {
                    PanelRenderer.drawCard(g, card, x, y, cardWidth, cardHeight);
                    x += cardWidth + 10;
                    if (x > 600) {
                        x = 20;
                        y += cardHeight + 10;
                    }
                }
            }
        };
        canvas.setSize(600, 400);
        canvas.addMouseListener(this);
        this.add(canvas);
        this.pack();
    }

    public void mouseClicked(MouseEvent e) {
        int x = 20;
        int y = 20;
        int cardWidth = 60;
        int cardHeight = 80;

        for (Card card : cards) {
            Rectangle bounds = new Rectangle(x, y, cardWidth, cardHeight);
            if (bounds.contains(new Point(e.getX(), e.getY()))) {
                SelectedCard = card;
                this.setVisible(false);
                return;
            }
            x += cardWidth + 10;
            if (x > 600) {
                x = 20;
                y += cardHeight + 10;
            }
        }
    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }
}
