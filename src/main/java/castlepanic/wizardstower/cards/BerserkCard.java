package castlepanic.wizardstower.cards;

import castlepanic.Game;
import castlepanic.base.cards.SpecialCard;
import castlepanic.component.Card;

public class BerserkCard extends SpecialCard {
    public BerserkCard() {
        super("Berserk");
        description = "Draw 1 card from the Castle deck for every hit card you play during the remainder of the turn.";
    }

    @Override
    public boolean play(Game game) {
        game.setDrawOnHit(true);
        return true;
    }

    @Override
    public Card clone()
    {
        return new BerserkCard();
    }
}
