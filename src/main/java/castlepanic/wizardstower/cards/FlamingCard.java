package castlepanic.wizardstower.cards;

import castlepanic.Game;
import castlepanic.base.cards.AttackCard;
import castlepanic.component.Card;

public class FlamingCard extends PlayWithHitCard {
    public FlamingCard() {
        super("Flaming");
        description = "Play this card WITH any hit card to catch the hit monster on fire.";
    }

    @Override
    protected boolean playWithHit(Game game, AttackCard card) {
        if(card.play(game)) {
            card.getChosenMonster().addFlame();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Card clone() {
        return new FlamingCard();
    }
}
