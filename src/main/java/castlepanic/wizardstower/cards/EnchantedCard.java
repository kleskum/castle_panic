package castlepanic.wizardstower.cards;

import castlepanic.Game;
import castlepanic.base.cards.AttackCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;

public class EnchantedCard extends PlayWithHitCard {
    public EnchantedCard() {
        super("Enchanted");
        description = "Play this card WITH any hit card for 2 additional points of damage to the Monster.";
    }

    @Override
    protected boolean playWithHit(Game game, AttackCard card) {
        if(card.play(game)) {
            card.getChosenMonster().damage(game.getInput(), 2);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Card clone() {
        return new EnchantedCard();
    }
}
