package castlepanic.wizardstower.cards;

import castlepanic.Game;
import castlepanic.base.cards.AttackCard;
import castlepanic.base.cards.SpecialCard;
import castlepanic.component.Card;
import castlepanic.component.Expansion;

public class StandTogetherCard extends SpecialCard {
    public StandTogetherCard() {
        super("Stand Together");
        description = "1 player of your choice may immediately play 1 hit or Wizard card.";
    }

    @Override
    public boolean play(Game game) {
        // TODO: choose a player, not very useful in a 1 player game
        game.getInput().addMessage("Choose a hit or Wizard card.");
        Card card = game.getInput().requestCardToPlay();
        if(card != null &&
            (
                (card instanceof AttackCard && card.getDescription().toLowerCase().contains("hit")) ||
                card.getExpansion() == Expansion.WizardsTower
            )
        ) {
            return game.playCard(card);
        }
        return false;
    }

    @Override
    public Card clone() {
        return new StandTogetherCard();
    }
}
