package castlepanic.wizardstower.cards;

import castlepanic.Game;
import castlepanic.base.cards.AttackCard;
import castlepanic.base.cards.SpecialCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;

public abstract class PlayWithHitCard extends SpecialCard {
    public PlayWithHitCard(String name) {
        super(name);
    }

    @Override
    public boolean play(Game game) {
        game.getInput().addMessage("Choose a hit card.");
        Card card = game.getInput().requestCardToPlay();
        if(card != null && card instanceof AttackCard && card.getDescription().toLowerCase().contains("hit")) {
            if(playWithHit(game, ((AttackCard) card))) {
                game.getCurrentPlayer().discard(card);
                return true;
            }
        }
        return false;
    }
    protected abstract boolean playWithHit(Game game, AttackCard card);
}
