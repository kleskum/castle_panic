package castlepanic.wizardstower.cards.wizard;

import castlepanic.Game;
import castlepanic.base.board.Ring;
import castlepanic.base.cards.SpecialAttackCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;
import castlepanic.component.Expansion;
import castlepanic.component.Token;
import castlepanic.wizardstower.tokens.Warlock;

import java.awt.*;

public class RainOfIronCard extends SpecialAttackCard {
    public RainOfIronCard(String[] types, Color[] colors) {
        super("Rain of Iron", types, colors);
        description = "Damage ALL Monsters in the same arc (not the Forest) for 1 point.";
        expansion = Expansion.WizardsTower;
    }

    @Override
    public boolean playSpecial(Game game, Monster monster) {
        if(monster.canHit(this)) {
            // hit all the monsters in this
            for(Ring ring : monster.getRing().getArc().getRings()) {
                if(types.contains(ring.getName())) {
                    for(int i = ring.getTokens().size()-1; i >= 0; i--) {
                        Token token = ring.getTokens().get(i);
                        if(token instanceof Monster && !(token instanceof Warlock)) {
                            ((Monster)token).damage(game.getInput(), 1);
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public Card clone() {
        return new RainOfIronCard(types.toArray(new String[0]), colors.toArray(new Color[0]));
    }
}
