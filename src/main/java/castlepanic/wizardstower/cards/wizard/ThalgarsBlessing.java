package castlepanic.wizardstower.cards.wizard;

import castlepanic.Game;
import castlepanic.base.cards.SpecialCard;
import castlepanic.component.Card;
import castlepanic.component.Expansion;
import castlepanic.component.Player;

public class ThalgarsBlessing extends SpecialCard {
    public ThalgarsBlessing() {
        super("Thalgar's Blessing");
        description = "All players draw up (from the Castle deck) to a full hand.";
        expansion = Expansion.WizardsTower;
    }

    @Override
    public boolean play(Game game) {
        game.getCurrentPlayer().getHand().remove(this);
        for(Player player : game.getPlayers()) {
            player.drawUp();
        }
        return true;
    }

    @Override
    public Card clone() {
        return new ThalgarsBlessing();
    }
}
