package castlepanic.wizardstower.cards.wizard;

import castlepanic.Game;
import castlepanic.base.board.Arc;
import castlepanic.base.board.Ring;
import castlepanic.base.cards.SpecialCard;
import castlepanic.base.tokens.FortifiedWall;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;
import castlepanic.component.Expansion;
import castlepanic.component.Token;
import castlepanic.wizardstower.tokens.Warlock;

import javax.swing.*;

public class TeleportCard extends SpecialCard {
    public TeleportCard() {
        super("Teleport");
        description = "Move any Monster in play to another space or any Fortify token in play to another Wall.";
        expansion = Expansion.WizardsTower;
    }

    @Override
    public boolean play(Game game) {
        if(game.getBoard().collectTokens(FortifiedWall.class).size() > 0 && game.getInput().askYesNoQuestion("Do you want to teleport a Fortify token?")) {
            game.getInput().addMessage("Choose a castle ring with a Fortify token.");
            Ring ring = game.getInput().chooseRing();
            if(ring != null && ring == ring.getArc().getInnerMostRing() && ring.findTokenType(FortifiedWall.class) != null) {
                game.getInput().addMessage("Choose a castle ring to teleport to.");
                Ring ring2 = game.getInput().chooseRing();
                if(ring2 != null && ring2 == ring2.getArc().getInnerMostRing() && ring2.findTokenType(FortifiedWall.class) == null) {
                    ring.findTokenType(FortifiedWall.class).place(ring2);
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        game.getInput().addMessage("Choose a token.");
        Token monster = game.getInput().chooseToken(Token.class);
        if(monster != null && !(monster instanceof Warlock)) {
            game.getInput().addMessage("Choose a space to teleport to.");
            Ring ring2 = game.getInput().chooseRing();
            if(ring2 != null) {
                monster.place(ring2);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public Card clone() {
        return new TeleportCard();
    }
}
