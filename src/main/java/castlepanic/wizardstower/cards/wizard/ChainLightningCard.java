package castlepanic.wizardstower.cards.wizard;

import castlepanic.Game;
import castlepanic.base.cards.SpecialAttackCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;
import castlepanic.component.Expansion;
import castlepanic.component.Token;
import castlepanic.wizardstower.tokens.Warlock;

import java.awt.*;

public class ChainLightningCard extends SpecialAttackCard {
    public ChainLightningCard(String[] types, Color[] colors) {
        super("Chain Lightning", types, colors);
        description = "Damage ALL Monsters in the same space for 1 point.";
        expansion = Expansion.WizardsTower;
    }

    @Override
    public boolean playSpecial(Game game, Monster monster) {
        if(monster.canHit(this)) {
            for(int i = monster.getRing().getTokens().size() - 1; i >= 0; i--) {
                Token token = monster.getRing().getTokens().get(i);
                if (token instanceof Monster && !(token instanceof Warlock)) {
                    ((Monster) token).damage(game.getInput(), 1);
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public Card clone() {
        return new ChainLightningCard(types.toArray(new String[0]), colors.toArray(new Color[0]));
    }
}
