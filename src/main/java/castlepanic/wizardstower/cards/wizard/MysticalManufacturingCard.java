package castlepanic.wizardstower.cards.wizard;

import castlepanic.Game;
import castlepanic.base.board.Ring;
import castlepanic.base.cards.MaterialCard;
import castlepanic.base.cards.SpecialCard;
import castlepanic.base.tokens.Monster;
import castlepanic.base.tokens.Tower;
import castlepanic.component.Card;
import castlepanic.component.Expansion;
import castlepanic.component.Token;

public class MysticalManufacturingCard extends SpecialCard {
    private int count;
    public MysticalManufacturingCard() {
        super("Mystical Manufacturing");
        description = "Play this card WITH 1 Brick OR 1 Mortar card to rebuild 1 destroyed Tower.";
        expansion = Expansion.WizardsTower;
    }

    @Override
    public boolean play(Game game) {
        game.getInput().addMessage("Choose a Brick or Mortar card.");
        Card card = game.getInput().requestCardToPlay();
        if(card != null && card instanceof MaterialCard) {
            MaterialCard materialCard = (MaterialCard)card;
            if(materialCard.getName().equals("Brick") || materialCard.getName().equals("Mortar")) {
                game.getInput().addMessage("Choose a castle ring to build a tower.");
                Ring ring = game.getInput().chooseRing();
                if(ring.getIndex() == ring.getArc().getRings().size() - 1) {
                    Tower tower = ring.findTokenType(Tower.class);
                    if (tower == null && ring.collectTokens(Monster.class).size() == 0) {
                        tower = new Tower();
                        tower.place(ring);
                        game.getCurrentPlayer().discard(materialCard);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public Card clone() {
        return new MysticalManufacturingCard();
    }
}
