package castlepanic.wizardstower.cards.wizard;

import castlepanic.Game;
import castlepanic.base.board.Ring;
import castlepanic.base.cards.SpecialAttackCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;
import castlepanic.component.Expansion;
import castlepanic.wizardstower.tokens.Warlock;

import java.awt.*;

public class LightningBoltCard extends SpecialAttackCard {

    public LightningBoltCard(String[] types, Color[] colors) {
        super("Lightning Bolt", types, colors);
        description = "Damage 1 Monster for 1 point, and then move that Monster to any arc in the Forest.";
        expansion = Expansion.WizardsTower;
    }

    public boolean playSpecial(Game game, Monster monster) {
        if(monster.canHit(this) && !(monster instanceof Warlock)) {
            monster.damage(game.getInput(), 1);
            game.getInput().redraw();

            if(monster.getHealth() > 0) {
                Ring ring = null;
                game.getInput().addMessage("Choose a forest ring to place the " + monster.getName());
                while (ring == null) {
                    ring = game.getInput().chooseRing();
                    if(ring.getIndex() != 0) {
                        ring = null; // must be forest ring
                    }
                }
                monster.place(ring);
            }
            return true;
        }
        return false;
    }

    @Override
    public Card clone() {
        return new LightningBoltCard(types.toArray(new String[0]), colors.toArray(new Color[0]));
    }
}
