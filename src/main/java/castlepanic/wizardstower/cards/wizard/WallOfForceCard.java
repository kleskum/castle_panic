package castlepanic.wizardstower.cards.wizard;

import castlepanic.Game;
import castlepanic.base.board.Ring;
import castlepanic.base.cards.SpecialAttackCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;
import castlepanic.component.Expansion;
import castlepanic.component.Token;
import castlepanic.wizardstower.tokens.Warlock;

import java.awt.*;

public class WallOfForceCard extends SpecialAttackCard {
    public WallOfForceCard(String[] types, Color[] colors) {
        super("Wall of Force", types, colors);
        description = "Move ALL Monsters in 1 arc back to the Forest";
        expansion = Expansion.WizardsTower;
    }

    @Override
    public boolean playSpecial(Game game, Monster monster) {
        if(monster.canHit(this) && !(monster instanceof Warlock)) {
            for(Ring ring : monster.getRing().getArc().getRings()) {
                if(types.contains(ring.getName())) {
                    for(int i = ring.getTokens().size()-1; i >= 0; i--) {
                        Token token = ring.getTokens().get(i);
                        if(token instanceof Monster && !(token instanceof Warlock)) {
                            Monster m = (Monster)token;
                            m.place(monster.getRing().getArc().getRings().get(0));
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public Card clone() {
        return new WallOfForceCard(types.toArray(new String[0]), colors.toArray(new Color[0]));
    }
}
