package castlepanic.wizardstower.cards.wizard;

import castlepanic.Game;
import castlepanic.base.board.Arc;
import castlepanic.base.board.Ring;
import castlepanic.base.cards.SpecialAttackCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;
import castlepanic.component.Expansion;
import castlepanic.component.Token;
import castlepanic.wizardstower.tokens.Warlock;

import java.awt.*;

public class WarStormCard extends SpecialAttackCard {
    public WarStormCard(String[] types, Color[] colors) {
        super("War Storm", types, colors);
        description = "Damage ALL Monsters in the Archer, Knight, AND Swordsman rings of 1 color for 1 point.";
        expansion = Expansion.WizardsTower;
    }

    @Override
    public boolean playSpecial(Game game, Monster monster) {
        if(monster.canHit(this) && !(monster instanceof Warlock)) {
            Color color = monster.getRing().getArc().getColor();
            for(Arc arc : monster.getRing().getArc().getBoard().getArcs()) {
                if (arc.getColor().equals(color)) {
                    for (Ring ring : arc.getRings()) {
                        for(int i = ring.getTokens().size()-1; i >= 0; i--) {
                            Token token = ring.getTokens().get(i);
                            if(token instanceof Monster && !(token instanceof Warlock)) {
                                ((Monster)token).damage(game.getInput(), 1);
                            }
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public Card clone() {
        return new WarStormCard(types.toArray(new String[0]), colors.toArray(new Color[0]));
    }
}
