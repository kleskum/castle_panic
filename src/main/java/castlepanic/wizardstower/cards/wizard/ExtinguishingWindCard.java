package castlepanic.wizardstower.cards.wizard;

import castlepanic.Game;
import castlepanic.MoveDirection;
import castlepanic.base.board.Arc;
import castlepanic.base.board.Ring;
import castlepanic.base.cards.SpecialCard;
import castlepanic.base.tokens.Monster;
import castlepanic.base.tokens.Structure;
import castlepanic.component.Card;
import castlepanic.component.Expansion;
import castlepanic.component.Token;
import castlepanic.wizardstower.tokens.Warlock;

public class ExtinguishingWindCard extends SpecialCard {
    public ExtinguishingWindCard() {
        super("Extinguishing Wind");
        description = "Remove ALL flame tokens from ALL Walls, Towers, and Monsters, AND move ALL Monsters back 1 space";
        expansion = Expansion.WizardsTower;
    }

    @Override
    public boolean play(Game game) {
        // remove all flames
        for(Arc arc : game.getBoard().getArcs()) {
            for(Ring ring : arc.getRings()) {
                for(int i = ring.getTokens().size()-1; i >= 0; i--) {
                    Token token = ring.getTokens().get(i);
                    if(token instanceof Monster && !(token instanceof Warlock)) {
                        ((Monster)token).removeFlames();
                        ((Monster)token).move(game.getInput(), MoveDirection.Back);
                    } else if(token instanceof Structure) {
                        ((Structure)token).removeFlames();
                    }
                }
            }
        }
        return true;
    }

    @Override
    public Card clone() {
        return new ExtinguishingWindCard();
    }
}
