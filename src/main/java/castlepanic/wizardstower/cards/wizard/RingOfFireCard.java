package castlepanic.wizardstower.cards.wizard;

import castlepanic.Game;
import castlepanic.base.board.Arc;
import castlepanic.base.board.Ring;
import castlepanic.base.cards.SpecialAttackCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;
import castlepanic.component.Expansion;
import castlepanic.component.Token;
import castlepanic.wizardstower.tokens.Warlock;

import java.awt.*;

public class RingOfFireCard extends SpecialAttackCard {
    public RingOfFireCard(String type, Color[] colors) {
        super("Ring of Fire", new String[] { type }, colors);
        description = "Catch ALL Monsters on fire in ALL colors of the " + type + " ring.";
        expansion = Expansion.WizardsTower;
    }

    @Override
    public boolean playSpecial(Game game, Monster monster) {
        if(monster.canHit(this)) {
            // place all monsters in same ring on fire
            for(Arc arc : monster.getRing().getArc().getBoard().getArcs()) {
                for(Ring ring : arc.getRings()) {
                    if(ring.getName().equals(types.get(0))) {
                        for (Token token : ring.getTokens()) {
                            if (token instanceof Monster && !(token instanceof Warlock)) {
                                ((Monster) token).addFlame();
                            }
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public Card clone() {
        return new RingOfFireCard(types.get(0), colors.toArray(new Color[0]));
    }
}
