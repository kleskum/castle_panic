package castlepanic.wizardstower.cards.wizard;

import castlepanic.Game;
import castlepanic.base.cards.SpecialAttackCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;
import castlepanic.component.Expansion;
import castlepanic.component.Token;
import castlepanic.wizardstower.tokens.Warlock;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HypnotizeCard extends SpecialAttackCard {

    private Random r = new Random();

    public HypnotizeCard(String[] types, Color[] colors) {
        super("Hypnotize", types, colors);
        description = "2 Monsters in the same space (not the Forest) attack each other simultaneously.";
        expansion = Expansion.WizardsTower;
    }

    @Override
    public boolean playSpecial(Game game, Monster monster) {
        // count monsters in ring
        List<Monster> monsters = new ArrayList<Monster>();
        for(Token token : monster.getRing().getTokens()) {
            if(token instanceof Monster && token != monster && !(token instanceof Warlock)) {
                monsters.add(((Monster)token));
            }
        }
        if (monster.canHit(this) && monsters.size() > 0 && !(monster instanceof Warlock)) {
            Monster monster1 = monster;
            Monster monster2 = null;
            game.getInput().addMessage("Choose the monster for the " + monster1.getName() + " to attack.");
            while(monster2 == null) {
                monster2 = game.getInput().chooseToken(Monster.class);
                if(monster2 == monster1 || monster2 instanceof Warlock) {
                    monster2 = null;
                }
            }
            // now have them fight!
            int health1 = monster1.getHealth();
            int health2 = monster2.getHealth();

            monster1.damage(game.getInput(), health2);
            monster2.damage(game.getInput(), health1);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Card clone() {
        return new HypnotizeCard(types.toArray(new String[0]), colors.toArray(new Color[0]));
    }
}
