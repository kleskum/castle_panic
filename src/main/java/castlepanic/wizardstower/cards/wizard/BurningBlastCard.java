package castlepanic.wizardstower.cards.wizard;

import castlepanic.Game;
import castlepanic.base.cards.SpecialAttackCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;
import castlepanic.component.Expansion;
import castlepanic.component.Token;
import castlepanic.wizardstower.tokens.Warlock;

import java.awt.*;

public class BurningBlastCard extends SpecialAttackCard {
    public BurningBlastCard(String[] types, Color[] colors) {
        super("Burning Blast", types, colors);
        description = "Set ALL Monsters in the same space on fire.";
        expansion = Expansion.WizardsTower;
    }

    @Override
    public boolean playSpecial(Game game, Monster monster) {
        if(monster.canHit(this) && !(monster instanceof Warlock)) {
            // place all monsters in same space on fire
            for (Token token : monster.getRing().getTokens()) {
                if (token instanceof Monster) {
                    ((Monster) token).addFlame();
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public Card clone() {
        return new BurningBlastCard(types.toArray(new String[0]), colors.toArray(new Color[0]));
    }
}
