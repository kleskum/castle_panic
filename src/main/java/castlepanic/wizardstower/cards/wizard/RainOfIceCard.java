package castlepanic.wizardstower.cards.wizard;

import castlepanic.Game;
import castlepanic.base.board.Arc;
import castlepanic.base.board.Ring;
import castlepanic.base.cards.SpecialCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;
import castlepanic.component.Expansion;
import castlepanic.component.Token;
import castlepanic.wizardstower.tokens.Warlock;

public class RainOfIceCard extends SpecialCard {
    public RainOfIceCard() {
        super("Rain of Ice");
        description = "No Monsters move this turn.  Remove ALL flame tokens from ALL Monsters.";
        expansion = Expansion.WizardsTower;
    }

    @Override
    public boolean play(Game game) {
        for(Arc arc : game.getBoard().getArcs()) {
            for(Ring ring : arc.getRings()) {
                for(Token token : ring.getTokens()) {
                    if(token instanceof Monster && !(token instanceof Warlock)) {
                        ((Monster)token).removeFlames();
                        ((Monster)token).setTarred(true);
                    }
                }
            }
        }
        return true;
    }

    @Override
    public Card clone() {
        return new RainOfIceCard();
    }
}
