package castlepanic.wizardstower.cards.wizard;

import castlepanic.Game;
import castlepanic.base.board.Ring;
import castlepanic.base.cards.MaterialCard;
import castlepanic.base.cards.SpecialCard;
import castlepanic.base.tokens.Wall;
import castlepanic.component.Card;
import castlepanic.component.Expansion;
import castlepanic.component.Token;

public class ArcaneAssemblyCard extends SpecialCard {

    public ArcaneAssemblyCard() {
        super("Arcane Assembly");
        description = "All players may immediately build Walls at the cost of 1 Brick OR 1 Mortar per Wall.";
        expansion = Expansion.WizardsTower;
    }

    @Override
    public boolean play(Game game) {
        while(!game.getInput().getNoSelection()) {
            game.getInput().addMessage("Choose a brick or mortar card.");
            Card card = game.getInput().requestCardToPlay();
            if(card != null && card instanceof MaterialCard) {
                MaterialCard materialCard = (MaterialCard)card;
                if(materialCard.getName().equals("Brick") || materialCard.getName().equals("Mortar")) {
                    game.getInput().addMessage("Choose a castle ring to build a wall.");
                    Ring ring = game.getInput().chooseRing();
                    if(ring.getIndex() == ring.getArc().getRings().size() - 1) {
                        Wall wall = ring.findTokenType(Wall.class);
                        if (wall == null) {
                            wall = new Wall();
                            wall.place(ring);
                            game.getCurrentPlayer().discard(materialCard);
                            game.getInput().redraw();
                            continue;
                        }
                    }
                }
            }
            game.getInput().addMessage("You can't do that.");
        }
        return true;
    }

    @Override
    public Card clone() {
        return new ArcaneAssemblyCard();
    }
}
