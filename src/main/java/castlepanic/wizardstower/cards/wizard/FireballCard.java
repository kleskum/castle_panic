package castlepanic.wizardstower.cards.wizard;

import castlepanic.Game;
import castlepanic.base.cards.SpecialAttackCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;
import castlepanic.component.Expansion;
import castlepanic.wizardstower.tokens.Warlock;

import java.awt.*;

public class FireballCard extends SpecialAttackCard {
    private String colorName;

    public FireballCard(String colorName, String[] types, Color color) {
        super(colorName + " Fireball", types, new Color[] { color });
        this.color = color;
        this.colorName = colorName;
        name = "Fireball";
        description = "Damage 1 Monster in any ring of the " + colorName + " arc for 1 point and catch that Monster on fire.";
        expansion = Expansion.WizardsTower;
    }

    @Override
    public boolean playSpecial(Game game, Monster monster) {
        if (monster.canHit(this) && !(monster instanceof Warlock)) {
            monster.damage(game.getInput(), 1);
            monster.addFlame();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Card clone() {
        return new FireballCard(colorName, types.toArray(new String[0]), color);
    }
}
