package castlepanic.wizardstower.cards.wizard;

import castlepanic.Game;
import castlepanic.base.cards.SpecialCard;
import castlepanic.component.Card;
import castlepanic.component.Expansion;

import java.util.ArrayList;
import java.util.List;

public class EyeOfTheOracleCard extends SpecialCard {
    private int count;
    public EyeOfTheOracleCard(int count) {
        super("Eye of the Oracle");
        this.count = count;
        description = "Draw the top " + count + " cards from Castle deck, keep 1, " +
                "and return the rest to the top of the deck in any order";
        expansion = Expansion.WizardsTower;
    }

    @Override
    public boolean play(Game game) {
        List<Card> cards = new ArrayList<Card>();
        for(int i = 0; i < count; i++) {
            cards.add(game.getDeck().popFromDraw());
        }

        // choose a card to keep
        game.getInput().addMessage("Choose 1 card to keep.");
        Card card = game.getInput().chooseCard(cards);
        if (card != null) {
            game.getCurrentPlayer().getHand().add(card);

            // now return the rest of the cards to the deck
            cards.remove(card);
            while(cards.size() > 0) {
                game.getInput().addMessage("Choose next card to place on deck.");
                card = game.getInput().chooseCard(cards);
                if(card != null) {
                    game.getDeck().pushOnDraw(card);
                    cards.remove(card);
                }
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public Card clone() {
        return new EyeOfTheOracleCard(count);
    }
}
