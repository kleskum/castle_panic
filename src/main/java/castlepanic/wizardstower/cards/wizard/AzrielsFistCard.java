package castlepanic.wizardstower.cards.wizard;

import castlepanic.Game;
import castlepanic.base.cards.SpecialAttackCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;
import castlepanic.component.Expansion;
import castlepanic.wizardstower.tokens.Warlock;

import java.awt.*;

public class AzrielsFistCard extends SpecialAttackCard {
    public AzrielsFistCard(String[] types, Color[] colors) {
        super("Azriel's Fist", types, colors);
        description = "Damage 1 Monster anywhere on the board for 1 point.";
        expansion = Expansion.WizardsTower;
    }

    @Override
    public boolean playSpecial(Game game, Monster monster) {
        if(monster.canHit(this) && !(monster instanceof Warlock)) {
            monster.damage(game.getInput(), 1);
            return true;
        }
        return false;
    }

    @Override
    public Card clone() {
        return new AzrielsFistCard(getTypes().toArray(new String[0]), getColors().toArray(new Color[0]));
    }
}
