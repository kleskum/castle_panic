package castlepanic.wizardstower.cards.wizard;

import castlepanic.Game;
import castlepanic.base.board.Arc;
import castlepanic.base.board.Ring;
import castlepanic.base.cards.SpecialCard;
import castlepanic.base.tokens.Monster;
import castlepanic.base.tokens.Tower;
import castlepanic.component.Card;
import castlepanic.component.Expansion;
import castlepanic.component.Token;
import castlepanic.wizardstower.tokens.Warlock;

public class WizardQuakeCard extends SpecialCard {
    public WizardQuakeCard() {
        super("Wizard Quake");
        description = "Destroy 1 Tower and slay ALL Monsters in the same arc as that Tower.";
        expansion = Expansion.WizardsTower;
    }

    @Override
    public boolean play(Game game) {
        game.getInput().addMessage("Choose a castle ring with a Tower token.");
        Ring castlering = game.getInput().chooseRing();
        if(castlering != null) {
            Tower tower = castlering.findTokenType(Tower.class);
            if(tower != null) {
                // destroy the tower!
                Arc arc = tower.getRing().getArc();
                tower.destroy(game.getInput());

                // now slay the monsters
                for(Ring ring : arc.getRings()) {
                    for(int i = ring.getTokens().size()-1; i >= 0; i--) {
                        Token token = ring.getTokens().get(i);
                        if(token instanceof Monster && !(token instanceof Warlock)) {
                            ((Monster)token).slay(game.getInput());
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }


    @Override
    public Card clone() {
        return new WizardQuakeCard();
    }
}
