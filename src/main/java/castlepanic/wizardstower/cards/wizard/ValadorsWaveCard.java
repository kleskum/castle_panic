package castlepanic.wizardstower.cards.wizard;

import castlepanic.Game;
import castlepanic.base.board.Arc;
import castlepanic.base.board.Ring;
import castlepanic.base.cards.SpecialAttackCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;
import castlepanic.component.Expansion;
import castlepanic.component.Token;
import castlepanic.wizardstower.tokens.Warlock;

import java.awt.*;

public class ValadorsWaveCard extends SpecialAttackCard {
    private int count;
    public ValadorsWaveCard(int count, String[] types, Color[] colors) {
        super("Valador's Wave", types, colors);
        this.count = count;
        description = "Deliver " + count + " points of damage in one color. " +
                "Distribute damage among as many Monsters as you choose.";
        expansion = Expansion.WizardsTower;
    }

    private boolean hasMonsters(Game game, Color color) {
        for(Arc arc : game.getBoard().getArcs()) {
            if(arc.getColor().equals(color)) {
                for (Ring ring : arc.getRings()) {
                    for (Token token : ring.getTokens()) {
                        if(token instanceof Monster) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override
    public boolean playSpecial(Game game, Monster monster) {
        if(monster.canHit(this) && !(monster instanceof Warlock)) {
            // hit this monster first
            Color color = monster.getRing().getArc().getColor();
            monster.hit(game.getInput());
            int total = count - 1;
            while(total > 0 && hasMonsters(game, color)) {
                game.getInput().addMessage("Choose a monster in same color to use next damge on (" + total + " remaining).");
                game.getInput().redraw();

                // now keep hitting until all damage used up
                Monster m = game.getInput().chooseToken(Monster.class);
                if(m != null && !(m instanceof Warlock)) {
                    // make sure same color
                    if(m.getRing().getArc().getColor().equals(color)) {
                        m.damage(game.getInput(), 1);
                        total--;
                    }
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public Card clone() {
        return new ValadorsWaveCard(count, types.toArray(new String[0]), colors.toArray(new Color[0]));
    }
}
