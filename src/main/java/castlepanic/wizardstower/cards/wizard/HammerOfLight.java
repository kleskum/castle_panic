package castlepanic.wizardstower.cards.wizard;

import castlepanic.Game;
import castlepanic.base.cards.SpecialAttackCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;
import castlepanic.component.Expansion;
import castlepanic.wizardstower.tokens.Warlock;

import java.awt.*;

public class HammerOfLight extends SpecialAttackCard {

    public HammerOfLight(String type, Color[] colors) {
        super("Hammer of Light", new String[] { type }, colors);
        description = "Slay 1 Monster in the " + type + " ring.";
        expansion = Expansion.WizardsTower;
    }

    @Override
    public boolean playSpecial(Game game, Monster monster) {
        if (monster.canHit(this) && !(monster instanceof Warlock)) {
            monster.slay(game.getInput());
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Card clone() {
        return new HammerOfLight(types.get(0), colors.toArray(new Color[0]));
    }
}
