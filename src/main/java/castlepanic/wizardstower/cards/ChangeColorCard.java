package castlepanic.wizardstower.cards;

import castlepanic.Game;
import castlepanic.base.cards.AttackCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;

import java.awt.*;

public class ChangeColorCard extends PlayWithHitCard {
    public ChangeColorCard() {
        super("Change Color");
        description = "Play this card WITH any hit card to change the color of the hit card.";
    }

    @Override
    protected boolean playWithHit(Game game, AttackCard card) {
        AttackCard attackCard = (AttackCard)card.clone(); // manipulate the original card
        if(!attackCard.play(game) && attackCard.getChosenMonster() != null) {
            attackCard.getColors().add(Color.red);
            attackCard.getColors().add(Color.blue);
            attackCard.getColors().add(Color.green);
            return attackCard.play(game, attackCard.getChosenMonster());
        } else {
            return attackCard.getChosenMonster() != null;
        }
    }

    @Override
    public Card clone() {
        return new ChangeColorCard();
    }
}
