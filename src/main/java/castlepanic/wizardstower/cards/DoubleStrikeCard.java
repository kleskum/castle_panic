package castlepanic.wizardstower.cards;

import castlepanic.Game;
import castlepanic.base.cards.AttackCard;
import castlepanic.base.cards.SpecialAttackCard;
import castlepanic.base.cards.SpecialCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;
import castlepanic.component.Expansion;

public class DoubleStrikeCard extends SpecialCard {
    public DoubleStrikeCard() {
        super("Double Strike");
        description = "Play 1 hit card (not a Special or Wizard card) twice this turn.";
    }

    @Override
    public boolean play(Game game) {
        game.getInput().addMessage("Choose a hit card.");
        Card card = game.getInput().requestCardToPlay();
        if(card != null && card instanceof AttackCard && card.getDescription().toLowerCase().contains("hit")
                && !(card instanceof SpecialAttackCard) && card.getExpansion() != Expansion.WizardsTower) {
            AttackCard attackCard = (AttackCard)card;
            game.getInput().addMessage("Choose a monster to hit.");
            Monster monster = game.getInput().chooseToken(Monster.class);
            if(monster != null) {
                if(!attackCard.play(game, monster)) {
                    return false;
                }
                game.getInput().redraw();
                monster = null;
                while(monster == null || !monster.canHit(attackCard)) {
                    game.getInput().addMessage("Choose a monster to hit.");
                    monster = game.getInput().chooseToken(Monster.class);
                }
                attackCard.play(game, monster);
                game.getCurrentPlayer().discard(attackCard);
                return true;
            }
        }
        return false;
    }

    @Override
    public Card clone() {
        return new DoubleStrikeCard();
    }
}
