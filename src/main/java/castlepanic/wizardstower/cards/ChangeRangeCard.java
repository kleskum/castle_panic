package castlepanic.wizardstower.cards;

import castlepanic.Game;
import castlepanic.base.cards.AttackCard;
import castlepanic.base.cards.SpecialCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;

public class ChangeRangeCard extends PlayWithHitCard {
    public ChangeRangeCard() {
        super("Change Range");
        description = "Play this card WITH any hit card to change the ring of the hit card to Archer, Knight, or Swordsman.";
    }

    @Override
    protected boolean playWithHit(Game game, AttackCard card) {
        AttackCard attackCard = (AttackCard)card.clone(); // manipulate the original card
        if(!attackCard.play(game) && attackCard.getChosenMonster() != null) {
            attackCard.getTypes().add("Swordsmen");
            attackCard.getTypes().add("Knights");
            attackCard.getTypes().add("Archers");
            return attackCard.play(game, attackCard.getChosenMonster());
        } else {
            return attackCard.getChosenMonster() != null;
        }
    }

    @Override
    public Card clone() {
        return new ChangeRangeCard();
    }
}
