package castlepanic.wizardstower.cards;

import castlepanic.Game;
import castlepanic.MoveDirection;
import castlepanic.base.cards.AttackCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;

public class KnockBackCard extends PlayWithHitCard {
    public KnockBackCard() {
        super("Knock Back");
        description = "Play this card WITH a hit card to move the hit Monster back 1 space AFTER damaging it.";
    }

    @Override
    protected boolean playWithHit(Game game, AttackCard card) {
        if(card.play(game)) {
            if (card.getChosenMonster().getHealth() > 0) {
                card.getChosenMonster().move(game.getInput(), MoveDirection.Back);
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Card clone() {
        return new KnockBackCard();
    }
}
