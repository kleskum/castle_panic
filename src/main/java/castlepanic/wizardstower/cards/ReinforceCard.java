package castlepanic.wizardstower.cards;

import castlepanic.Game;
import castlepanic.base.cards.SpecialCard;
import castlepanic.component.Card;
import castlepanic.component.Player;

import javax.swing.*;

public class ReinforceCard extends SpecialCard {
    public ReinforceCard() {
        super("Reinforce");
        description = "Each player immediately draws 1 card of his or her choice from either the Castle or Wizard deck.";
    }

    @Override
    public boolean play(Game game) {
        for(Player player : game.getPlayers()) {
            int result = -1;
            if(game.hasWizardTower() && (game.getWizardDeck().getDrawCount()+game.getWizardDeck().getDiscardCount()) > 0 ) {
                result = JOptionPane.showConfirmDialog(null,
                        "Do you want to draw from the wizard deck?",
                        "Wizard Deck",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
            }
            if(result== 0) {
                player.drawWizard();
            } else {
                player.draw();
            }
        }
        return true;
    }

    @Override
    public Card clone() {
        return new ReinforceCard();
    }
}
