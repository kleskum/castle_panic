package castlepanic.wizardstower.cards;

import castlepanic.Game;
import castlepanic.base.cards.SpecialCard;
import castlepanic.component.Card;

public class NeverLoseHopeCard extends SpecialCard {
    public NeverLoseHopeCard() {
        super("Never Lose Hope");
        description = "Immediately discard as many cards as you wish.  For every card you discard, draw 1 Castle card.";
    }

    @Override
    public boolean play(Game game) {
        int count = 0;
        while(!game.getInput().getNoSelection()) {
            game.getInput().addMessage("Choose card to discard or cancel.");
            Card card = game.getInput().requestCard();
            if(card != null && card != this) {
                game.getCurrentPlayer().discard(card);
                game.getInput().redraw();
                count++;
            }
        }
        for(int i = 0; i < count; i++) {
            game.getCurrentPlayer().draw();
        }
        return true;
    }

    @Override
    public Card clone() {
        return new NeverLoseHopeCard();
    }
}
