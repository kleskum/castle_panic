package castlepanic.wizardstower.tokens;

import castlepanic.Game;
import castlepanic.Input;
import castlepanic.MoveDirection;
import castlepanic.base.board.Arc;
import castlepanic.base.tokens.MonsterBoss;
import castlepanic.component.Token;

import java.util.List;

public class Chimera extends MonsterBoss implements MegaMonsterBoss {
    // need the game for fire breathing
    public Chimera(int health) {
        super("Chimera", health);
        description = "<html><b>" + name + "</b><br>When Drawn:</b< The " + name + " breathes fire when placed on the board.<br>" +
                "<b>In Play:</b> The " + name + " moves 1 space counter-clockwise and then 1 space toward the Castle.<br>" +
                "&nbsp;&nbsp;Immediately after moving, the " + name + " breathes fire.  The " + name + " does not breath fire if it does not move.<br>" +
                "&nbsp;&nbsp;Inside the Castle ring, the " + name + " moves by the standard rules and no longer breathes fire.</html>";

    }

    protected void breathFire(Input input) {
        FireBreath fireBreath = new FireBreath();
        if(getRing() != null) {
            fireBreath.startFromRing(input, getRing());
        }
    }

    @Override
    public void doAction(Game game) {
        breathFire(game.getInput());
    }

    @Override
    public boolean move(Input input) {
        if(getRing().getIndex() == getRing().getArc().getRings().size()-1) {
            return super.move(input);
        } else {
            move(input, MoveDirection.CounterClockwise);
            if (move(input, MoveDirection.Normal)) {
                breathFire(input);
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public Token clone() {
        return new Chimera(maxHealth);
    }
}
