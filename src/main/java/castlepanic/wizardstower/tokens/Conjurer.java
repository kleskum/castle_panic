package castlepanic.wizardstower.tokens;

import castlepanic.Game;
import castlepanic.base.tokens.GroundMonster;
import castlepanic.base.tokens.Monster;
import castlepanic.base.tokens.MonsterBoss;
import castlepanic.component.Token;

import java.awt.*;

public class Conjurer extends MonsterBoss {
    public Conjurer(int health) {
        super("Conjurer", health);
    }

    @Override
    public void doAction(Game game) {
        int numImps = game.getDie().roll(game.getInput());
        game.getInput().addMessage("Conjured up " + numImps + " Imps.");
        for(int i = 0; i < numImps; i++) {
            Monster imp = new GroundMonster("Imp", 1, Color.magenta);
            imp.place(game.getBoard().getArcs().get(i).getRings().get(0));
        }
    }

    @Override
    public Token clone() {
        return new Conjurer(maxHealth);
    }
}
