package castlepanic.wizardstower.tokens;

import castlepanic.Game;
import castlepanic.Input;
import castlepanic.MoveDirection;
import castlepanic.SlayListener;
import castlepanic.base.cards.AttackCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Token;

import java.awt.*;

public class Doppelganger extends Monster implements SlayListener {
    public Doppelganger() {
        super("Doppelganger", 0, Color.orange.darker());
    }

    @Override
    public void doAction(Game game) {
        game.getSlayListeners().add(this);
    }

    @Override
    public int damage(Input input, int damage) {
        return 0;
    }

    @Override
    public void heal() {

    }

    @Override
    public boolean move(Input input, MoveDirection direction) {
        return true;
    }

    @Override
    public boolean canHit(AttackCard attackCard) {
        return false;
    }

    @Override
    public Token clone() {
        return new Doppelganger();
    }

    public void monsterSlayed(Game game, Monster monster) {
        if(!(monster instanceof MegaMonsterBoss)) {
            // replace this doppelganger with the other monster
            monster.clone().place(getRing());
            monster.doAction(game);
            getRing().removeToken(this);
            game.getSlayListeners().remove(this); // stop listening
        }
    }
}
