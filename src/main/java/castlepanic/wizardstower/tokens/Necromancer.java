package castlepanic.wizardstower.tokens;

import castlepanic.Game;
import castlepanic.Input;
import castlepanic.base.board.Ring;
import castlepanic.base.tokens.Monster;
import castlepanic.base.tokens.MonsterBoss;
import castlepanic.base.tokens.Tower;
import castlepanic.base.tokens.Wall;
import castlepanic.component.Bag;
import castlepanic.component.Token;

public class Necromancer extends MonsterBoss implements MegaMonsterBoss {
    private Bag bag;

    public Necromancer(Bag bag, int health) {
        super("Necromancer", health);
        this.bag = bag;
        description = "<html><b>" + name + "<b><br><b>When Drawn:</b> Draw 2 Monsters (not Monster effects) randomly from the Monster discard pile<br>" +
                "&nbsp;&nbsp;and move them, facedown, to the regular Monster draw pile.<br>" +
                "<b>In Play:</b> If the " + name + " attacks a structure, he is immediately slayed.  For every damage point the<br>" +
                "&nbsp;&nbsp;" + name + " had remaining when it attacked the structure, 1 Monster (not a Monster effect) is drawn randomly from the<br>" +
                "&nbsp;&nbsp;discard pile and placed in the Monster draw pile.  If there are fewer Monsters in the discard pile than damage points<br>" +
                "&nbsp;&nbsp;remaining on the " + name + ", draw as many Monsters as possible.  Monsters that were destroyed at the same time as the<br>" +
                "&nbsp;&nbsp;" + name + " may be drawn from the discard pile.</html>";
    }

    @Override
    public void doAction(Game game) {
        // draw 2 monsters from the discard pile
        drawDiscardedMonsters(game.getBag(), 2);
    }

    private void drawDiscardedMonsters(Bag bag, int count) {
        for(int i = 0; i < count; i++) {
            Monster monster = bag.pickRandomDicardedMonster();
            if (monster != null) {
                bag.addToken(monster.clone());
            }
        }
    }

    @Override
    protected Ring moveIntoWall(Input input, Ring nextRing, Wall wall) {
        int count = getHealth();
        Ring ring = super.moveIntoWall(input, nextRing, wall);
        slay(input);
        drawDiscardedMonsters(bag, count);
        return ring;
    }

    @Override
    protected void moveIntoTower(Input input, Ring nextRing, Tower tower) {
        int count = getHealth();
        super.moveIntoTower(input, nextRing, tower);
        slay(input);
        drawDiscardedMonsters(bag, count);
    }

    @Override
    public Token clone() {
        return new Necromancer(bag, maxHealth);
    }
}
