package castlepanic.wizardstower.tokens;

import castlepanic.Input;
import castlepanic.base.cards.AttackCard;
import castlepanic.base.tokens.GroundMonster;
import castlepanic.component.Token;

public class SlayAndMissMonster extends GroundMonster {
    private String typeMiss;
    private String typeSlay;

    public SlayAndMissMonster(String name, int health, String typeMiss, String typeSlay) {
        super(name, health);
        this.typeMiss = typeMiss;
        this.typeSlay = typeSlay;
        description = "<html><b>" + name + "</b><br>If the " + name + " is damaged while it is in the " + typeSlay + " ring, it " +
                "is immediately slayed no matter how many damage points it has remaining.<br>" +
                "The " + name + " takes no damage from hit cards while in the " + typeMiss + " ring.</html>";
    }

    @Override
    public boolean canHit(AttackCard attackCard) {
        if(getRing().getName().equals(typeMiss) && attackCard.getDescription().toLowerCase().contains("hit")) {
            return false;
        } else {
            return super.canHit(attackCard);
        }
    }

    @Override
    public int damage(Input input, int damage) {
        if(getRing().getName().equals(typeSlay)) {
            return super.damage(input, getHealth());
        } else {
            return super.damage(input, damage);
        }
    }

    @Override
    public Token clone() {
        return new SlayAndMissMonster(name, maxHealth, typeMiss, typeSlay);
    }
}
