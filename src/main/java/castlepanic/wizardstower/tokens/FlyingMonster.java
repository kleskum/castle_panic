package castlepanic.wizardstower.tokens;

import castlepanic.Game;
import castlepanic.base.cards.AttackCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Token;

import java.awt.*;

public class FlyingMonster extends Monster {

    public FlyingMonster(String name, int health) {
        super(name, health, Color.cyan);
    }

    @Override
    public boolean canHit(AttackCard attackCard) {
        return (attackCard.getTypes().contains("Archers")) &&
                attackCard.containsColor(getRing().getArc().getColor()) &&
                ring != ring.getArc().getOuterMostRing() && // make sure not in the forest
                ring != ring.getArc().getInnerMostRing(); // make sure not in the castle
    }

    @Override
    public void doAction(Game game) {
        // do nothing
    }

    @Override
    public Token clone() {
        return new FlyingMonster(name, maxHealth);
    }
}
