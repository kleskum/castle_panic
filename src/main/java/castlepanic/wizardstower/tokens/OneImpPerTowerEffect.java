package castlepanic.wizardstower.tokens;

import castlepanic.Game;
import castlepanic.base.board.Arc;
import castlepanic.base.board.Ring;
import castlepanic.base.tokens.Effect;
import castlepanic.base.tokens.GroundMonster;
import castlepanic.base.tokens.Monster;
import castlepanic.base.tokens.Tower;
import castlepanic.component.Token;

import java.awt.*;

public class OneImpPerTowerEffect extends Effect {
    public OneImpPerTowerEffect() {
        super("1 Imp per Tower");
    }

    private Tower getTowerToken(Ring ring) {
        for(Token token : ring.getTokens()) {
            if(token instanceof Tower) {
                return ((Tower)token);
            }
        }
        return null;
    }

    @Override
    public void doAction(Game game) {
        for(Arc arc : game.getBoard().getArcs()) {
            Tower tower = getTowerToken(arc.getRings().get(arc.getRings().size() - 1));
            if(tower != null) {
                Monster imp = new GroundMonster("Imp", 1, Color.magenta);
                imp.place(arc.getRings().get(0));
            }
        }
    }

    @Override
    public Token clone()
    {
        return new OneImpPerTowerEffect();
    }
}
