package castlepanic.wizardstower.tokens;

import castlepanic.Input;
import castlepanic.MoveDirection;
import castlepanic.base.tokens.GroundMonster;
import castlepanic.component.Token;

public class CavalryMonster extends GroundMonster {
    public CavalryMonster(String name, int health) {
        super(name, health);
        description = "<html><b>" + name  + "</b><br>This Monster moves 2 spaces at a time.  (If moved by another Monster token,\n" +
                "the " + name + " also moves 2 spaces.)<br>Inside the Castle ring the " + name + "\n" +
                "moves by standard rules.<br>If caught on fire, the " + name + " takes 1 point of damage\n" +
                "from each flame token after moving, not 2 per token.";
    }

    @Override
    public boolean move(Input input) {
        boolean result = move(input, MoveDirection.Normal);
        if(getRing() != null && getRing().getIndex() != getRing().getArc().getRings().size()-1) {
            return super.move(input);
        } else {
            return result;
        }
    }

    @Override
    public Token clone() {
        return new CavalryMonster(name, maxHealth);
    }
}
