package castlepanic.wizardstower.tokens;

import castlepanic.Game;
import castlepanic.Input;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Token;
import castlepanic.darktitan.tokens.Cavalier;
import castlepanic.darktitan.tokens.Hero;

public class Phoenix extends FlyingMonster {
    public Phoenix(int health) {
        super("Phoenix", health);
    }

    @Override
    public void doAction(Game game) {
    }

    @Override
    public int damage(Input input, int damage) {
        castlepanic.base.board.Ring myRing = this.getRing();
        int hit = super.damage(input, damage);
        if(getHealth() <= 0) {
            // burst monsters into flames!
            for(Token token : myRing.getTokens()) {
                if(token instanceof Monster) {
                    ((Monster)token).addFlame();
                } else if(token instanceof Hero) {
                    myRing.removeToken(token); // kills heroes
                }
            }
        }
        return hit;
    }

    @Override
    public Token clone() {
        return new Phoenix(maxHealth);
    }
}
