package castlepanic.wizardstower.tokens;

import castlepanic.Game;
import castlepanic.Input;
import castlepanic.base.tokens.GroundMonster;
import castlepanic.base.tokens.Monster;
import castlepanic.base.tokens.MonsterBoss;
import castlepanic.component.Token;

import java.awt.*;

public class Hydra extends MonsterBoss implements MegaMonsterBoss {
    public Hydra(int health) {
        super("Hydra", health);
        description = "<html><b>" + name + "</b><br><b>In Play:</b> Draw 2 imps for every point of damage to the " + name + " (except for the last point)<br>" +
                "&nbsp;&nbsp;and place them in the Forest ring in the same arc as the " + name + ".  This DOES include damage caused by fire and<br>" +
                "&nbsp;&nbsp;structures.  Do not draw any Imps when the " + name + " is slayed.</html>";
    }

    @Override
    public void doAction(Game game) {
    }

    @Override
    public int damage(Input input, int damage) {
        int hit = super.damage(input, damage);
        if(getHealth() > 0) {
            for (int i = 0; i < damage; i++) {
                Monster imp1 = new GroundMonster("Imp", 1, Color.magenta);
                Monster imp2 = new GroundMonster("Imp", 1, Color.magenta);
                imp1.place(getRing().getArc().getRings().get(0));
                imp2.place(getRing().getArc().getRings().get(0));
            }
        }
        return hit;
    }

    @Override
    public Token clone() {
        return new Hydra(maxHealth);
    }
}
