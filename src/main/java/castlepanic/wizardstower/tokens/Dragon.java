package castlepanic.wizardstower.tokens;

import castlepanic.Game;
import castlepanic.Input;
import castlepanic.MoveDirection;
import castlepanic.base.board.Arc;
import castlepanic.component.Die;
import castlepanic.component.Token;

import java.util.List;

public class Dragon extends FlyingMonsterBoss implements MegaMonsterBoss {
    private Die die;

    public Dragon(int health) {
        super("Dragon", health);
        this.die = new Die(6);
        description = "<html><b>" + name + "</b><br><b>When Drawn:</b> The " + name + " breathes fire when placed on the board.<br>" +
                "<b>In Play:</b> Roll the die and move the " + name + " accordingly. The " + name + " does not breathe fire<br>" +
                "&nbsp;&nbsp;if it is stopped from moving by a card.  Inside the Castle ring, the " + name + " moves by standard rules and no<br>" +
                "&nbsp;&nbsp;longer breathes fire.</html>";
    }

    private void breathFire(Input input) {
        FireBreath fireBreath = new FireBreath();
        fireBreath.startFromRing(input, getRing());
    }

    @Override
    public void doAction(Game game) {
        breathFire(game.getInput());
    }

    @Override
    public boolean move(Input input) {
        if(getRing().getIndex() == getRing().getArc().getRings().size()-1) {
            return super.move(input);
        } else {
            int num = die.roll(input);
            boolean result = false;
            switch (num) {
                case 1:
                    result = move(input, MoveDirection.Clockwise);
                    break;
                case 2:
                case 5:
                    result = move(input, MoveDirection.Normal);
                    break;
                case 4:
                    result = move(input, MoveDirection.Back);
                    break;
                case 6:
                    result = move(input, MoveDirection.CounterClockwise);
                    break;
            }
            breathFire(input);
            return result;
        }
    }

    @Override
    public Token clone() {
        return new Dragon(maxHealth);
    }
}
