package castlepanic.wizardstower.tokens;

import castlepanic.Input;
import castlepanic.base.board.Ring;
import castlepanic.base.tokens.BoulderEffect;
import castlepanic.base.tokens.Structure;
import castlepanic.component.Token;
import castlepanic.darktitan.tokens.Hero;

public class FireBreath extends BoulderEffect {
    public FireBreath() {
        name = "Fire Breath";
    }

    @Override
    protected boolean effectStructure(Input input, Structure structure, int count) {
        structure.addFlame(input);
        return false;
    }

    @Override
    protected void slayMonsters(Input input, Ring ring) {
        // only slay Heros
        for(Hero hero : ring.collectTokens(Hero.class)) {
            hero.getRing().removeToken(hero);
            input.addMessage(getName() + " slayed " + hero.getName());
        }
    }

    @Override
    public Token clone()
    {
        return new FireBreath();
    }
}
