package castlepanic.wizardstower.tokens;

import castlepanic.Input;
import castlepanic.base.board.Ring;
import castlepanic.base.tokens.BoulderEffect;
import castlepanic.base.tokens.GroundMonster;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Token;

public class TrebuchetEffect extends BoulderEffect {
    public TrebuchetEffect() {
        name = "Trebuchet";
    }

    @Override
    protected void slayMonsters(Input input, Ring ring) {
        for(Monster monster : ring.collectTokens(FlyingMonster.class)) {
            monster.damage(input, monster.getHealth()); // slay is special
            input.addMessage(getName() + " slayed " + monster.getName());
        }
    }

    @Override
    public Token clone()
    {
        return new TrebuchetEffect();
    }
}
