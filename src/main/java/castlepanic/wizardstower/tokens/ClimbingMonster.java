package castlepanic.wizardstower.tokens;

import castlepanic.Input;
import castlepanic.base.board.Ring;
import castlepanic.base.tokens.GroundMonster;
import castlepanic.base.tokens.Wall;
import castlepanic.component.Token;

public class ClimbingMonster extends GroundMonster {
    public ClimbingMonster(String name, int health) {
        super(name, health);
    }

    @Override
    protected Ring moveIntoWall(Input input, Ring nextRing, Wall wall) {
        // ignore the wall
        return nextRing;
    }


    @Override
    public Token clone() {
        return new ClimbingMonster(name, maxHealth);
    }
}
