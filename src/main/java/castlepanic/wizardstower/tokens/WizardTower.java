package castlepanic.wizardstower.tokens;

import castlepanic.PanelRenderer;
import castlepanic.base.tokens.Tower;
import castlepanic.component.Token;

import java.awt.*;

public class WizardTower extends Tower {
    @Override
    public Token clone() {
        return new WizardTower();
    }

    @Override
    public void draw(Graphics g, Rectangle board, int iring, int iarc, int itoken) {
        Point center = new Point(board.x + board.width / 2, board.y + board.height / 2);
        int radius = board.width / 2;
        int arcSize = 360 / getRing().getArc().getBoard().getArcs().size();

        int arcDistance = radius / getRing().getArc().getRings().size();
        int towerSize = arcDistance / 3;
        int arcPosition = (getRing().getArc().getRings().size() - iring - 1) * arcDistance + arcDistance / 2 + 5;

        int tokenPosition = (iarc * arcSize) + arcSize / 2;

        Point pt = PanelRenderer.getBoardPoint(center, tokenPosition, arcPosition);

        Polygon polygon = new Polygon(new int[] { pt.x, pt.x - towerSize/2, pt.x },
                new int[] { pt.y - towerSize/2, pt.y + towerSize/2, pt.y + towerSize/2}, 3);
        if(this.numFlames > 0) {
            g.setColor(Color.orange);
        } else {
            g.setColor(Color.gray);
        }
        g.fillPolygon(polygon);

        polygon = new Polygon(new int[] { pt.x, pt.x + towerSize/2, pt.x },
                new int[] { pt.y - towerSize/2, pt.y + towerSize/2, pt.y + towerSize/2}, 3);
        if(this.numFlames > 1) {
            g.setColor(Color.orange);
        } else {
            g.setColor(Color.gray);
        }
        g.fillPolygon(polygon);

        polygon = new Polygon(new int[] { pt.x, pt.x - towerSize/2, pt.x + towerSize/2 },
                new int[] { pt.y - towerSize/2, pt.y + towerSize/2, pt.y + towerSize/2 }, 3);
        g.setColor(Color.black);
        g.drawPolygon(polygon);
    }
}
