package castlepanic.wizardstower.tokens;

import castlepanic.Game;
import castlepanic.Input;
import castlepanic.MoveDirection;
import castlepanic.base.board.Arc;
import castlepanic.base.tokens.MonsterBoss;
import castlepanic.component.Card;
import castlepanic.component.Die;
import castlepanic.component.Expansion;
import castlepanic.component.Player;
import castlepanic.component.Token;

import java.util.List;

public class Warlock extends MonsterBoss implements MegaMonsterBoss {
    private Die die;

    public Warlock(int health) {
        super("Warlock", health);
        this.die = new Die(6);
        description = "<html><b>" + name + "</b><br><b>When Drawn:</b> All players must discard 1 Wizard card.<br>" +
                "<b>In Play:</b> The " + name + " is unaffected by Wizard cards.  On Phase 5, roll the die and move the " + name + "<br>" +
                "&nbsp;&nbsp;to that numbered arc (keeping the " + name + " in the same ring), and then move the " + name + " one space closer to<br>" +
                "&nbsp;&nbsp;the Castle.  Inside the Castle ring, the " + name + " moves by standard rules.</html>";
    }

    private boolean hasWizardCard(Player player) {
        for(Card card : player.getHand()) {
            if(card.getExpansion() == Expansion.WizardsTower) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void doAction(Game game) {
        for(Player player : game.getPlayers()) {
            if(hasWizardCard(player)) {
                // request card
                Card card = null;
                while (card == null || card.getExpansion() != Expansion.WizardsTower) {
                    game.getInput().addMessage("Choose a wizard card to discard.");
                    card = game.getInput().requestCard();
                }

                // discard
                player.discard(card);
            }
        }
    }

    @Override
    public boolean move(Input input) {
        int arcIndex = die.roll(input);
        int index = getRing().getIndex();
        List<Arc> arcs = getRing().getArc().getBoard().getArcs();
        place(arcs.get(arcIndex-1).getRings().get(index));
        return move(input, MoveDirection.Normal);
    }

    @Override
    public Token clone() {
        return new Warlock(maxHealth);
    }
}
