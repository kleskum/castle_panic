package castlepanic.wizardstower.tokens;

import castlepanic.Input;
import castlepanic.base.tokens.BoulderEffect;
import castlepanic.base.tokens.Structure;
import castlepanic.component.Token;

public class FlamingBoulderEffect extends BoulderEffect {
    public FlamingBoulderEffect() {
        name = "Flaming Boulder";
    }

    @Override
    protected boolean effectStructure(Input input, Structure structure, int count) {
        if(count == 0) {
            // first structure is destroyed
            structure.destroy(input);
            input.addMessage(getName() + " destroyed " + structure.getName() + " at " + structure.getRing().getArc().getNumber());
            return true;
        } else {
            // second structure catches fire
            structure.addFlame(input);
            return false;
        }
    }

    @Override
    public Token clone()
    {
        return new FlamingBoulderEffect();
    }
}
