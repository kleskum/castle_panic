package castlepanic.wizardstower.tokens;

import castlepanic.Game;
import castlepanic.base.tokens.MonsterBoss;
import castlepanic.component.Card;
import castlepanic.component.Player;
import castlepanic.component.Token;

public class Basilisk extends MonsterBoss implements MegaMonsterBoss{
    public Basilisk(int health) {
        super("Basilisk", health);
        description = "<html><b>" + name + "</b><br><b>When Drawn:</b< All players must discard down to a hand of 2 cards.<br>" +
                "<b>In Play:</b> All players skip Phase 2 (Discard an Draw) on their turns.</html>";
    }

    @Override
    public void doAction(Game game) {
        for(Player player : game.getPlayers()) {
            while(player.getHand().size() > 2) {
                // request card
                Card card = null;
                while (card == null) {
                    game.getInput().addMessage("Choose a card to discard.");
                    card = game.getInput().requestCard();
                }

                // discard
                player.discard(card);
                game.getInput().redraw();
            }
        }
        game.setSkipPhase2(true);
    }

    @Override
    public Token clone() {
        return new Basilisk(maxHealth);
    }
}
