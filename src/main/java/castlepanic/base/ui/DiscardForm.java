package castlepanic.base.ui;

import castlepanic.component.Card;
import castlepanic.Game;
import castlepanic.PanelRenderer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class DiscardForm extends JDialog implements MouseListener {
    private Game myGame;
    private Card selectedCard = null;

    public DiscardForm(JFrame parent, Game game) {
        super(parent, "Discard", true);
        setTitle("Castle Panic");
        setResizable(false);
        myGame = game;

        Canvas canvas = new Canvas() {
            @Override
            public void paint(Graphics g) {
                super.paint(g);
                if (myGame != null) {
                    int x = 20;
                    int y = 20;
                    int cardWidth = 60;
                    int cardHeight = 80;

                    int width = 0;
                    for (Card card : myGame.getDeck().getDiscardPile()) {
                        width = Math.max(x + cardWidth + 30, width);

                        PanelRenderer.drawCard(g, card, x, y, cardWidth, cardHeight);
                        x += cardWidth + 10;
                        if (x > 600) {
                            x = 20;
                            y += cardHeight + 10;
                        }
                    }
                }
            }
        };
        canvas.setSize(800, 600);
        canvas.addMouseListener(this);
        this.add(canvas);
        this.pack();
    }

    public Card getSelectedCard() {
        return selectedCard;
    }

    public void mouseClicked(MouseEvent e) {
        if (myGame != null) {
            int x = 20;
            int y = 20;
            int cardWidth = 60;
            int cardHeight = 80;

            for (Card card : myGame.getDeck().getDiscardPile()) {
                Rectangle bounds = new Rectangle(x, y, cardWidth, cardHeight);
                if (bounds.contains(new Point(e.getX(), e.getY()))) {
                    selectedCard = card;
                    this.setVisible(false);
                    return;
                }
                x += cardWidth + 10;
                if (x > 600) {
                    x = 20;
                    y += cardHeight + 10;
                }
            }
        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }
}
