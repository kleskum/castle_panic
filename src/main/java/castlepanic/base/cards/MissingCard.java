package castlepanic.base.cards;

import castlepanic.component.Card;
import castlepanic.Game;

public class MissingCard extends SpecialCard {
    public MissingCard() {
        super("Missing");
        description = "Do not draw any Monster tokens this turn.";
    }

    @Override
    public boolean play(Game game) {
        game.setMonstersToDraw(0);
        return true;
    }

    @Override
    public Card clone() {
        return new MissingCard();
    }
}
