package castlepanic.base.cards;

import castlepanic.component.Card;
import castlepanic.Game;
import castlepanic.base.tokens.Monster;

import java.awt.*;

public class BarbarianCard extends SpecialAttackCard {
    public BarbarianCard(String[] types, Color[] colors) {
        super("Barbarian", types, colors);
        description = "Slay 1 Monster anywhere, including the Castle ring, but not the Forest.";
    }

    @Override
    public boolean playSpecial(Game game, Monster monster) {
        if (monster.canHit(this)) {
            monster.slay(game.getInput());
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Card clone() {
        return new BarbarianCard(types.toArray(new String[0]), colors.toArray(new Color[0]));
    }
}
