package castlepanic.base.cards;

import castlepanic.component.Card;
import castlepanic.Game;
import castlepanic.base.board.Ring;
import castlepanic.component.Token;
import castlepanic.base.tokens.Wall;

public class MaterialCard extends Card {
    public MaterialCard(String name) {
        super(name);
        description = name;
    }


    @Override
    public boolean play(Game game) {
        String reqOther = getName().equals("Brick") ? "Mortar" : "Brick";
        game.getInput().addMessage("Choose a " + reqOther + " card.");
        Card card = game.getInput().requestCardToPlay();
        if(card != null && card instanceof MaterialCard) {
            MaterialCard materialCard = (MaterialCard)card;
            if(materialCard.getName().equals(reqOther)) {
                game.getInput().addMessage("Choose a castle ring to build a wall.");
                Ring ring = game.getInput().chooseRing();
                if(ring.getIndex() == ring.getArc().getRings().size() - 1) {
                    Wall wall = ring.findTokenType(Wall.class);
                    if (wall == null) {
                        wall = new Wall();
                        wall.place(ring);
                        game.getCurrentPlayer().discard(materialCard);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public Card clone() {
        return new MaterialCard(name);
    }
}
