package castlepanic.base.cards;

import castlepanic.base.tokens.FortifiedWall;
import castlepanic.base.tokens.Tower;
import castlepanic.component.Card;
import castlepanic.Game;
import castlepanic.base.board.Ring;
import castlepanic.component.Token;
import castlepanic.base.tokens.Wall;

public class FortifyWallCard extends SpecialCard {
    public FortifyWallCard() {
        super("Fortify Wall");
        description = "Place Fortify token on 1 Wall.  When hit, token damages Monsters and stops Boulders.";
    }

    @Override
    public boolean play(Game game) {
        game.getInput().addMessage("Choose a castle ring with a wall to foritfy.");
        Ring ring = game.getInput().chooseRing();
        if (ring != null) {
            Wall wall = ring.findTokenType(Wall.class);
            if(wall != null) {
                // fortify the wall
                FortifiedWall fortifiedWall = new FortifiedWall();
                fortifiedWall.place(ring);
                return true;
            }
        }
        return false;
    }

    @Override
    public Card clone() {
        return new FortifyWallCard();
    }
}
