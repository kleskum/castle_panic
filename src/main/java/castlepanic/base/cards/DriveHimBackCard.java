package castlepanic.base.cards;

import castlepanic.base.board.Arc;
import castlepanic.component.Card;
import castlepanic.Game;
import castlepanic.base.tokens.Monster;
import castlepanic.wizardstower.tokens.FlyingMonster;

import java.awt.*;

public class DriveHimBackCard extends SpecialAttackCard {
    public DriveHimBackCard(String[] types, Color[] colors) {
        super("Drive Him Back!", types, colors);
        description = "Move 1 Monster back into the Forest, keeping it in the same numbered arc.";
    }

    @Override
    public boolean playSpecial(Game game, Monster monster) {
        if (monster.canHit(this) && !(monster instanceof FlyingMonster)) {
            Arc arc = monster.getRing().getArc();
            monster.place(arc.getRings().get(0));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Card clone() {
        return new DriveHimBackCard(types.toArray(new String[0]), colors.toArray(new Color[0]));
    }
}
