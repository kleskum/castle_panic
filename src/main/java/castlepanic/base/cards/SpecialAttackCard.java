package castlepanic.base.cards;

import castlepanic.Game;
import castlepanic.base.tokens.Monster;

import java.awt.*;

public abstract class SpecialAttackCard extends AttackCard {
    protected Color color = Color.magenta;

    public SpecialAttackCard(String name, String[] types, Color[] colors) {
        // color names don't matter for special attacks
        super(types, new String[0], colors);
        this.name = name;
    }

    public abstract boolean playSpecial(Game game, Monster monster);

    public Color getColor() {
        return color;
    }

    @Override
    public boolean play(Game game, Monster monster) {
        if(monster.canHit(this)) {
            return playSpecial(game, monster);
        } else {
            return false;
        }
    }
}
