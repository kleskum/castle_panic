package castlepanic.base.cards;

import castlepanic.Game;
import castlepanic.component.Card;

public class DrawCardsCard extends SpecialCard {
    private int numCards;

    public DrawCardsCard(int count) {
        super("Draw " + count + " Cards");
        numCards = count;
        description = "Play this card to add " + count + " cards to your hand, even if it exceeds the normal hand size.";
    }

    @Override
    public boolean play(Game game) {
        for (int i = 0; i < numCards; i++) {
            game.getCurrentPlayer().draw();
        }
        return true;
    }

    @Override
    public Card clone() {
        return new DrawCardsCard(numCards);
    }
}
