package castlepanic.base.cards;

import castlepanic.component.Card;

public abstract class SpecialCard extends Card {
    public SpecialCard(String name) {
        super(name);
    }
}
