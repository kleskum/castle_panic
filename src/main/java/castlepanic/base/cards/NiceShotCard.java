package castlepanic.base.cards;

import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;
import castlepanic.Game;

public class NiceShotCard extends SpecialCard {
    public NiceShotCard() {
        super("Nice Shot");
        description = "Play this card WITH any hit card to slay the hit Monster.";
    }

    @Override
    public boolean play(Game game) {
        game.getInput().addMessage("Choose a card to attack with.");
        Card card = game.getInput().requestCardToPlay();
        if(card != null && card instanceof AttackCard && card.getDescription().toLowerCase().contains("hit")) {
            AttackCard attackCard = (AttackCard)card;
            game.getInput().addMessage("Choose a monster to attack.");
            Monster monster = game.getInput().chooseToken(Monster.class);
            if(monster != null && monster.canHit(attackCard)) {
                monster.slay(game.getInput());
                game.getCurrentPlayer().discard(attackCard);
                return true;
            }
        }
        return false;
    }

    @Override
    public Card clone() {
        return new NiceShotCard();
    }
}
