package castlepanic.base.cards;

import castlepanic.component.Card;
import castlepanic.Game;
import castlepanic.base.tokens.Monster;
import castlepanic.wizardstower.tokens.FlyingMonster;

import java.awt.*;

public class TarCard extends SpecialAttackCard {
    public TarCard(String[] types, Color[] colors) {
        super("Tar", types, colors);
        description = "Place Tar Token on 1 Monster.  That Monster does not move this turn.";
    }

    @Override
    public boolean playSpecial(Game game, Monster monster) {
        if (monster.canHit(this) && !(monster instanceof FlyingMonster)) {
            monster.setTarred(true);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Card clone() {
        return new TarCard(types.toArray(new String[0]), colors.toArray(new Color[0]));
    }
}
