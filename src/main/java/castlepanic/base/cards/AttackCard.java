package castlepanic.base.cards;

import castlepanic.component.Card;
import castlepanic.Game;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Token;
import castlepanic.darktitan.tokens.SupportToken;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class AttackCard extends Card {
    protected List<String> colorNames = new ArrayList<String>();
    protected List<String> types = new ArrayList<String>();
    protected List<Color> colors = new ArrayList<Color>();
    private Monster chosenMonster = null;

    public AttackCard(String type, String colorName, Color color) {
        this(new String[] { type }, new String[] { colorName }, new Color[] { color });
    }

    public AttackCard(String type, String[] colorNames, Color[] colors) {
        this(new String[] { type }, colorNames, colors);
    }

    public AttackCard(String[] types, String colorName, Color color) {
        this(types, new String[] { colorName }, new Color[] { color });
    }

    public AttackCard(String[] types, String[] colorNames, Color[] colors) {
        description = "Hit 1 Monster in ";
        if (types.length > 1) {
            name = "Hero";
            description += "the ";
        } else {
            name = types[0];
            if(colors.length > 1) {
                description += "any color ";
            } else {
                description += "the " + colorNames[0] + " ";
            }
        }

        for (String colorName : colorNames) {
            this.colorNames.add(colorName);
        }
        for (int i = 0; i < types.length; i++) {
            if(i > 0) {
                description += ", ";
                if(types.length > 1 && i == types.length-1) {
                    description += "or ";
                }
            }
            description += types[i] + " ";
            this.types.add(types[i]);
        }
        for (Color color : colors) {
            this.colors.add(color);
        }

        if (types.length > 1 && colorNames.length > 0) {
            description += "ring of the " + colorNames[0] + " arc.";
        } else {
            description += "ring.";
        }
    }

    public List<String> getTypes() {
        return types;
    }

    public List<Color> getColors() {
        return colors;
    }

    public Monster getChosenMonster() {
        return chosenMonster;
    }

    public boolean containsType(String type) {
        return types.contains(type);
    }

    public boolean containsColor(Color color) {
        return colors.contains(color);
    }

    @Override
    public boolean play(Game game) {
        // check for support tokesn
        String message = "Choose a Monster token to hit";
        if(getDescription().toLowerCase().contains("hit") && game.getBoard().collectTokens(SupportToken.class).size() > 0) {
            message += " or a Support token to move";
        }
        message += ".";
        game.getInput().addMessage(message);
        Token token = game.getInput().chooseToken(Token.class);
        if(token == null) {
            return false;
        } else if(getDescription().toLowerCase().contains("hit") && token instanceof SupportToken){
            SupportToken supportToken = (SupportToken)token;
            boolean bonus = containsColor(supportToken.getRing().getArc().getColor());
            supportToken.move(game);
            if(supportToken.getRing() != null && bonus) {
                // move a bonus space
                supportToken.move(game);
            }
            return true;
        } else if(token instanceof Monster) {
            Monster monster = (Monster)token;
            return play(game, monster);
        } else {
            return false;
        }
    }

    public boolean play(Game game, Monster monster) {
        // check if valid
        chosenMonster = monster;
        if (monster.canHit(this)) {
            // hit the monster
            int score = monster.hit(game.getInput());

            // update player's score
            game.getCurrentPlayer().increaseScore(score);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Card clone() {
        return new AttackCard(types.toArray(new String[0]), colorNames.toArray(new String[0]), colors.toArray(new Color[0]));
    }
}
