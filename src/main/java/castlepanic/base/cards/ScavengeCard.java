package castlepanic.base.cards;

import castlepanic.component.Card;
import castlepanic.Game;

public class ScavengeCard extends SpecialCard {
    public ScavengeCard() {
        super("Scavenge");
        description = "Search the discard pile for 1 card of your choice and add it to your hand.";
    }

    @Override
    public boolean play(Game game) {
        if (game.getDeck().getDiscardCount() == 0) {
            return false;
        }
           
        // have player choose card from discard
        Card card = game.getInput().chooseCardFromDiscard();
        if (card != null) {
            game.getCurrentPlayer().scavenge(card);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Card clone() {
        return new ScavengeCard();
    }
}

