package castlepanic.base.board;

import castlepanic.GameConfig;
import castlepanic.component.Token;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.awt.Color;

public class Arc {
    private Board board;
    private int number;
    private Color color;
    private List<Ring> rings = new ArrayList<Ring>();

    public Arc(GameConfig config, Board board, int number, Color color) {
        this.board = board;
        this.number = number;
        this.color = color;

        // add rings
        for (String name : config.getRingNames()) {
            this.rings.add(new Ring(this, name));
        }
    }

    public Board getBoard() {
        return board;
    }

    public int getNumber() {
        return number;
    }

    public Color getColor() {
        return color;
    }

    public List<Ring> getRings() {
        return rings;
    }

    public List<Ring> getRingsOuterToInner() {
        return rings;
    }

    public List<Ring> getRingsInnerToOuter() {
        List<Ring> list = new ArrayList<Ring>();
        list.addAll(rings);
        Collections.reverse(list);
        return list;
    }

    public int getIndex() {
        return board.getArcs().indexOf(this);
    }

    public Ring getOuterMostRing() {
        return rings.get(0);
    }

    public Ring getInnerMostRing() {
        return rings.get(rings.size()-1);
    }

    public Arc oppositeArc() {
        int arcIndex = getIndex();
        int totalArcs = getBoard().getArcs().size();
        arcIndex += totalArcs / 2;
        if (arcIndex > totalArcs) {
            arcIndex -= totalArcs;
        }
        return board.getArcs().get(arcIndex - 1);
    }

    public <T extends Token> List<T> collectTokens(Class<T> clazz) {
        List<T> list = new ArrayList<T>();
        for(Ring ring : rings) {
            list.addAll(ring.collectTokens(clazz));
        }
        return list;
    }
}
