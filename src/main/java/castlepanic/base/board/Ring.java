package castlepanic.base.board;

import castlepanic.base.tokens.Tower;
import castlepanic.component.Token;

import java.util.ArrayList;
import java.util.List;

public class Ring {
    private String name;
    private List<Token> tokens = new ArrayList<Token>();
    private Arc arc;

    public Ring(Arc arc, String name) {
        this.arc = arc;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Token> getTokens() {
        return tokens;
    }

    public Arc getArc() {
        return arc;
    }

    public int getIndex() {
        return arc.getRings().indexOf(this);
    }

    public void addToken(Token token) {
        tokens.add(token);
    }

    public void removeToken(Token token) {
        tokens.remove(token);
    }

    public void clear() {
        tokens.clear();
    }

    public Ring previousArcRing() {
        List<Arc> allArcs = getArc().getBoard().getArcs();
        int arcIndex = getArc().getIndex();
        Arc previousArc = arcIndex == 0 ? allArcs.get(allArcs.size()-1) : allArcs.get(arcIndex-1);
        return previousArc.getRings().get(getIndex());
    }

    public Ring nextArcRing() {
        List<Arc> allArcs = getArc().getBoard().getArcs();
        int arcIndex = getArc().getIndex();
        Arc nextArc = arcIndex == allArcs.size()-1 ? allArcs.get(0) : allArcs.get(arcIndex+1);
        return nextArc.getRings().get(getIndex());
    }

    public Ring previousRing() {
        return this == getArc().getOuterMostRing() ? this : getArc().getRings().get(getIndex()-1);
    }

    public Ring nextRing() {
        return this == getArc().getInnerMostRing() ? nextArcRing() : getArc().getRings().get(getIndex()+1);
    }

    public <T extends Token> T findTokenType(Class<T> clazz) {
        for(Token token : tokens) {
            if(clazz.isInstance(token)) {
                return (T)token;
            }
        }
        return null;
    }

    public <T extends Token> List<T> collectTokens(Class<T> clazz) {
        List<T> list = new ArrayList<T>();
        for(Token token : tokens) {
            if(clazz.isInstance(token)) {
                list.add((T)token);
            }
        }
        return list;
    }
}

