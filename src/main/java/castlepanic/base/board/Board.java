package castlepanic.base.board;

import castlepanic.GameConfig;
import castlepanic.component.Token;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Board {
    private List<Arc> arcs = new ArrayList<Arc>();

    public Board(GameConfig config) {
        int number = 1;
        for (Color color : config.getArcColors()) {
            for (int i = 0; i < config.getArcsPerColor(); i++) {
                arcs.add(new Arc(config, this, number, color));
                number++;
            }
        }
    }

    public List<Arc> getArcs() {
        return arcs;
    }

    public void clear() {
        for (Arc arc : arcs) {
            for (Ring ring : arc.getRings()) {
                ring.clear();
            }
        }
    }

    public <T extends Token> List<T> collectTokens(Class<T> clazz) {
        List<T> list = new ArrayList<T>();
        for(Arc arc : arcs) {
            list.addAll(arc.collectTokens(clazz));
        }
        return list;
    }
}

