package castlepanic.base.tokens;

import castlepanic.PanelRenderer;

import java.awt.*;

public abstract class MonsterBoss extends GroundMonster {
    public MonsterBoss(String name, int health)
        {
            super(name, health);
        }

    @Override
    public void draw(Graphics g, Rectangle board, int iring, int iarc, int itoken) {
        super.draw(g, board, iring, iarc, itoken);
        Rectangle bounds = getBounds(board, iring, iarc, itoken);

        g.setColor(Color.yellow);
        ((Graphics2D)g).setStroke(new BasicStroke(2));
        g.drawOval(bounds.x, bounds.y, bounds.width, bounds.height);
        ((Graphics2D)g).setStroke(new BasicStroke(1));

        g.setColor(Color.black);
        PanelRenderer.drawCenteredString(g, this.getHealth().toString(), bounds, new Font("Arial", Font.PLAIN, 18));
    }
}
