package castlepanic.base.tokens;

import castlepanic.component.Card;
import castlepanic.Game;
import castlepanic.component.Player;
import castlepanic.component.Token;
import castlepanic.base.cards.AttackCard;

public class PlagueEffect extends Effect {
    private String myType;

    public PlagueEffect(String type) {
        super("Plague! " + type);
        myType = type;
        description = "All Players must discard all cards for " + type + " from their hands.";
    }

    @Override
    public void doAction(Game game) {
        // loop through players
        for (Player player : game.getPlayers()) {
            // loop through cards
            for (int i = player.getHand().size() - 1; i >= 0; i--) {
                Card card = player.getHand().get(i);

                // check for attack cards
                if (card instanceof AttackCard) {
                    // check for match
                    AttackCard attack = (AttackCard)card;
                    if (attack.getTypes().size() == 1 && attack.containsType(myType) && attack.getName().contains(myType)) {
                        // discard card
                        player.discard(attack);
                    }
                }
            }
        }
    }

    @Override
    public Token clone() {
        return new PlagueEffect(myType);
    }
}
