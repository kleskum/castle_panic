package castlepanic.base.tokens;

import castlepanic.Game;
import castlepanic.Input;
import castlepanic.component.Token;

public abstract class Structure extends Token {
    protected int numFlames = 0;

    public Structure(String name) {
        super(name);
    }

    public void addFlame(Input input) {
        numFlames++;
        if(numFlames > 2) {
            // too many flames, structure is destroyed
            destroy(input);
        }
    }

    public void removeFlames() {
        numFlames = 0;
    }

    public void destroy(Input input) {
        input.notifyStructureDestroyed(this);
        ring.removeToken(this);
    }

    @Override
    public void doAction(Game game) {
        // do nothing
    }
}
