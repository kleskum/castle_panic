package castlepanic.base.tokens;

import castlepanic.Input;
import castlepanic.component.Card;
import castlepanic.Game;
import castlepanic.component.Player;
import castlepanic.component.Token;

import java.util.List;

public class DiscardEffect extends Effect {
    private int numCards;

    public DiscardEffect(int count) {
        super("Discard " + count);
        numCards = count;
    }

    @Override
    public void doAction(Game game) {
        doAction(game.getInput(), game.getPlayers());
    }

    private void doAction(Input input, List<Player> players) {
        // loop through players
        for (Player player : players) {
            for (int i = 0; i < numCards && player.getHand().size() > 0; i++) {
                // request card
                Card card = null;
                while (card == null) {
                    input.addMessage("Choose a card to discard.");
                    card = input.requestCard();
                }
                // discard
                player.discard(card);
            }
        }
    }

    @Override
    public Token clone() {
        return new DiscardEffect(numCards);
    }
}
