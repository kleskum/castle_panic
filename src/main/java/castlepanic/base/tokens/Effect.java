package castlepanic.base.tokens;

import castlepanic.component.Token;

public abstract class Effect extends Token {
    protected String description;

    public Effect(String name) {
        super(name);
    }

    public String getDescription() {
        return description;
    }
}
