package castlepanic.base.tokens;

import castlepanic.PanelRenderer;
import castlepanic.component.Token;

import java.awt.*;

public class Tower extends Structure {
    public Tower() {
        super("Tower");
    }

    @Override
    public Token clone() {
        return new Tower();
    }

    @Override
    public void draw(Graphics g, Rectangle board, int iring, int iarc, int itoken) {
        Point center = new Point(board.x + board.width / 2, board.y + board.height / 2);
        int radius = board.width / 2;
        int arcSize = 360 / getRing().getArc().getBoard().getArcs().size();

        int arcDistance = radius / getRing().getArc().getRings().size();
        int towerSize = arcDistance / 3;
        int arcPosition = (getRing().getArc().getRings().size() - iring - 1) * arcDistance + arcDistance / 2 + 5;

        int tokenPosition = (iarc * arcSize) + arcSize / 2;

        Point pt = PanelRenderer.getBoardPoint(center, tokenPosition, arcPosition);

        if(this.numFlames > 0) {
            g.setColor(Color.orange);
        } else {
            g.setColor(Color.gray);
        }
        g.fillRect(pt.x - towerSize / 2, pt.y - towerSize / 2, towerSize/2, towerSize);
        if(this.numFlames > 1) {
            g.setColor(Color.orange);
        } else {
            g.setColor(Color.gray);
        }
        g.fillRect(pt.x, pt.y - towerSize / 2, towerSize/2, towerSize);

        g.setColor(Color.black);
        g.drawRect(pt.x - towerSize / 2, pt.y - towerSize / 2, towerSize, towerSize);
    }
}
