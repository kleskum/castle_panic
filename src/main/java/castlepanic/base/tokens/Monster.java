package castlepanic.base.tokens;

import castlepanic.Input;
import castlepanic.MoveDirection;
import castlepanic.PanelRenderer;
import castlepanic.base.cards.AttackCard;
import castlepanic.component.Token;
import castlepanic.base.board.Ring;
import castlepanic.darktitan.tokens.SupportToken;

import java.awt.*;

public abstract class Monster extends Token {
    protected Color color;
    protected String description;
    protected int maxHealth;
    protected Integer health = 0;
    protected boolean tarred = false;
    protected int numFlames = 0;

    public Monster(String name, int health, Color color) {
        super(name);
        this.description = name;
        maxHealth = health;
        this.health = health;
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public String getDescription() {
        return description;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public Integer getHealth() {
        return health;
    }

    public boolean isTarred() {
        return tarred;
    }

    public void setTarred(boolean tarred) {
        this.tarred = tarred;
    }

    public void reset() {
        this.ring = null;
        this.tarred = false;
        this.health = maxHealth;
    }

    // Hit the monster and return the score.
    public int hit(Input input) {
        return damage(input, 1);
    }

    public int damage(Input input, int damage) {
        health -= damage;

        if (health <= 0) {
            input.addMessage("Slayed " + name);
        } else {
            input.addMessage("Hit " + name);
        }

        if (health <= 0) {
            // monster died
            if(this.ring != null) {
                this.ring.removeToken(this);
            }
            this.ring = null;

            input.notifyMonsterSlayed(this);

            // return reward
            if (this instanceof MonsterBoss) {
                return 4;
            } else {
                return maxHealth;
            }
        }
        return 0;
    }

    public int slay(Input input) {
        damage(input, health);
        if (this instanceof MonsterBoss) {
            return 4;
        } else {
            return maxHealth;
        }
    }

    public void heal() {
        heal(1);
    }

    public void heal(int amt) {
        health += amt;
        if(health > maxHealth) {
            health = maxHealth;
        }
    }

    public boolean move(Input input) {
        return move(input, MoveDirection.Normal);
    }

    protected Ring moveIntoWall(Input input, Ring nextRing, Wall wall) {
        if(wall.numFlames == 0) {
            damage(input, 1);
        }
        numFlames += wall.numFlames;
        wall.destroy(input);
        return getRing();
    }

    protected void moveIntoTower(Input input, Ring nextRing, Tower tower) {
        if(tower.numFlames == 0) {
            damage(input, 1);
        }
        numFlames += tower.numFlames;
        tower.destroy(input);
    }

    // returns if other monsters in same ring should keep moving
    public boolean move(Input input, MoveDirection direction) {
        if (tarred) {
            return true;
        }
        if (getRing() != null) {
            Ring nextRing = null;

            // move towards castle
            int ringIndex = getRing().getIndex();
            int arcIndex = getRing().getArc().getIndex();

            if (direction == MoveDirection.Clockwise || direction == MoveDirection.CounterClockwise || getRing() == getRing().getArc().getInnerMostRing()) {
                // in castle, move around arc

                // check direction
                if (direction == MoveDirection.CounterClockwise) {
                    nextRing = getRing().previousArcRing();
                } else {
                    nextRing = getRing().nextArcRing();
                }
            } else if(direction == MoveDirection.Back) {
                // move to previous ring
                if(ringIndex > 0) {
                    nextRing = getRing().previousRing();
                } else {
                    return true; // the monster doesn't move
                }
            } else {
                // move to next ring
                nextRing = getRing().nextRing();
            }

            // get next arc index
            int nextArcIndex = nextRing.getArc().getIndex();

            // check for wall and tower
            Token wall = null;
            Token tower = null;
            for (Token token : nextRing.getTokens()) {
                // look for wall if moving straight
                if (token instanceof FortifiedWall && arcIndex == nextArcIndex) {
                    // favor fortified walls first
                    wall = token;
                } else if (wall == null && token instanceof Wall && arcIndex == nextArcIndex) {
                    wall = token;
                }
                // look for tower
                if (token instanceof Tower) {
                    tower = token;
                }
            }

            // destroy wall or tower if exists
            if (wall != null) {
                nextRing = moveIntoWall(input, nextRing, (Wall)wall);
                if(nextRing == getRing() || getRing() == null) {
                    // stop moving other monsters if this one was stalled
                    return false;
                }
            }
            if (tower != null) {
                moveIntoTower(input, nextRing, (Tower)tower);
            }

            // move the monster
            if (getRing() != null) {
                getRing().removeToken(this);
                nextRing.addToken(this);
                this.ring = nextRing;
            }

            // look for newly encountered support token
            if(this.ring != null) {
                for (SupportToken supportToken : this.ring.collectTokens(SupportToken.class)) {
                    input.redraw();
                    supportToken.battle(input);
                }
            }
        }
        return true;
    }

    public boolean canHit(AttackCard attackCard) {
        return attackCard.getTypes().contains(getRing().getName()) && attackCard.getColors().contains(getRing().getArc().getColor());
    }

    public void addFlame() {
        numFlames++;
    }

    public void removeFlames() {
        numFlames = 0;
    }

    public void burn(Input input) {
        for(int i = 0; i < numFlames; i++) {
            damage(input, 1);
        }
    }

    @Override
    public void draw(Graphics g, Rectangle board, int iring, int iarc, int itoken) {
        Rectangle bounds = getBounds(board, iring, iarc, itoken);

        if (this.tarred) {
            g.setColor(Color.black);
            g.fillOval(bounds.x - 3, bounds.y - 3, bounds.width + 6, bounds.height + 6);
        }
        if(numFlames > 0) {
            g.setColor(Color.orange);
        } else {
            g.setColor(color);
        }
        g.fillOval(bounds.x, bounds.y, bounds.width, bounds.height);
        g.setColor(Color.black);
        g.drawOval(bounds.x, bounds.y, bounds.width, bounds.height);

        g.setColor(Color.black);
        PanelRenderer.drawCenteredString(g, this.health.toString(), bounds, new Font("Arial", Font.PLAIN, 18));
    }
}

