package castlepanic.base.tokens;

import castlepanic.Game;
import castlepanic.Input;
import castlepanic.base.board.Board;
import castlepanic.base.board.Ring;
import castlepanic.component.Token;

import java.util.HashSet;
import java.util.Set;

public class AllMoveEffect extends Effect {
    public AllMoveEffect() {
        super("All Monsters Move");
    }

    @Override
    public void doAction(Game game) {
        doAction(game.getInput(), game.getBoard());
    }

    private void doAction(Input input, Board board) {
        Set<Ring> stalledRings = new HashSet<Ring>();
        for(Monster monster : board.collectTokens(Monster.class)) {
            if(!stalledRings.contains(monster.getRing())) {
                if (!monster.move(input) && monster.getRing() != null) {
                    stalledRings.add(monster.getRing());
                }
            }
        }
    }

    @Override
    public Token clone() {
        return new AllMoveEffect();
    }
}

