package castlepanic.base.tokens;

import castlepanic.Game;
import castlepanic.component.Token;

public class GoblinKing extends MonsterBoss {
    public GoblinKing() {
        super("Goblin King", 2);
        description = "<html><b>" + name + "</b><br>Draw and and resolve 3 more Monster tokens.</html>";
    }

    @Override
    public void doAction(Game game) {
        // same as draw monster effect
        DrawMonsterEffect effect = new DrawMonsterEffect(3);
        effect.doAction(game);
    }

    @Override
    public Token clone() {
        return new GoblinKing();
    }
}
