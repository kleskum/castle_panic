package castlepanic.base.tokens;

import castlepanic.Game;
import castlepanic.MoveDirection;
import castlepanic.base.board.Ring;
import castlepanic.component.Token;

import java.util.HashSet;
import java.util.Set;

public class ArcMoveEffect extends Effect {
    private boolean clockwise;

    public ArcMoveEffect(boolean cw) {
        super("Monsters Move " + (!cw ? "Counter-" : "") + "Clockwise");
        clockwise = cw;
        description = "All Monsters move 1 space " + (!clockwise ? "counter-" : "") +
                "clockwise but stay in the same ring.  This includes Monsters in the Castle" +
                " and Forest rings.";
    }

    @Override
    public void doAction(Game game) {
        Set<Ring> stalledRings = new HashSet<Ring>();
        for(Monster monster : game.getBoard().collectTokens(Monster.class)) {
            if(!stalledRings.contains(monster.getRing())) {
                if (!monster.move(game.getInput(), clockwise ? MoveDirection.Clockwise : MoveDirection.CounterClockwise) && monster.getRing() != null) {
                    stalledRings.add(monster.getRing());
                }
            }
        }
    }

    @Override
    public Token clone() {
        return new ArcMoveEffect(clockwise);
    }
}
