package castlepanic.base.tokens;

import castlepanic.component.Token;

import java.awt.*;

public class Wall extends Structure {
    public Wall() {
        super("Wall");
    }

    @Override
    public void draw(Graphics g, Rectangle board, int iring, int iarc, int itoken) {
        int radius = board.width / 2;
        int arcSize = 360 / getRing().getArc().getBoard().getArcs().size();

        ((Graphics2D)g).setStroke(new BasicStroke(8));

        int arcDistance = radius / getRing().getArc().getRings().size();
        int arcPosition = arcDistance * iring;
        int arcWidth = board.width - arcPosition * 2;
        int arcAngle = (iarc * arcSize)*-1 - 60;
        Rectangle level = new Rectangle(board.x + arcPosition, board.y + arcPosition, arcWidth, arcWidth);

        if(this.numFlames > 0) {
            g.setColor(Color.orange);
        } else {
            g.setColor(Color.gray);
        }
        g.drawArc(level.x, level.y, level.width, level.height, arcAngle, arcSize/2);
        if(this.numFlames > 1) {
            g.setColor(Color.orange);
        } else {
            g.setColor(Color.gray);
        }
        g.drawArc(level.x, level.y, level.width, level.height, arcAngle + arcSize/2, arcSize/2);

        ((Graphics2D)g).setStroke(new BasicStroke(1));

    }

    @Override
    public Token clone() {
        return new Wall();
    }
}
