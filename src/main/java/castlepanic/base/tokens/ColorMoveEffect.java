package castlepanic.base.tokens;

import castlepanic.Game;
import castlepanic.base.board.Ring;
import castlepanic.component.Token;

import java.awt.Color;
import java.util.HashSet;
import java.util.Set;

public class ColorMoveEffect extends Effect {
    private String colorName;
    private Color myColor;

    public ColorMoveEffect(String colorName, Color color) {
        super(colorName + " Monsters Move 1");
        this.colorName = colorName;
        myColor = color;
        description = "All Monsters in the " + colorName + " arc move 1 ring closer to the castle.";
    }

    @Override
    public void doAction(Game game) {
        Set<Ring> stalledRings = new HashSet<Ring>();
        for (Monster monster : game.getBoard().collectTokens(Monster.class)) {
            if (monster.getRing().getArc().getColor().equals(myColor) && !stalledRings.contains(monster.getRing())) {
                if (!monster.move(game.getInput()) && monster.getRing() != null) {
                    stalledRings.add(monster.getRing());
                }
            }
        }
    }

    @Override
    public Token clone() {
        return new ColorMoveEffect(colorName, myColor);
    }
}
