package castlepanic.base.tokens;

import castlepanic.Game;
import castlepanic.component.Token;

import java.awt.*;

public class GroundMonster extends Monster {
    public GroundMonster(String name, int health) {
        super(name, health, Color.gray);
    }

    public GroundMonster(String name, int health, Color color) {
        super(name, health, color);
    }

    @Override
    public void doAction(Game game) {
        // do nothing
    }

    @Override
    public Token clone() {
        return new GroundMonster(name, maxHealth, color);
    }
}
