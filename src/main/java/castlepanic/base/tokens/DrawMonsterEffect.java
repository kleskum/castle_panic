package castlepanic.base.tokens;

import castlepanic.Game;
import castlepanic.component.Token;

public class DrawMonsterEffect extends Effect {
    private int numMonsters;

    public DrawMonsterEffect(int count) {
        super("Draw " + count + " Monsters");
        numMonsters = count;
    }

    @Override
    public void doAction(Game game) {
        for (int i = 0; i < numMonsters; i++) {
            game.drawMonsterToken();
        }
    }

    @Override
    public Token clone() {
        return new DrawMonsterEffect(numMonsters);
    }
}
