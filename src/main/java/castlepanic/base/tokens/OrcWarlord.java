package castlepanic.base.tokens;

import castlepanic.Game;
import castlepanic.component.Token;

public class OrcWarlord extends MonsterBoss {
    public OrcWarlord() {
        super("Orc Warlord", 3);
        description = "<html><b>" + name + "</b><br>All Monsters in the same color move 1 space.</html>";
    }

    @Override
    public void doAction(Game game) {
        // same as color move effect
        ColorMoveEffect effect = new ColorMoveEffect("N/A", getRing().getArc().getColor());
        effect.doAction(game);
    }

    @Override
    public Token clone() {
        return new OrcWarlord();
    }
}

