package castlepanic.base.tokens;

import castlepanic.Game;
import castlepanic.component.Token;

public class Healer extends MonsterBoss {
    public Healer() {
        super("Healer", 2);
        description = "<html><b>" + name + "</b><br>All Monsters on the board regain 1 point of damage.</html>";
    }

    @Override
    public void doAction(Game game) {
        // heal all monsters
        HealEffect effect = new HealEffect(1);
        effect.doAction(game);
    }

    @Override
    public Token clone() {
        return new Healer();
    }
}
