package castlepanic.base.tokens;

import castlepanic.Game;
import castlepanic.component.Token;

public class TrollMage extends MonsterBoss {
    public TrollMage() {
        super("Troll Mage", 3);
        description = "<html><b>" + name + "</b><br>All Monsters on the board move 1 space.</html>";
    }

    @Override
    public void doAction(Game game) {
        // move all monsters
        AllMoveEffect effect = new AllMoveEffect();
        effect.doAction(game);
    }

    @Override
    public Token clone() {
        return new TrollMage();
    }
}
