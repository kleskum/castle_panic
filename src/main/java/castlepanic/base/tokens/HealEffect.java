package castlepanic.base.tokens;

import castlepanic.Game;
import castlepanic.component.Token;

public class HealEffect extends Effect {
    private int myHeal;

    public HealEffect(int heal) {
        super("Heal " + heal);
        myHeal = heal;
    }

    @Override
    public void doAction(Game game) {
        for(Monster monster : game.getBoard().collectTokens(Monster.class)) {
            monster.heal(myHeal);
        }
    }

    @Override
    public Token clone() {
        return new HealEffect(myHeal);
    }
}
