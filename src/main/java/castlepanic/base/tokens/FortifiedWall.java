package castlepanic.base.tokens;

import castlepanic.Input;
import castlepanic.PanelRenderer;
import castlepanic.component.Token;

import java.awt.*;

public class FortifiedWall extends Wall {
    @Override
    public Token clone()
    {
        return new FortifiedWall();
    }

    @Override
    public void addFlame(Input input) {
        // adding a flame to a fority wall is the same as destroying it
        this.destroy(input);
    }

    @Override
    public void draw(Graphics g, Rectangle board, int iring, int iarc, int itoken) {
        int radius = board.width / 2;
        int arcSize = 360 / getRing().getArc().getBoard().getArcs().size();

        Point center = new Point(board.x + board.width / 2, board.y + board.height / 2);
        int arcDistance = radius / getRing().getArc().getRings().size();
        int towerSize = arcDistance / 3;
        int arcPosition = (getRing().getArc().getRings().size() - iring - 1) * arcDistance + arcDistance + 3;

        int tokenPosition = (iarc * arcSize) + arcSize / 2;

        Point pt = PanelRenderer.getBoardPoint(center, tokenPosition, arcPosition);

        g.setColor(Color.gray);
        g.fillRect(pt.x - towerSize / 2, pt.y - towerSize / 2, towerSize, towerSize);
        g.setColor(Color.black);
        g.drawRect(pt.x - towerSize / 2, pt.y - towerSize / 2, towerSize, towerSize);
    }
}
