package castlepanic.base.tokens;

import castlepanic.Input;
import castlepanic.base.board.Arc;
import castlepanic.Game;
import castlepanic.base.board.Ring;
import castlepanic.component.Token;
import castlepanic.darktitan.tokens.Agranok;
import castlepanic.darktitan.tokens.Hero;
import castlepanic.wizardstower.tokens.FlyingMonster;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BoulderEffect extends Effect {
    public BoulderEffect()
        {
            super("Boulder");
        }

    // returns true if the boulder should keep going
    protected boolean effectStructure(Input input, Structure structure, int count) {
        structure.destroy(input);
        input.addMessage(getName() + " destroyed " + structure.getName() + " at " + structure.getRing().getArc().getNumber());
        return false;
    }

    protected void slayMonsters(Input input, Ring ring) {
        for(Monster monster : ring.collectTokens(GroundMonster.class)) {
            monster.damage(input, monster.getHealth()); // slay is special
            input.addMessage(getName() + " slayed " + monster.getName());
        }
        for(Hero hero : ring.collectTokens(Hero.class)) {
            hero.getRing().removeToken(hero);
            input.addMessage(getName() + " slayed " + hero.getName());
        }
    }

    // returns true if the boulder should keep going
    private <T extends Structure> boolean dealWithStructure(Input input, Ring ring, Class<T> clazz, List<Structure> hitStructures) {
        T structure = ring.findTokenType(clazz);
        if(structure != null) {
            boolean keepGoing = effectStructure(input, structure, hitStructures.size());
            hitStructures.add(structure);
            return keepGoing;
        } else {
            return true;
        }
    }

    public void startFromRing(Input input, Ring startRing) {
        List<Arc> arcs = startRing.getArc().getBoard().getArcs();
        Arc arc = startRing.getArc();
        List<Structure> hitStructures = new ArrayList<Structure>();

        // move downward
        for(Ring ring : arc.getRingsOuterToInner()) {
            if(ring.getIndex() >= startRing.getIndex()) {
                // first look for wall
                if (!dealWithStructure(input, ring, FortifiedWall.class, hitStructures)) {
                    return;
                }
                if (!dealWithStructure(input, ring, Wall.class, hitStructures)) {
                    return;
                }

                // next look for tower
                if (!dealWithStructure(input, ring, Tower.class, hitStructures)) {
                    return;
                }
                // now slay monsters
                slayMonsters(input, ring);
            }
        }

        // uh oh... move across arc
        arc = arc.oppositeArc();

        // move upward
        for(Ring ring : arc.getRingsOuterToInner()) {
            // always slay monsters
            slayMonsters(input, ring);
            // then look for tower
            if(!dealWithStructure(input, ring, Tower.class, hitStructures)) {
                return;
            }

            // finally look for wall
            if (!dealWithStructure(input, ring, FortifiedWall.class, hitStructures)) {
                return;
            }
            if(!dealWithStructure(input, ring, Wall.class, hitStructures)) {
                return;
            }
        }
    }

    @Override
    public void doAction(Game game) {
        int arcIndex = game.getDie().roll(game.getInput());
        startFromRing(game.getInput(), game.getBoard().getArcs().get(arcIndex-1).getOuterMostRing());
    }

    @Override
    public Token clone() {
        return new BoulderEffect();
    }
}
