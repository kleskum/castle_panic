package castlepanic;

public enum MoveDirection {
    Normal,
    Clockwise,
    CounterClockwise,
    Back
}

