package castlepanic;

import castlepanic.base.tokens.Structure;

public interface DestroyListener {
    void structureDestroyed(Game game, Structure structure);
}
