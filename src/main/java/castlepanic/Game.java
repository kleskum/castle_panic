package castlepanic;

import castlepanic.base.board.Arc;
import castlepanic.base.board.Board;
import castlepanic.base.cards.BarbarianCard;
import castlepanic.base.cards.ScavengeCard;
import castlepanic.base.tokens.AllMoveEffect;
import castlepanic.base.tokens.Monster;
import castlepanic.base.tokens.MonsterBoss;
import castlepanic.base.tokens.Tower;
import castlepanic.base.tokens.Wall;
import castlepanic.chivalrousdeeds.tokens.Princess;
import castlepanic.component.Bag;
import castlepanic.component.Card;
import castlepanic.component.Deck;
import castlepanic.component.Die;
import castlepanic.component.Player;
import castlepanic.component.Token;
import castlepanic.darktitan.cards.CavalierCard;
import castlepanic.darktitan.tokens.Agranok;
import castlepanic.darktitan.tokens.Cavalier;
import castlepanic.darktitan.tokens.SupportToken;
import castlepanic.wizardstower.cards.BerserkCard;
import castlepanic.wizardstower.tokens.WizardTower;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Game {
    private Random random = new Random();
    private Board board = null;
    private Die die = null;

    private Deck deck = new Deck();
    private Deck wizardDeck = new Deck();

    private Bag bag = new Bag();
    private List<Player> players = new ArrayList<Player>();
    private Input input = null;
    private GameConfig configuration = new GameConfig();
    private int monstersToDraw = configuration.getMonsterDrawCount();
    private boolean drawOnHit = false;
    private List<SlayListener> slayListeners = new ArrayList<SlayListener>();
    private List<DestroyListener> destroyListeners = new ArrayList<DestroyListener>();
    private boolean skipPhase2 = false;

    private Player currentPlayer = null;
    private boolean gameOver = true;
    private Card lastSelectedCard = null;

    // dark titan expansion
    private Agranok agranok = null;

    // chivalrous deeds
    private Princess princess = null;

    public Game(int numPlayers, Input input) {
        // build board
        this.board = new Board(this.configuration);
        this.die = new Die(6);
        this.slayListeners.add(bag);

        // save inputs
        this.input = input;

        // add monster tokens
        this.configuration.addMonsterTokens(this);
 
        // add cards
        this.configuration.addCards(this);

        // set number of players
        changePlayers(numPlayers);

        // render
        input.redraw();
    }

    public Board getBoard() {
        return board;
    }

    public Die getDie() {
        return die;
    }

    public Deck getDeck() {
        return deck;
    }

    public Deck getWizardDeck() {
        return wizardDeck;
    }

    public Bag getBag() {
        return bag;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public Input getInput() {
        return input;
    }

    public void setMonstersToDraw(int count) {
        this.monstersToDraw = count;
    }

    public void setDrawOnHit(boolean draw) {
        this.drawOnHit = draw;
    }

    public List<SlayListener> getSlayListeners() {
        return slayListeners;
    }

    public List<DestroyListener> getDestroyListeners() {
        return destroyListeners;
    }

    public void setSkipPhase2(boolean skip) {
        this.skipPhase2 = skip;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void changePlayers(int numPlayers) {
        // force game over
        endGame();

        // clear players
        players.clear();

        // add players
        int maxCards = configuration.getMaxCardsPerPlayer(numPlayers);
        for (int i = 0; i < numPlayers; i++) {
            Player player = new Player(deck, wizardDeck, maxCards);
            players.add(player);
        }
    }

    public void setup() {
        // reset game
        gameOver = false;

        // start with first player
        currentPlayer = players.get(0);

        // remove cards from players
        for (Player player : players) {
            player.discardAll();
            player.resetScore();
        }

        // remove tokens from board
        board.clear();

        // gather up cards and shuffle
        deck.gather();
        deck.shuffle();

        wizardDeck.gather();
        wizardDeck.shuffle();

        // gather up tokens and mix
        bag.gather();
        bag.mix();

        // add walls and towers to castle ring
        for (Arc arc : board.getArcs()) {
            Wall wall = new Wall();
            wall.place(arc.getInnerMostRing());
            Tower tower = new Tower();
            tower.place(arc.getInnerMostRing());
        }

        // check expansion
        if(configuration.isWizardsTower()) {
            // replace a random tower with a wizards tower
            int iarc = die.roll(getInput()) - 1;
            Arc arc = board.getArcs().get(iarc);
            Tower tower = arc.getInnerMostRing().findTokenType(Tower.class);
            tower.getRing().removeToken(tower);
            WizardTower wizardTower = new WizardTower();
            wizardTower.place(arc.getRings().get(arc.getRings().size()-1));

            // remove tokens from base set
            for(int i = 0; i < 2; i++) {
                bag.findMonsterToken("Boulder");
            }
            bag.findMonsterToken("Green Monsters Move 1");
            bag.findMonsterToken("Blue Monsters Move 1");
            bag.findMonsterToken("Red Monsters Move 1");
            for(int i = 0; i < 6; i++) {
                bag.findMonsterToken("Goblin");
            }
            for(int i = 0; i < 6; i++) {
                bag.findMonsterToken("Orc");
            }
            for(int i = 0; i < 5; i++) {
                bag.findMonsterToken("Troll");
            }

            // add random mega bosses
            int numMegaBosses = configuration.isDarkTitan() ? 2 : 3;
            for(int i = 0; i < numMegaBosses && configuration.getMegaBossMonsters().size() > 0; i++) {
                int index = random.nextInt(configuration.getMegaBossMonsters().size());
                bag.addToken(configuration.getMegaBossMonsters().remove(index));
            }
        }

        if(configuration.isDarkTitan()) {
            if(configuration.isWizardsTower()) {
                bag.findMonsterToken("Phoenix");
                bag.findMonsterToken("Gargoyle");
                bag.findMonsterToken("Goblin Cavalry");
                bag.findMonsterToken("Climbing Troll");
                for (int i = 0; i < 2; i++) {
                    bag.findMonsterToken("Ogre");
                }
                bag.findMonsterToken("Troll");
                bag.findMonsterToken("Orc");
            } else {
                // remove tokens from base set
                for (int i = 0; i < 3; i++) {
                    bag.findMonsterToken("Goblin");
                }
                for (int i = 0; i < 4; i++) {
                    bag.findMonsterToken("Orc");
                }
                for (int i = 0; i < 3; i++) {
                    bag.findMonsterToken("Troll");
                }
            }

            // choose a random agranok
            int index = random.nextInt(configuration.getAgranoks().size());
            agranok = configuration.getAgranoks().get(index);

            // remove Barrage card from a 1 player game
            if(players.size() == 1) {
                deck.removeCards("Barrage");
            }
        }

        if(configuration.isChivalrousDeeds()) {
            // randomly place the princess on a tower
            princess = new Princess();
            destroyListeners.add(princess);

            // remove the knights cards and put in princess deck
            for(Card card : deck.removeCards("Knights")) {
                princess.getDeck().pushOnDraw(card);
            }

            princess.getDeck().shuffle();
            for(int i = 0; i < 6; i++) {
                princess.getHand().add(princess.getDeck().popFromDraw());
            }

            int num = die.roll(input);
            Tower tower = getBoard().getArcs().get(num-1).getInnerMostRing().findTokenType(Tower.class);
            princess.place(tower.getRing());
        }

        if(configuration.isWizardsTower()) {
            // starting monsters are random
            int arcNum = 0;
            while(arcNum < 6 && bag.getTokenCount() > 0) {
                Token token = bag.pick();
                if(token instanceof Monster && !(token instanceof MonsterBoss)) {
                    Monster monster =  (Monster)token;
                    monster.place(board.getArcs().get(arcNum).getRings().get(1));
                    monster.doAction(this);
                    arcNum++;
                } else {
                    bag.addToken(token);
                }
            }
        } else {
            // same startup for both Base and Dark Titan

            // start monsters in archer ring
            bag.findMonsterToken("Goblin").place(board.getArcs().get(0).getRings().get(1));
            bag.findMonsterToken("Orc").place(board.getArcs().get(1).getRings().get(1));
            bag.findMonsterToken("Goblin").place(board.getArcs().get(2).getRings().get(1));
            bag.findMonsterToken("Orc").place(board.getArcs().get(3).getRings().get(1));
            bag.findMonsterToken("Goblin").place(board.getArcs().get(4).getRings().get(1));
            bag.findMonsterToken("Troll").place(board.getArcs().get(5).getRings().get(1));
        }

        // deal out cards
        for (int i = 0; i < configuration.getMaxCardsPerPlayer(players.size()); i++) {
            for (Player player : players) {
                Card card = deck.popFromDraw();
                player.deal(card);
            }
        }

        // render
        input.redraw();
    }

    public void playGame() {
        // set up the game
        setup();

        // play until game over
        while (!gameOver()) {
            // 1. Draw Up
            currentPlayer.drawUp();
            input.redraw();
            if (gameOver()) break;

            // 2. Discard (Optional)
            if(!skipPhase2) {
                int numDiscards = configuration.getAllowedDiscards(players.size());
                int discarded = 0;
                while(discarded < numDiscards) {
                    input.addMessage("Choose a card to discard (optional).");
                    Card discard = input.requestCard();
                    if (discard != null) {
                        if (input.askYesNoQuestion("Are you sure you want to discard this card?")) {
                            currentPlayer.discard(discard);
                            if (configuration.isWizardsTower() && hasWizardTower() && wizardDeck.getCombinedCount() > 0 &&
                                    input.askYesNoQuestion("Do you want to draw from the wizard deck?")) {
                                currentPlayer.drawWizard();
                            } else {
                                currentPlayer.draw();
                            }
                            discarded++;
                            input.redraw();
                        }
                    } else {
                        break;
                    }
                }
            }
            input.redraw();
            if (gameOver()) break;

            // 3. Trade
            if (players.size() > 1 || princess != null) {
                Trade trade = input.requestTrade();
                if (trade != null && trade.MyCard != null && trade.OtherCard != null) {
                    if(trade.OtherPlayer != null) {
                        currentPlayer.discard(trade.MyCard);
                        trade.OtherPlayer.discard(trade.OtherCard);
                        currentPlayer.scavenge(trade.OtherCard);
                        trade.OtherPlayer.scavenge(trade.MyCard);
                    } else if(trade.OtherCard.play(this)) {
                        currentPlayer.discard(trade.MyCard);
                        princess.getDeck().getDiscardPile().add(trade.OtherCard);
                        princess.getHand().remove(trade.OtherCard);
                        princess.getHand().add(princess.getDeck().popFromDraw());
                    }
                }
            }
            input.redraw();
            if (gameOver()) break;

            // 4. Play Cards
            do {
                input.addMessage("Choose a card to play or Cancel.");
                Card card = input.requestCardToPlay();
                if(card != null) {

                    if(!playCard(card)) {
                        input.addMessage("You can't do that.");
                    }
                    input.resetNoSelect();
                }
                input.redraw();
            } while(!input.getNoSelection());
            if (gameOver()) break;

            // Move Cavalier
            moveCavalier();

            // 5. Move Monsters
            if(agranok != null) {
                agranok.setDoMoveAction(true); // reset the agranok's move action
            }
            AllMoveEffect move = new AllMoveEffect();
            move.doAction(this);
            input.redraw();
            if (gameOver()) break;

            // Burn Monsters
            burnMonsters();

            // 6. Draw Monsters
            for (int i = 0; i < monstersToDraw; i++) {
                drawMonsterToken();
                input.redraw();
            }
            if (gameOver()) break;

            // deal with new arrival monsters in the forest
            for(SupportToken supportToken : getBoard().collectTokens(SupportToken.class)) {
                if(supportToken.getRing() == supportToken.getRing().getArc().getOuterMostRing()) {
                    supportToken.battle(getInput());
                }
            }

            // at the end of the round, move the princes to a random new tower
            if(princess != null) {
                princess.move(this);
            }

            // End Turn
            endTurn();
            input.redraw();
        }

        // game over
        input.addMessage("Game ended.");
        input.redraw();
    }

    public void endGame() {
        gameOver = true;
    }

    public boolean hasWizardTower() {
        return board.collectTokens(WizardTower.class).size() > 0;
    }

    public boolean gameOver() {
        if (gameOver) {
            return true;
        }

        // check if monsters remain
        int monsterCount = board.collectTokens(Monster.class).size();
        if (monsterCount == 0 && bag.getTokenCount() == 0) {
            return true;
        }

        // check if towers remain
        if (board.collectTokens(Tower.class).size() == 0) {
            return true;
        }

        // game isn't over
        return false;
    }
        
    public void drawMonsterToken() {
        Token token = bag.pick();
        if (token == null) {
            return;
        }
        input.addMessage("Drew " + token.getName());
        if(token instanceof Monster || token instanceof SupportToken) {
            int arcIndex = die.roll(input);
            token.place(board.getArcs().get(arcIndex - 1).getOuterMostRing());
        }
        token.doAction(this);
    }

    public Card getLastSelectedCard() {
        return lastSelectedCard;
    }

    public boolean playCard(Card card) {
        lastSelectedCard = card;
        if(card.play(this)) {
            if(drawOnHit && !(card instanceof BerserkCard) && card.getDescription().toLowerCase().contains("hit")) {
                // draw a card on hits
                currentPlayer.draw();
            }
            currentPlayer.discard(card);

            input.redraw();
            return true;
        } else {
            return false;
        }
    }

    public void burnMonsters() {
        for(Monster monster : board.collectTokens(Monster.class)) {
            monster.burn(input);
        }
    }

    public void moveCavalier() {
        List<Cavalier> cavaliers = board.collectTokens(Cavalier.class);
        if(cavaliers.size() > 0) {
            cavaliers.get(0).doAction(this);
        }
    }

    public void endTurn() {
        // untar any characters
        for(Monster monster : board.collectTokens(Monster.class)) {
            monster.setTarred(false);
        }

        // reset monster draw count
        monstersToDraw = configuration.getMonsterDrawCount();

        // don't draw after hits
        drawOnHit = false;

        // check that the agranok has at least 2HP
        if(agranok != null && agranok.getHealth() == 1) {
            agranok.heal();
        }

        // turn to next player
        int playerIndex = players.indexOf(currentPlayer) + 1;
        if (playerIndex >= players.size()) {
            playerIndex = 0;
        }
        currentPlayer = players.get(playerIndex);
    }

    public Agranok getAgranok() {
        return agranok;
    }

    public Princess getPrincess() {
        return princess;
    }
}
