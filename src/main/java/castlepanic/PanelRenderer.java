package castlepanic;

import castlepanic.base.board.Arc;
import castlepanic.base.board.Ring;
import castlepanic.base.cards.AttackCard;
import castlepanic.base.cards.MaterialCard;
import castlepanic.base.cards.SpecialAttackCard;
import castlepanic.base.cards.SpecialCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;
import castlepanic.component.Expansion;
import castlepanic.component.Player;
import castlepanic.component.Token;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.awt.geom.Arc2D;
import java.util.ArrayList;

public class PanelRenderer extends JPanel implements Renderer {
    private GameForm gameForm;

    public PanelRenderer(GameForm gameForm) {
        this.gameForm = gameForm;
    }

    public static Point getBoardPoint(Point center, int degrees, int radius) {
        Point centerPoint = new Point(0, 0);
        Point result = new Point(0, 0);
        double angle = degrees * Math.PI / 180;
        result.y = center.y + (int)Math.round(radius * Math.sin(angle));
        result.x = center.x + (int)Math.round(radius * Math.cos(angle));
        return result;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (gameForm.Game != null) {
            Font left = new Font("Arial", Font.PLAIN, 12);
            {
                g.setFont(left);
                g.setColor(Color.black);
                g.drawString(gameForm.Game.getBag().getTokenCount().toString(), 10, 10);
                g.drawString(Integer.toString(gameForm.Game.getDeck().getDrawCount()), 10, 40);
            }

            int maxD = Math.min(this.getWidth(), this.getHeight()) - 120;

            Rectangle board = new Rectangle(this.getWidth() / 2 - maxD / 2, 10, maxD, maxD);
            Point center = new Point(board.x + board.width / 2, board.y + board.height / 2);

            int radius = maxD / 2;
            int arcSize = 360 / gameForm.Game.getBoard().getArcs().size();

            // fill board
            g.setColor(Color.green.darker());
            g.fillOval(board.x, board.y, board.width, board.height);

            // fill arcs
            int arcAngle = 0;
            for (Arc arc : gameForm.Game.getBoard().getArcs()) {
                if (arc.getColor().equals(Color.red)) {
                    g.setColor(Color.blue);
                } else if (arc.getColor().equals(Color.blue)) {
                    g.setColor(Color.red);
                } else {
                    g.setColor(arc.getColor());
                }
                {
                    int arcDistance = radius / arc.getRings().size();
                    int arcPosition = arcDistance;
                    int arcWidth = board.width - arcPosition * 2;
                    Rectangle level = new Rectangle(board.x + arcPosition, board.y + arcPosition, arcWidth, arcWidth);

                    Arc2D.Float arc2d = new Arc2D.Float(Arc2D.PIE);
                    arc2d.setFrame(level);
                    arc2d.setAngleStart(arcAngle);
                    arc2d.setAngleExtent(arcSize);
                    ((Graphics2D)g).fill(arc2d);

                    arcPosition += (arc.getRings().size() - 2) * arcDistance;
                    arcWidth = board.width - arcPosition * 2;
                    level = new Rectangle(board.x + arcPosition, board.y + arcPosition, arcWidth, arcWidth);

                    g.setColor(Color.lightGray);
                    arc2d = new Arc2D.Float(Arc2D.PIE);
                    arc2d.setFrame(level);
                    arc2d.setAngleStart(arcAngle);
                    arc2d.setAngleExtent(arcSize);
                    ((Graphics2D)g).fill(arc2d);
                }

                arcAngle += arcSize;
            }

            // draw arcs
            arcAngle = 0;
            for (Arc arc : gameForm.Game.getBoard().getArcs()) {
                g.setColor(Color.black);
                Arc2D.Float arc2d = new Arc2D.Float(Arc2D.PIE);
                arc2d.setFrame(board);
                arc2d.setAngleStart(arcAngle);
                arc2d.setAngleExtent(arcSize);
                ((Graphics2D)g).draw(arc2d);

                int arcDistance = radius / arc.getRings().size();
                int arcPosition = arcDistance;
                for (Ring ring : arc.getRings()) {
                    int arcWidth = board.width - arcPosition * 2;
                    if (arcWidth > 0) {
                        Rectangle level = new Rectangle(board.x + arcPosition, board.y + arcPosition, arcWidth, arcWidth);
                        arc2d = new Arc2D.Float(Arc2D.PIE);
                        arc2d.setFrame(level);
                        arc2d.setAngleStart(arcAngle);
                        arc2d.setAngleExtent(arcSize);
                        ((Graphics2D)g).draw(arc2d);
                    }
                    arcPosition += arcDistance;
                }

                arcAngle += arcSize;
            }

            // draw tokens
            for(int iarc = 0; iarc < gameForm.Game.getBoard().getArcs().size(); iarc++) {
                Arc arc = gameForm.Game.getBoard().getArcs().get(iarc);
                for(int iring = 0; iring < arc.getRings().size(); iring++) {
                    Ring ring = arc.getRings().get(iring);
                    for(int itoken = 0; itoken < ring.getTokens().size(); itoken++) {
                        Token token = ring.getTokens().get(itoken);
                        token.draw(g, board, iring, iarc, itoken);
                    }
                }
            }

            // draw cards
            int x = 20;
            int y = this.getHeight() - 90;
            int cardWidth = 60;
            int cardHeight = 80;
            for (Player player : gameForm.Game.getPlayers()) {
                Font score = new Font("Arial", Font.PLAIN, 10);
                g.setFont(score);
                {
                    g.setColor(Color.black);
                    g.drawString(player.getScore().toString(), x, y - 15);
                }

                for (int i = 0; i < player.getHand().size(); i++) {
                    Card card = player.getHand().get(i);
                    drawCard(g, card, x, y, cardWidth, cardHeight);
                    x += cardWidth + 10;
                }
            }

            if(gameForm.Game.getPrincess() != null) {
                for (int i = 0; i < gameForm.Game.getPrincess().getHand().size(); i++) {
                    Card card = gameForm.Game.getPrincess().getHand().get(i);
                    drawCard(g, card, x, y, cardWidth, cardHeight);
                    x += cardWidth + 10;
                }
            }

            g.setColor(Color.red);
            g.fillOval(x, y, cardWidth, cardWidth);
            g.setColor(Color.black);
            g.drawOval(x, y, cardWidth, cardWidth);
        }
    }

    public static void drawCenteredString(Graphics g, String text, Rectangle rect, Font font) {
        TextRenderer.drawString(g, text, font, g.getColor(), rect, TextAlignment.MIDDLE, TextFormat.FIRST_LINE_VISIBLE);
    }

    public static void drawCard(Graphics g, Card card, int x, int y, int cardWidth, int cardHeight) {
        Rectangle bounds = new Rectangle(x, y, cardWidth, cardHeight);
        g.setColor(Color.white);
        g.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);

        if(card.getExpansion() == Expansion.WizardsTower) {
            g.setColor(Color.lightGray);
        } else {
            g.setColor(Color.black);
        }
        g.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);

        if (card instanceof SpecialAttackCard) {
            SpecialAttackCard special = (SpecialAttackCard)card;
            g.setColor(special.getColor());
            g.fillRect(x + 5, y + 5, cardWidth - 10, 10);

            if(special.getTypes().contains("Forest")) {
                g.setColor(Color.green.darker());
                g.fillRect(x + 5, y + 5, cardWidth - 10, 5);
            }

            g.setColor(Color.black);
            if(special.getTypes().contains("Castle")) {
                g.fillRect(x + cardWidth - 15, y + 5, 10, 10);
            }
            drawCenteredString(g, special.getName(), bounds, new Font("Arial", Font.PLAIN, 9));
        } else if (card instanceof AttackCard) {
            AttackCard attack = (AttackCard)card;
            int icolor = 0;
            int colorWidth = (cardWidth - 10) / attack.getColors().size();
            for (Color color : attack.getColors()) {
                g.setColor(color);
                {
                    g.fillRect(x + 5 + icolor * colorWidth, y + 5, colorWidth, 10);
                }
                icolor++;
            }
            String name = attack.getName();
            g.setColor(Color.black);
            drawCenteredString(g, name, bounds, new Font("Arial", Font.PLAIN, 9));
        } else if (card instanceof MaterialCard) {
            MaterialCard material = (MaterialCard)card;
            g.setColor(Color.gray);
            g.fillRect(x + 5, y + 5, cardWidth - 10, 10);
            g.setColor(Color.black);
            drawCenteredString(g, material.getName(), bounds, new Font("Arial", Font.PLAIN, 9));
        } else if (card instanceof SpecialCard) {
            SpecialCard special = (SpecialCard)card;
            g.setColor(Color.magenta);
            g.fillRect(x + 5, y + 5, cardWidth - 10, 10);
            g.setColor(Color.black);
            drawCenteredString(g, special.getName(), bounds, new Font("Arial", Font.PLAIN, 9));
        }
    }

    public Card getCardAt(Point pt) {
        int x = 20;
        int y = this.getHeight() - 90;
        int cardWidth = 60;
        int cardHeight = 80;
        for (Player player : gameForm.Game.getPlayers()) {
            for (Card card : player.getHand()) {
                Rectangle bounds = new Rectangle(x, y, cardWidth, cardHeight);
                if (bounds.contains(pt)) {
                    return card;
                }
                x += cardWidth + 10;
            }
        }
        if(gameForm.Game.getPrincess() != null) {
            for(Card card : gameForm.Game.getPrincess().getHand()) {
                Rectangle bounds = new Rectangle(x, y, cardWidth, cardHeight);
                if (bounds.contains(pt)) {
                    return card;
                }
                x += cardWidth + 10;
            }
        }
        return null;
    }

    public <T extends Token> T getTokenAt(Point point, Class<T> clazz) {
        int maxD = Math.min(this.getWidth(), this.getHeight()) - 120;
        int radius = maxD / 2;
        int arcSize = 360 / gameForm.Game.getBoard().getArcs().size();
        Rectangle board = new Rectangle(this.getWidth() / 2 - maxD / 2, 10, maxD, maxD);
        Point center = new Point(board.x + board.width / 2, board.y + board.height / 2);

        int iarc = 0;
        for (Arc arc : gameForm.Game.getBoard().getArcs()) {
            int iring = 0;
            for (Ring ring : arc.getRings()) {
                int itoken = 0;
                for (Token token : ring.getTokens()) {
                    if (clazz.isInstance(token)) {
                        T monster = (T)token;

                        int arcDistance = radius / arc.getRings().size();
                        int monsterSize = arcDistance / 2;
                        int arcPosition = (arc.getRings().size() - iring - 1) * arcDistance + arcDistance / 2;

                        int arcPerToken = arcSize / ring.getTokens().size();
                        int tokenPosition = (iarc * arcSize) + (arcPerToken * itoken) + (arcPerToken / 2);

                        Point pt = getBoardPoint(center, tokenPosition, arcPosition);
                        Rectangle bounds = new Rectangle(pt.x - monsterSize / 2, pt.y - monsterSize / 2, monsterSize, monsterSize);

                        if (bounds.contains(point)) {
                            return monster;
                        }
                    }
                    itoken++;
                }
                iring++;
            }
            iarc++;
        }
        return null;
    }

    private boolean inPolygonBounds(Point pt, Point[] _pts) {
        int minx = Integer.MAX_VALUE;
        int maxx = Integer.MIN_VALUE;
        int miny = Integer.MAX_VALUE;
        int maxy = Integer.MIN_VALUE;
        for (Point _pt : _pts) {
            minx = Math.min(minx, _pt.x);
            maxx = Math.max(maxx, _pt.x);
            miny = Math.min(miny, _pt.y);
            maxy = Math.max(maxy, _pt.y);
        }
        Rectangle bounds = new Rectangle(minx, miny, maxx - miny, maxy - miny);
        return bounds.contains(pt);
    }

    private boolean polygonContains(Point pt, Point[] _pts) {
        int NumberOfPoints = _pts.length;
        boolean isIn = false;
        if (inPolygonBounds(pt, _pts)) {
            int i = 0, j = 0;
            for (j = NumberOfPoints - 1; i < NumberOfPoints; j = i++) {
                if (
                        (
                         ((_pts[i].y <= pt.y) && (pt.y < _pts[j].y)) || ((_pts[j].y <= pt.y) && (pt.y < _pts[i].y))
                        ) &&
                        (pt.x < (_pts[j].x - _pts[i].x) * (pt.y - _pts[i].y) / (_pts[j].y - _pts[i].y) + _pts[i].x)
                       )
                {
                    isIn = !isIn;
                }
            }
        }
        return isIn;
    }

    public Ring getRingAt(Point point) {
        int maxD = Math.min(this.getWidth(), this.getHeight()) - 120;
        int radius = maxD / 2;
        int arcSize = 360 / gameForm.Game.getBoard().getArcs().size();
        Rectangle board = new Rectangle(this.getWidth() / 2 - maxD / 2, 10, maxD, maxD);
        Point center = new Point(board.x + board.width / 2, board.y + board.height / 2);

        int arcAngle = 0;
        for(Arc arc : gameForm.Game.getBoard().getArcs()) {
            int arcDistance = radius / arc.getRings().size();
            for(int iring = 0; iring < arc.getRings().size(); iring++) {
                Ring ring = arc.getRings().get(arc.getRings().size() - (iring+1));
                int dist1 = arcDistance * iring;
                int dist2 = arcDistance * (iring+1);

                List<Point> pts = new ArrayList<Point>();

                // add a point every 5 degrees
                for(int x = arcAngle; x <= arcAngle + arcSize; x += 5) {
                    pts.add(getBoardPoint(center, x, dist2));
                }
                for(int x = arcAngle + arcSize; x >= arcAngle; x -= 5) {
                    pts.add(getBoardPoint(center, x, dist1));
                }

                if (polygonContains(point, pts.toArray(new Point[0]))) {
                    return ring;
                }
            }
            arcAngle += arcSize;
        }
        return null;
    }

    public boolean getCancelAt(Point point) {
        int x = 20;
        int y = this.getHeight() - 90;
        int cardWidth = 60;
        for (Player player : gameForm.Game.getPlayers()) {
            for (Card card : player.getHand()) {
                x += cardWidth + 10;
            }
        }
        if(gameForm.Game.getPrincess() != null) {
            for(Card card : gameForm.Game.getPrincess().getHand()) {
                x += cardWidth + 10;
            }
        }
        Rectangle bounds = new Rectangle(x, y, cardWidth, cardWidth);
        return bounds.contains(point);
    }
}
