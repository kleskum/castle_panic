package castlepanic;

import castlepanic.base.tokens.Monster;

public interface SlayListener {
    void monsterSlayed(Game game, Monster monster);
}
