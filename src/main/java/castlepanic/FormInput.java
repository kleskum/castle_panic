package castlepanic;

import castlepanic.base.board.Ring;
import castlepanic.base.tokens.Monster;
import castlepanic.base.tokens.Structure;
import castlepanic.base.ui.DiscardForm;
import castlepanic.component.Card;
import castlepanic.component.Player;
import castlepanic.component.Token;
import castlepanic.darktitan.tokens.SupportToken;
import castlepanic.wizardstower.tokens.Basilisk;
import castlepanic.wizardstower.ui.ChooseCardForm;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class FormInput implements Input {
    private GameForm myForm;

    public FormInput(GameForm form) {
        myForm = form;
    }

    public void redraw() {
        myForm.repaint();
    }

    public void addMessage(String msg) {
        synchronized (this) {
            myForm.addMessage(msg);
        }
    }

    public Card requestCard() {
        return requestCard(myForm.Game.getCurrentPlayer());
    }

    public Card requestAnyCard() {
        return requestCard(null);
    }

    public Card requestCard(Player player) {
        synchronized (this) {
            try {
                myForm.clearCardSelection();
                this.wait();
                Card card = myForm.getCardSelection();
                if(player != null) {
                    while (card != null && myForm.getCardOwner(card) != player) {
                        this.wait();
                        card = myForm.getCardSelection();
                    }
                }
                return card;
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public Trade requestTrade() {
        synchronized (this) {;
            if(myForm.Game.getInput().askYesNoQuestion("Do you want to trade?")) {
                Trade trade = new Trade();
                myForm.Game.getInput().addMessage("Choose one of your cards to trade.");
                trade.MyCard = myForm.Game.getInput().requestCard(myForm.Game.getCurrentPlayer());
                myForm.Game.getInput().addMessage("Choose another player's card to trade.");
                trade.OtherCard = myForm.Game.getInput().requestAnyCard();
                trade.OtherPlayer = myForm.getCardOwner(trade.OtherCard);
                return trade;
            } else {
                return null;
            }
        }
    }

    public Card requestCardToPlay() {
        synchronized (this) {
            try {
                myForm.clearCardSelection();
                this.wait();
                Card card = myForm.getCardSelection();
                // don't return a monster as a selection
                while ((card != null && myForm.getCardOwner(card) != myForm.Game.getCurrentPlayer()) || myForm.getTokenSelection() != null) {
                    this.wait();
                    card = myForm.getCardSelection();
                }
                return card;
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public <T extends Token> T chooseToken(Class<T> clazz) {
        synchronized (this) {
            try {
                myForm.clearTokenSelection();
                this.wait();
                Token token = myForm.getTokenSelection();
                if(clazz.isInstance(token)) {
                    return (T)token;
                } else {
                    return null;
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public Ring chooseRing() {
        synchronized (this) {
            try {
                myForm.clearRingSelection();
                this.wait();
                Ring ring = myForm.getRingSelection();
                return ring;
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public Card chooseCardFromDiscard() {
        synchronized (this) {
            DiscardForm form = new DiscardForm(myForm, myForm.Game);
            form.setVisible(true);
            Card card = form.getSelectedCard();
            return card;
        }
    }

    public Card chooseCard(List<Card> cards) {
        synchronized (this) {
            ChooseCardForm form = new ChooseCardForm(myForm, cards);
            form.setVisible(true);
            Card card = form.SelectedCard;
            return card;
        }
    }

    public boolean getNoSelection() {
        return myForm.getNoSelection();
    }

    public void resetNoSelect() {
        myForm.clearNoSelection();
    }
    public void notifyMonsterSlayed(Monster monster) {
        for(int i = myForm.Game.getSlayListeners().size() - 1; i >= 0; i--) {
            SlayListener slayListener = myForm.Game.getSlayListeners().get(i);
            slayListener.monsterSlayed(myForm.Game, monster);
        }
        if(monster instanceof Basilisk) {
            myForm.Game.setSkipPhase2(false);
        }
    }

    public void notifyStructureDestroyed(Structure structure) {
        for(int i = myForm.Game.getDestroyListeners().size() - 1; i >= 0; i--) {
            DestroyListener destroyListener = myForm.Game.getDestroyListeners().get(i);
            destroyListener.structureDestroyed(myForm.Game, structure);
        }
    }

    public boolean askYesNoQuestion(String question) {
        int result = JOptionPane.showConfirmDialog(null, question, "Choice", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        return result == 0;
    }
}

