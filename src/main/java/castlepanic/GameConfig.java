package castlepanic;

import castlepanic.base.cards.AttackCard;
import castlepanic.base.cards.BarbarianCard;
import castlepanic.base.cards.DrawCardsCard;
import castlepanic.base.cards.DriveHimBackCard;
import castlepanic.base.cards.FortifyWallCard;
import castlepanic.base.cards.MaterialCard;
import castlepanic.base.cards.MissingCard;
import castlepanic.base.cards.NiceShotCard;
import castlepanic.base.cards.ScavengeCard;
import castlepanic.base.cards.TarCard;
import castlepanic.base.tokens.ArcMoveEffect;
import castlepanic.base.tokens.BoulderEffect;
import castlepanic.base.tokens.ColorMoveEffect;
import castlepanic.base.tokens.DiscardEffect;
import castlepanic.base.tokens.DrawMonsterEffect;
import castlepanic.base.tokens.GoblinKing;
import castlepanic.base.tokens.GroundMonster;
import castlepanic.base.tokens.Healer;
import castlepanic.base.tokens.Monster;
import castlepanic.base.tokens.OrcWarlord;
import castlepanic.base.tokens.PlagueEffect;
import castlepanic.base.tokens.TrollMage;
import castlepanic.component.Bag;
import castlepanic.component.Card;
import castlepanic.component.Deck;
import castlepanic.component.Token;
import castlepanic.darktitan.cards.BarrageCard;
import castlepanic.darktitan.cards.BoilingOilCard;
import castlepanic.darktitan.cards.CavalierCard;
import castlepanic.darktitan.tokens.Agranok;
import castlepanic.darktitan.tokens.AgranokLevel1;
import castlepanic.darktitan.tokens.AgranokLevel2;
import castlepanic.darktitan.tokens.AgranokLevel3;
import castlepanic.darktitan.tokens.AgranokLevel4;
import castlepanic.darktitan.tokens.AgranokLevel5;
import castlepanic.darktitan.tokens.BoomMonster;
import castlepanic.darktitan.tokens.ColorPlagueEffect;
import castlepanic.darktitan.tokens.DarkSorceress;
import castlepanic.darktitan.tokens.EliteMonster;
import castlepanic.darktitan.tokens.HeraldEffect;
import castlepanic.darktitan.tokens.ReserveSquad;
import castlepanic.darktitan.tokens.StonemasonsCart;
import castlepanic.darktitan.tokens.SupplyWagon;
import castlepanic.darktitan.tokens.WitherEffect;
import castlepanic.wizardstower.cards.BerserkCard;
import castlepanic.wizardstower.cards.ChangeColorCard;
import castlepanic.wizardstower.cards.ChangeRangeCard;
import castlepanic.wizardstower.cards.DoubleStrikeCard;
import castlepanic.wizardstower.cards.EnchantedCard;
import castlepanic.wizardstower.cards.FlamingCard;
import castlepanic.wizardstower.cards.KnockBackCard;
import castlepanic.wizardstower.cards.NeverLoseHopeCard;
import castlepanic.wizardstower.cards.ReinforceCard;
import castlepanic.wizardstower.cards.StandTogetherCard;
import castlepanic.wizardstower.cards.wizard.ArcaneAssemblyCard;
import castlepanic.wizardstower.cards.wizard.AzrielsFistCard;
import castlepanic.wizardstower.cards.wizard.BurningBlastCard;
import castlepanic.wizardstower.cards.wizard.ChainLightningCard;
import castlepanic.wizardstower.cards.wizard.ExtinguishingWindCard;
import castlepanic.wizardstower.cards.wizard.EyeOfTheOracleCard;
import castlepanic.wizardstower.cards.wizard.FireballCard;
import castlepanic.wizardstower.cards.wizard.HammerOfLight;
import castlepanic.wizardstower.cards.wizard.HypnotizeCard;
import castlepanic.wizardstower.cards.wizard.LightningBoltCard;
import castlepanic.wizardstower.cards.wizard.MysticalManufacturingCard;
import castlepanic.wizardstower.cards.wizard.RainOfIceCard;
import castlepanic.wizardstower.cards.wizard.RainOfIronCard;
import castlepanic.wizardstower.cards.wizard.RingOfFireCard;
import castlepanic.wizardstower.cards.wizard.TeleportCard;
import castlepanic.wizardstower.cards.wizard.ThalgarsBlessing;
import castlepanic.wizardstower.cards.wizard.ValadorsWaveCard;
import castlepanic.wizardstower.cards.wizard.WallOfForceCard;
import castlepanic.wizardstower.cards.wizard.WarStormCard;
import castlepanic.wizardstower.cards.wizard.WizardQuakeCard;
import castlepanic.wizardstower.tokens.Basilisk;
import castlepanic.wizardstower.tokens.CavalryMonster;
import castlepanic.wizardstower.tokens.Chimera;
import castlepanic.wizardstower.tokens.ClimbingMonster;
import castlepanic.wizardstower.tokens.Conjurer;
import castlepanic.wizardstower.tokens.Doppelganger;
import castlepanic.wizardstower.tokens.Dragon;
import castlepanic.wizardstower.tokens.FlamingBoulderEffect;
import castlepanic.wizardstower.tokens.FlyingMonster;
import castlepanic.wizardstower.tokens.Hydra;
import castlepanic.wizardstower.tokens.Necromancer;
import castlepanic.wizardstower.tokens.OneImpPerTowerEffect;
import castlepanic.wizardstower.tokens.Phoenix;
import castlepanic.wizardstower.tokens.SlayAndMissMonster;
import castlepanic.wizardstower.tokens.TrebuchetEffect;
import castlepanic.wizardstower.tokens.Warlock;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GameConfig {
    private boolean wizardsTower = false;
    private boolean darkTitan = true;
    private boolean chivalrousDeeds = false; // i made this one up

    private Color[] arcColors = new Color[] {Color.red, Color.green, Color.blue};
    private String[] arcColorNames = new String[] {"Red", "Green", "Blue"};
    private int arcsPerColor = 2;
    private String[] ringNames = new String[] {"Forest", "Archers", "Knights", "Swordsmen", "Castle"};
    private int monsterDrawCount = 2;

    private List<Monster> megaBossMonsters = new ArrayList<Monster>();
    private List<Agranok> agranoks = new ArrayList<Agranok>();

    public Color[] getArcColors() {
        return arcColors;
    }

    public String[] getArcColorNames() {
        return arcColorNames;
    }

    public int getArcsPerColor() {
        return arcsPerColor;
    }

    public String[] getRingNames() {
        return ringNames;
    }

    public boolean isWizardsTower() {
        return wizardsTower;
    }

    public boolean isDarkTitan() {
        return darkTitan;
    }

    public boolean isChivalrousDeeds() {
        return chivalrousDeeds;
    }

    public int getMonsterDrawCount() {
        return monsterDrawCount;
    }

    public List<Monster> getMegaBossMonsters() {
        return megaBossMonsters;
    }

    public List<Agranok> getAgranoks() {
        return agranoks;
    }

    public int getMaxCardsPerPlayer(int numPlayers) {
        switch (numPlayers) {
            case 1:
            case 2:
                return 6;
            case 3:
            case 4:
            case 5:
                return 5;
            default:
                return 4;
        }
    }

    public int getAllowedDiscards(int numPlayers) {
        return numPlayers == 1 ? 2 : 1;
    }

    public void addCards(Game game) {
        String[] noCastle = Arrays.asList(ringNames).subList(0, ringNames.length-1).toArray(new String[0]);
        String[] noForest = Arrays.asList(ringNames).subList(1, ringNames.length).toArray(new String[0]);
        String[] typeNames = Arrays.asList(noForest).subList(0, noForest.length-1).toArray(new String[0]);

        // attack cards
        for (int i = 0; i < arcColors.length; i++) {
            for (String type : typeNames) {
                addCards(game.getDeck(), new AttackCard(type, arcColorNames[i], arcColors[i]), 3);
            }
            addCards(game.getDeck(), new AttackCard(typeNames, arcColorNames[i], arcColors[i]), 1);
        }
        for (String type : typeNames) {
            addCards(game.getDeck(), new AttackCard(type, arcColorNames, arcColors), 1);
        }
            
        // material cards
        addCards(game.getDeck(), new MaterialCard("Brick"), 4);
        addCards(game.getDeck(), new MaterialCard("Mortar"), 4);

        // special attacks
        addCards(game.getDeck(), new BarbarianCard(noForest, arcColors), 1);
        addCards(game.getDeck(), new DriveHimBackCard(noForest, arcColors), 1);
        addCards(game.getDeck(), new TarCard(ringNames, arcColors), 1);

        // specials
        addCards(game.getDeck(), new FortifyWallCard(), 1);
        addCards(game.getDeck(), new DrawCardsCard(2), 1);
        addCards(game.getDeck(), new NiceShotCard(), 1);
        addCards(game.getDeck(), new MissingCard(), 1);
        addCards(game.getDeck(), new ScavengeCard(), 1);

        if(wizardsTower) {
            // wizard's deck
            for (int i = 0; i < arcColors.length; i++) {
                addCards(game.getWizardDeck(), new FireballCard(arcColorNames[i], ringNames, arcColors[i]), 1);
            }

            addCards(game.getWizardDeck(), new BurningBlastCard(typeNames, arcColors), 1);
            addCards(game.getWizardDeck(), new RingOfFireCard("Swordsmen", arcColors), 1);
            addCards(game.getWizardDeck(), new ThalgarsBlessing(), 1);
            addCards(game.getWizardDeck(), new HammerOfLight("Forest", arcColors), 1);
            addCards(game.getWizardDeck(), new EyeOfTheOracleCard(5), 1);
            addCards(game.getWizardDeck(), new ExtinguishingWindCard(), 1);
            addCards(game.getWizardDeck(), new HypnotizeCard(noForest, arcColors), 1);
            addCards(game.getWizardDeck(), new WarStormCard(typeNames, arcColors), 1);
            addCards(game.getWizardDeck(), new LightningBoltCard(noForest, arcColors), 1);
            addCards(game.getWizardDeck(), new RainOfIronCard(noForest, arcColors), 1);
            addCards(game.getWizardDeck(), new WallOfForceCard(noForest, arcColors), 1);
            addCards(game.getWizardDeck(), new AzrielsFistCard(ringNames, arcColors), 1);
            addCards(game.getWizardDeck(), new RainOfIceCard(), 1);
            addCards(game.getWizardDeck(), new ChainLightningCard(ringNames, arcColors), 1);
            addCards(game.getWizardDeck(), new ValadorsWaveCard(4, ringNames, arcColors), 1);
            addCards(game.getWizardDeck(), new TeleportCard(), 1);
            addCards(game.getWizardDeck(), new WizardQuakeCard(), 1);
            addCards(game.getWizardDeck(), new MysticalManufacturingCard(), 1);
            addCards(game.getWizardDeck(), new ArcaneAssemblyCard(), 1);

            // new castle cards
            addCards(game.getDeck(), new ChangeColorCard(), 1);
            addCards(game.getDeck(), new FlamingCard(), 1);
            addCards(game.getDeck(), new BerserkCard(), 1);
            addCards(game.getDeck(), new ReinforceCard(), 1);
            addCards(game.getDeck(), new EnchantedCard(), 1);
            addCards(game.getDeck(), new DoubleStrikeCard(), 1);
            addCards(game.getDeck(), new NeverLoseHopeCard(), 1);
            addCards(game.getDeck(), new ChangeRangeCard(), 1);
            addCards(game.getDeck(), new KnockBackCard(), 1);
            addCards(game.getDeck(), new StandTogetherCard(), 1);
        }

        if(darkTitan) {
            // new castle cards
            for (int i = 0; i < arcColors.length; i++) {
                addCards(game.getDeck(), new BoilingOilCard("Swordsmen", arcColorNames[i], arcColors[i]), 1);
            }
            addCards(game.getDeck(), new CavalierCard(), 1);
            addCards(game.getDeck(), new BarrageCard(), 1);
        }
    }

    private void addCards(Deck deck, Card card, int count) {
        for (int i = 0; i < count; i++) {
            deck.pushOnDraw(card.clone());
        }
    }

    public void addMonsterTokens(Game game) {
        String[] ringNames = this.ringNames;
        String[] typeNames = Arrays.asList(ringNames).subList(1, ringNames.length - 1).toArray(new String[0]);

        // monsters
        addMonsterTokens(game.getBag(), new GroundMonster("Goblin", 1), 6);
        addMonsterTokens(game.getBag(), new GroundMonster("Orc", 2), 11);
        addMonsterTokens(game.getBag(), new GroundMonster("Troll", 3), 10);

        // color moves
        for (int i = 0; i < arcColors.length; i++) {
            addMonsterTokens(game.getBag(), new ColorMoveEffect(arcColorNames[i], arcColors[i]), 2);
        }

        // arc moves
        addMonsterTokens(game.getBag(), new ArcMoveEffect(true), 1);
        addMonsterTokens(game.getBag(), new ArcMoveEffect(false), 1);

        // plagues
        for (String type : typeNames) {
            addMonsterTokens(game.getBag(), new PlagueEffect(type), 1);
        }

        // discard
        addMonsterTokens(game.getBag(), new DiscardEffect(1), 1);

        // draw monsters
        addMonsterTokens(game.getBag(), new DrawMonsterEffect(3), 1);
        addMonsterTokens(game.getBag(), new DrawMonsterEffect(4), 1);

        // boulders
        addMonsterTokens(game.getBag(), new BoulderEffect(), 4);

        // boss monsters
        addMonsterTokens(game.getBag(), new GoblinKing(), 1);
        addMonsterTokens(game.getBag(), new OrcWarlord(), 1);
        addMonsterTokens(game.getBag(), new TrollMage(), 1);
        addMonsterTokens(game.getBag(), new Healer(), 1);

        if(wizardsTower) {
            // new tokens
            addMonsterTokens(game.getBag(), new GroundMonster("Ogre", 4), 3);
            addMonsterTokens(game.getBag(), new SlayAndMissMonster("Golem", 3, "Knights", "Swordsmen"), 1);
            addMonsterTokens(game.getBag(), new SlayAndMissMonster("Centaur", 3, "Archers", "Knights"), 1);
            addMonsterTokens(game.getBag(), new SlayAndMissMonster("Cyclops", 3, "Swordsmen", "Archers"), 1);
            addMonsterTokens(game.getBag(), new FlyingMonster("Gargoyle", 2), 2);
            addMonsterTokens(game.getBag(), new ClimbingMonster("Climbing Troll", 3), 2);
            addMonsterTokens(game.getBag(), new CavalryMonster("Goblin Cavalry", 2), 2);
            addMonsterTokens(game.getBag(), new Phoenix(1), 2);
            addMonsterTokens(game.getBag(), new Conjurer(2), 1);
            addMonsterTokens(game.getBag(), new Doppelganger(), 1);
            addMonsterTokens(game.getBag(), new OneImpPerTowerEffect(), 1);
            addMonsterTokens(game.getBag(), new TrebuchetEffect(), 1);
            addMonsterTokens(game.getBag(), new FlamingBoulderEffect(), 1);

            // mega bosses
            megaBossMonsters.add(new Basilisk(5));
            megaBossMonsters.add(new Chimera(5));
            megaBossMonsters.add(new Dragon(5));
            megaBossMonsters.add(new Hydra(4));
            megaBossMonsters.add(new Necromancer(game.getBag(), 4));
            megaBossMonsters.add(new Warlock(4));
        }

        if(darkTitan) {
            // new tokens
            addMonsterTokens(game.getBag(), new HeraldEffect(), 5);
            for (int i = 0; i < arcColors.length; i++) {
                addMonsterTokens(game.getBag(), new ColorPlagueEffect(arcColorNames[i], arcColors[i]), 1);
            }
            addMonsterTokens(game.getBag(), new EliteMonster("Elite Goblin", 1), 2);
            addMonsterTokens(game.getBag(), new EliteMonster("Elite Orc", 2), 2);
            addMonsterTokens(game.getBag(), new EliteMonster("Elite Troll", 3), 2);
            addMonsterTokens(game.getBag(), new BoomMonster("Boom Troll", 3), 1);
            addMonsterTokens(game.getBag(), new DarkSorceress(4), 1);
            addMonsterTokens(game.getBag(), new WitherEffect(), 1);

            // support tokens
            addMonsterTokens(game.getBag(), new SupplyWagon(2), 1);
            addMonsterTokens(game.getBag(), new ReserveSquad(3), 1);
            addMonsterTokens(game.getBag(), new StonemasonsCart(2), 1);

            // agranok
            agranoks.add(new AgranokLevel1(game, 8));
            agranoks.add(new AgranokLevel2(game, 8));
            agranoks.add(new AgranokLevel3(game, 8));
            agranoks.add(new AgranokLevel4(game, 8));
            agranoks.add(new AgranokLevel5(game, 8));
        }
    }

    private void addMonsterTokens(Bag bag, Token token, int count) {
        for (int i = 0; i < count; i++) {
            bag.addToken(token.clone());
        }
    }
}
