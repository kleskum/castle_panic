package castlepanic;

import castlepanic.base.board.Ring;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;
import castlepanic.component.Player;
import castlepanic.component.Token;
import castlepanic.darktitan.tokens.SupportToken;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class GameForm extends JFrame implements MouseListener, MouseMotionListener, Runnable {
    private boolean isRunning = true;
    private int fps = 30;
    private int windowWidth = 1200;
    private int windowHeight = 800;
    private Insets insets;

    public Game Game = null;
    private FormInput input = null;

    private Card selectedCard = null;
    private Token selectedToken = null;
    private Ring selectedRing = null;
    private boolean selectNothing = false;

    private PanelRenderer panelRenderer1;
    private JEditorPane editorPane;
    private JScrollPane editorScrollPane;

    private JToolTip tip = new JToolTip();
    private Popup popup = null;

    private boolean startGame = false;

    public GameForm() {
        input = new FormInput(this);
        Game = new Game(GetNumberPlayers(), input);

        panelRenderer1 = new PanelRenderer(this);
        this.setLayout(new BorderLayout());
        this.add(panelRenderer1, BorderLayout.CENTER);
        editorPane = new JEditorPane();
        editorPane.setEditable(false);
        editorScrollPane = new JScrollPane(editorPane);
        editorScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        editorScrollPane.setPreferredSize(new Dimension(250, 145));
        editorScrollPane.setMinimumSize(new Dimension(10, 10));
        this.add(editorScrollPane, BorderLayout.EAST);
        this.pack();

        Thread thread = new Thread(this);
        thread.start();
    }

    public void start() {
        setTitle("Castle Panic");
        setSize(windowWidth, windowHeight);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);

        insets = getInsets();
        setSize(insets.left + windowWidth + insets.right, insets.top + windowHeight + insets.bottom);

        this.addMouseListener(this);
        this.addMouseMotionListener(this);

        while(isRunning) {
            long time = System.currentTimeMillis();

            //  delay for each frame  -   time it took for one frame
            time = (1000 / fps) - (System.currentTimeMillis() - time);

            if (time > 0) {
                try {
                    Thread.sleep(time);
                } catch(Exception e) {

                }
            }
        }
        setVisible(false);
    }

    private int GetNumberPlayers() {
        int numPlayers = 1;
        return numPlayers;
    }

    public void addMessage(String msg) {
        System.out.println(msg);
        try {
            Document doc = editorPane.getDocument();
            doc.insertString(doc.getLength(), msg + "\n", null);
            JScrollBar vertical = editorScrollPane.getVerticalScrollBar();
            vertical.setValue( vertical.getMaximum() );
        } catch(BadLocationException exc) {
            exc.printStackTrace();
        }
    }

    public void run() {
        while (true) {
            Game.playGame();
            if (!startGame) {
                synchronized (this) {
                    try {
                        this.wait();
                    } catch (Exception ex) {
                        throw new RuntimeException(ex);
                    }
                }
            }
            startGame = false;
        }
    }

    public void clearCardSelection() {
        selectedCard = null;
    }
    public void clearTokenSelection() {
        selectedToken = null;
    }
    public void clearRingSelection() {
        selectedRing = null;
    }
    public Card getCardSelection() {
        return selectedCard;
    }

    public Player getCardOwner(Card ownedCard) {
        if (ownedCard != null) {
            for (Player player : Game.getPlayers()) {
                for (Card card : player.getHand()) {
                    if (card == selectedCard) {
                        return player;
                    }
                }
            }
        }
        return null;
    }

    public Token getTokenSelection() {
        return selectedToken;
    }

    public Ring getRingSelection() {
        return selectedRing;
    }

    public void clearNoSelection() {
        selectNothing = false;
    }
    public boolean getNoSelection() {
        return selectNothing;
    }

    public void mouseClicked(MouseEvent e) {
        synchronized (input) {
            selectedCard = panelRenderer1.getCardAt(new Point(e.getX(), e.getY()));
            selectedToken = panelRenderer1.getTokenAt(new Point(e.getX(), e.getY()), Token.class);
            selectedRing = panelRenderer1.getRingAt(new Point(e.getX(), e.getY()));
            selectNothing = panelRenderer1.getCancelAt(new Point(e.getX(), e.getY()));
            if (selectedCard != null || selectedToken != null || selectedRing != null || selectNothing) {
                input.notifyAll();
            }
        }
    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public void mouseDragged(MouseEvent e) {

    }

    public void mouseMoved(MouseEvent e) {
        Monster monster = panelRenderer1.getTokenAt(new Point(e.getX(), e.getY()), Monster.class);
        SupportToken supportToken = panelRenderer1.getTokenAt(new Point(e.getX(), e.getY()), SupportToken.class);
        Card card = panelRenderer1.getCardAt(new Point(e.getX(), e.getY()));
        if (monster != null) {
            if(!monster.getDescription().equals(tip.getTipText())) {
                if (popup != null) {
                    tip.setTipText("");
                    popup.hide();
                }
                tip.setTipText(monster.getDescription());
                PopupFactory popupFactory = PopupFactory.getSharedInstance();
                popup = popupFactory.getPopup(this, tip, this.getX() + e.getX(), this.getY() + e.getY() - 40);
            }
            popup.show();
        } else if (supportToken != null) {
            if(!supportToken.getName().equals(tip.getTipText())) {
                if (popup != null) {
                    tip.setTipText("");
                    popup.hide();
                }
                tip.setTipText(supportToken.getName());
                PopupFactory popupFactory = PopupFactory.getSharedInstance();
                popup = popupFactory.getPopup(this, tip, this.getX() + e.getX(), this.getY() + e.getY() - 40);
            }
            popup.show();
        }else if (card != null) {
            if(!card.getDescription().equals(tip.getTipText())){
                if (popup != null) {
                    tip.setTipText("");
                    popup.hide();
                }
                tip.setTipText(card.getDescription());
                PopupFactory popupFactory = PopupFactory.getSharedInstance();
                popup = popupFactory.getPopup(this, tip, this.getX() + e.getX(), this.getY() + e.getY() - 20);
            }
            popup.show();
        } else if (popup != null) {
            tip.setTipText("");
            popup.hide();
        }
    }
}
