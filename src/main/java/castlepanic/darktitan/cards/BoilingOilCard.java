package castlepanic.darktitan.cards;

import castlepanic.Game;
import castlepanic.base.board.Arc;
import castlepanic.base.board.Ring;
import castlepanic.base.cards.AttackCard;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Card;

import java.awt.*;

public class BoilingOilCard extends AttackCard {
    public BoilingOilCard(String type, String colorName, Color color) {
        super(type, colorName, color);
        name = colorName + " Boiling Oil";
        description = "Damage all Monsters in the " + colorName + " " + type + " ring for 1 point.";
    }

    @Override
    public boolean play(Game game, Monster monster) {
        if (monster.canHit(this)) {
            int index = monster.getRing().getIndex();
            Ring monsterRing = monster.getRing();
            Color ringColor = monsterRing.getArc().getColor();
            for(Arc arc : monsterRing.getArc().getBoard().getArcs()) {
                Ring typeRing = arc.getRings().get(index);
                if(ringColor.equals(typeRing.getArc().getColor())) {
                    for(Monster m : typeRing.collectTokens(Monster.class)) {
                        m.damage(game.getInput(), 1);
                    }
                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Card clone() {
        return new BoilingOilCard(types.get(0), colorNames.get(0), colors.get(0));
    }
}
