package castlepanic.darktitan.cards;

import castlepanic.Game;
import castlepanic.base.cards.SpecialCard;
import castlepanic.component.Card;

public class BarrageCard extends SpecialCard {
    public BarrageCard() {
        super("Barrage");
        description = "Choose 1 ring or 1 color.  All players may immediately play all Hit cards of that type.";
    }

    @Override
    public boolean play(Game game) {
        return false;
    }

    @Override
    public Card clone() {
        return new BarrageCard();
    }
}
