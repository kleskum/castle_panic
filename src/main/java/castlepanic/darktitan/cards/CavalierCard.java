package castlepanic.darktitan.cards;

import castlepanic.Game;
import castlepanic.base.board.Ring;
import castlepanic.base.cards.SpecialCard;
import castlepanic.component.Card;
import castlepanic.darktitan.tokens.Cavalier;

public class CavalierCard extends SpecialCard {
    public CavalierCard() {
        super("Cavalier");
        description = "Place the " + name + " token in any arc of the a Swordman ring.";
    }

    public boolean play(Game game) {
        if(game.getBoard().collectTokens(Cavalier.class).size() > 0) {
            // already have a Cavalier
            return false;
        }

        // place the cavalier in the swordsman ring
        game.getInput().addMessage("Choose a Swordsmen ring.");
        Ring ring = game.getInput().chooseRing();
        if(ring != null && ring.getName().equals("Swordsmen")) {
            Cavalier cavalier = new Cavalier();
            cavalier.place(ring);
            return true;
        } else {
            return false;
        }
    }

    public Card clone() {
        return new CavalierCard();
    }
}
