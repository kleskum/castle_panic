package castlepanic.darktitan.tokens;

import castlepanic.Game;
import castlepanic.Input;
import castlepanic.base.tokens.DiscardEffect;
import castlepanic.component.Player;
import castlepanic.component.Token;

import java.util.List;

public class DarkSorceress extends EffectMonster {
    private List<Player> effectedPlayers = null;

    public DarkSorceress(int health) {
        super("Dark Sorceress", health);
    }

    @Override
    public void doAction(Game game) {
        // all players discard 1
        DiscardEffect discardEffect = new DiscardEffect(1);
        discardEffect.doAction(game);

        // reduce hand size
        effectedPlayers = game.getPlayers();
        for(Player player : effectedPlayers) {
            player.setMaxDraw(player.getMaxDraw()-1);
        }
    }

    @Override
    public int damage(Input input, int damage) {
        int result = super.damage(input, damage);
        if(health <= 0) {
            // return hand size back to normal
            for(Player player : effectedPlayers) {
                player.setMaxDraw(player.getMaxDraw()+1);
            }
        }
        return result;
    }

    @Override
    public Token clone() {
        return new DarkSorceress(maxHealth);
    }
}
