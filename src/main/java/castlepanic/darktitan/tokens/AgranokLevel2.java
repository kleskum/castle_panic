package castlepanic.darktitan.tokens;

import castlepanic.Game;
import castlepanic.Input;
import castlepanic.component.Token;

public class AgranokLevel2 extends Agranok {
    public AgranokLevel2(Game game, int health) {
        super(game, 2, health);
        description = "<html><b>" + name + "</b><br>All monsters in same number move 1 space.</html>";
    }

    @Override
    public void doMoveEffect(Input input) {
        if(getRing() != null && getRing() != getRing().getArc().getInnerMostRing()) {
            // all monsters in the same arc move
            int num = game.getDie().roll(input);
            input.addMessage(name + " moves all Monsters in " + num);
            NumberMoveEffect effect = new NumberMoveEffect(num);
            effect.doAction(game);
        }
    }

    @Override
    public Token clone() {
        return new AgranokLevel2(game, maxHealth);
    }
}
