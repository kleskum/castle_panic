package castlepanic.darktitan.tokens;

import castlepanic.Game;
import castlepanic.Input;
import castlepanic.base.tokens.AllMoveEffect;
import castlepanic.base.tokens.BoulderEffect;
import castlepanic.component.Token;

public class AgranokLevel5 extends Agranok {
    public AgranokLevel5(Game game, int health) {
        super(game, 5, health);
        description = "<html><b>" + name + "</b><br>" +
                "<b>1-2</b> Heal 1 point of damage.<br>" +
                "<b>3-4</b> All Monsters move 1 space.<br>" +
                "<b>5-6</b> Throw a boulder from current space.</html>";
    }

    @Override
    public void doMoveEffect(Input input) {
        if(getRing() != null && getRing() != getRing().getArc().getInnerMostRing()) {
            switch (game.getDie().roll(input)) {
                case 1:
                case 2:
                    // regain 1 point of health
                    input.addMessage(name + " heals 1 point.");
                    heal();
                    break;
                case 3:
                case 4:
                    // all monsters move 1 space
                    input.addMessage(name + " moves all Monsters.");
                    AllMoveEffect allMoveEffect = new AllMoveEffect();
                    allMoveEffect.doAction(game);
                    break;
                default:
                    // throw giant boulder
                    input.addMessage(name + " throws a boulder");
                    BoulderEffect boulderEffect = new BoulderEffect();
                    boulderEffect.startFromRing(input, getRing());
                    break;
            }
        }
    }

    @Override
    public Token clone() {
        return new AgranokLevel5(game, maxHealth);
    }
}
