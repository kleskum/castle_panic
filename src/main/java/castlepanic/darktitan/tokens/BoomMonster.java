package castlepanic.darktitan.tokens;

import castlepanic.Input;
import castlepanic.base.board.Ring;
import castlepanic.base.tokens.GroundMonster;
import castlepanic.base.tokens.Monster;
import castlepanic.base.tokens.Structure;
import castlepanic.base.tokens.Tower;
import castlepanic.base.tokens.Wall;
import castlepanic.component.Token;

public class BoomMonster extends GroundMonster {
    public BoomMonster(String name, int heatlh) {
        super(name, heatlh);
        description = "When the " + name + " is slain, it causes 1 point of damage to all Monsters in the same space.";
    }

    @Override
    protected Ring moveIntoWall(Input input, Ring nextRing, Wall wall) {
        slay(input);
        for(Structure structure : wall.getRing().getArc().collectTokens(Structure.class)) {
            structure.destroy(input);
        }
        return getRing();
    }

    @Override
    protected void moveIntoTower(Input input, Ring nextRing, Tower tower) {
        slay(input);
        for(Structure structure : tower.getRing().getArc().collectTokens(Structure.class)) {
            structure.destroy(input);
        }
    }

    @Override
    public int damage(Input input, int damage) {
        Ring r = ring;
        int result = super.damage(input, damage);
        if(health <= 0) {
            for(Monster monster : r.collectTokens(Monster.class)) {
                monster.damage(input, 1);
            }
        }
        return result;
    }

    @Override
    public Token clone() {
        return new BoomMonster(name, maxHealth);
    }
}
