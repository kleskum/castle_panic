package castlepanic.darktitan.tokens;

import castlepanic.Game;
import castlepanic.Input;
import castlepanic.base.tokens.AllMoveEffect;
import castlepanic.component.Token;

public class AgranokLevel4 extends Agranok {
    public AgranokLevel4(Game game, int health) {
        super(game, 4, health);
        description = "<html><b>" + name + "</b><br>" +
                "<b>1-2</b> Banish the top card from the Castle deck.<br>" +
                "<b>3-4</b> Return 1 Herald in the discard pile to the Forest.<br>" +
                "<b>5-6</b> All Monsters move 1 space.</html>";
    }

    @Override
    public void doMoveEffect(Input input) {
        if(getRing() != null && getRing() != getRing().getArc().getInnerMostRing()) {
            switch (game.getDie().roll(input)) {
                case 1:
                case 2:
                    // banish the top card from the deck
                    input.addMessage(name + " banishes the top card in the deck.");
                    WitherEffect witherEffect = new WitherEffect();
                    witherEffect.doAction(game);
                    break;
                case 3:
                case 4:
                    // return 1 herald from the discard pile to the forest in the same arc as Agranok
                    input.addMessage(name + " sends 1 Herald from the discard to the forest.");
                    ReturnMonsterEffect returnMonsterEffect = new ReturnMonsterEffect("Herald", getRing().getArc().getIndex()+1);
                    returnMonsterEffect.doAction(game);
                    break;
                default:
                    // all monsters move 1 space
                    input.addMessage(name + " moves all Monsters.");
                    AllMoveEffect allMoveEffect = new AllMoveEffect();
                    allMoveEffect.doAction(game);
                    break;
            }
        }
    }

    @Override
    public Token clone() {
        return new AgranokLevel4(game, maxHealth);
    }
}
