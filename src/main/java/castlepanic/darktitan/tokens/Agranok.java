package castlepanic.darktitan.tokens;

import castlepanic.Game;
import castlepanic.Input;
import castlepanic.base.board.Ring;
import castlepanic.base.tokens.DiscardEffect;
import castlepanic.base.tokens.GroundMonster;
import castlepanic.base.tokens.Monster;
import castlepanic.base.tokens.Tower;
import castlepanic.base.tokens.Wall;
import castlepanic.wizardstower.tokens.MegaMonsterBoss;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public abstract class Agranok extends Monster implements MegaMonsterBoss {
    protected Game game;
    protected int level;
    protected List<GroundMonster> heralds = new ArrayList<GroundMonster>();
    private boolean doMoveAction = false;

    public Agranok(Game game, int level, int health) {
        super("Agranok Level " + level, health, Color.darkGray);
        this.level = level;
        this.game = game;
    }

    public void setDoMoveAction(boolean doMoveAction) {
        this.doMoveAction = doMoveAction;
    }

    public List<GroundMonster> getHeralds() {
        return heralds;
    }

    protected abstract void doMoveEffect(Input input);

    @Override
    public void doAction(Game game) {

    }

    @Override
    public boolean move(Input input) {
        if(super.move(input)) {
            if(doMoveAction) { // watch out for recursive moves
                doMoveAction = false;
                doMoveEffect(input);
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected Ring moveIntoWall(Input input, Ring nextRing, Wall wall) {
        // take no damage
        wall.destroy(input);
        return nextRing;
    }

    @Override
    protected void moveIntoTower(Input input, Ring nextRing, Tower tower) {
        // take no damage
        tower.destroy(input);
    }

    @Override
    public int damage(Input input, int damage) {
        int prevHealth = health;
        int result = super.damage(input, damage);
        if(health > 0 && prevHealth > 4 && health < 4) {
            // flipped over, all players discard
            DiscardEffect discardEffect = new DiscardEffect(1);
            discardEffect.doAction(game);
        }
        return result;
    }

    @Override
    public int slay(Input input) {
        // can't slay, only hit 4 and card is banished
        if(game.getLastSelectedCard() != null) {
            game.getLastSelectedCard().banish(input);
        }
        int result = damage(input, 4);
        input.redraw();
        return result;
    }
}
