package castlepanic.darktitan.tokens;

import castlepanic.Game;
import castlepanic.Input;
import castlepanic.base.board.Board;
import castlepanic.base.board.Ring;
import castlepanic.base.tokens.Effect;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Token;

import java.util.HashSet;
import java.util.Set;

public class NumberMoveEffect extends Effect {
    private int arcNumber;

    public NumberMoveEffect(int arcNumber) {
        super("Monsters in arc " + arcNumber + " Move 1");
        this.arcNumber = arcNumber;
        description = "All Monsters in the " + arcNumber + " arc move 1 ring closer to the castle.";
    }

    @Override
    public void doAction(Game game) {
        doAction(game.getInput(), game.getBoard());
    }

    private void doAction(Input input, Board board) {
        Set<Ring> stalledRings = new HashSet<Ring>();
        for (Monster monster : board.getArcs().get(arcNumber-1).collectTokens(Monster.class)) {
            if (!stalledRings.contains(monster.getRing())) {
                if (!monster.move(input) && monster.getRing() != null) {
                    stalledRings.add(monster.getRing());
                }
            }
        }
    }

    @Override
    public Token clone() {
        return new NumberMoveEffect(arcNumber);
    }
}
