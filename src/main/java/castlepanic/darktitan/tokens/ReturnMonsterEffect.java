package castlepanic.darktitan.tokens;

import castlepanic.Game;
import castlepanic.base.board.Board;
import castlepanic.base.tokens.Effect;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Bag;
import castlepanic.component.Token;

public class ReturnMonsterEffect extends Effect {
    private String monsterName;
    private int arcNumber;

    public ReturnMonsterEffect(String monsterName, int arcNumber) {
        super("Return Monster");
        this.monsterName = monsterName;
        this.arcNumber = arcNumber;
    }

    @Override
    public void doAction(Game game) {
        doAction(game.getBoard(), game.getBag());
    }

    private void doAction(Board board, Bag bag) {
        Monster monster = bag.findDiscardedMonsterToken(monsterName);
        if(monster != null) {
            monster.place(board.getArcs().get(arcNumber-1).getOuterMostRing());
        }
    }

    @Override
    public Token clone() {
        return new ReturnMonsterEffect(monsterName, arcNumber);
    }
}
