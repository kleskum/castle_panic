package castlepanic.darktitan.tokens;

import castlepanic.Game;
import castlepanic.component.Player;
import castlepanic.component.Token;

public class SupplyWagon extends SupportToken {
    public SupplyWagon(int health) {
        super("Supply Wagon", health);
    }

    @Override
    protected void benefit(Game game) {
        for(Player player : game.getPlayers()) {
            for(int i = 0; i < getHealth(); i++) {
                player.draw();
            }
        }
        game.getInput().redraw();
    }

    @Override
    public Token clone() {
        return new SupplyWagon(maxHealth);
    }
}
