package castlepanic.darktitan.tokens;

import castlepanic.Game;
import castlepanic.Input;
import castlepanic.component.Token;

public class AgranokLevel1 extends Agranok {
    public AgranokLevel1(Game game, int health) {
        super(game, 1, health);
        description = "<html><b>" + name + "</b><br>No special ability.</html>";
    }

    @Override
    public void doMoveEffect(Input input) {
        // No special ability at this level
    }

    @Override
    public Token clone() {
        return new AgranokLevel1(game, maxHealth);
    }
}
