package castlepanic.darktitan.tokens;

import castlepanic.Game;
import castlepanic.PanelRenderer;
import castlepanic.base.board.Ring;
import castlepanic.component.Token;

import java.awt.*;

public abstract class Hero extends Token {
    public Hero(String name) {
        super(name);
    }

    public void move(Game game) {
        Ring r = null;
        game.getInput().addMessage("Choose a ring to move to.");
        while(r == null || !adjacent(ring, r)) {
            r = game.getInput().chooseRing();
        }
        this.place(r);
        game.getInput().redraw();
    }

    private boolean adjacent(Ring r1, Ring r2) {
        if(r1 == null || r2 == null) {
            return false;
        }

        if(r1 == r1.getArc().getOuterMostRing()) {
            // in the forest
            return r2 == r1.nextArcRing() || r2 == r1.nextRing() || r2 == r1.previousArcRing();
        } else if(r1 == r1.getArc().getInnerMostRing()) {
            // in the castle
            return r2 == r1.nextArcRing() || r2 == r1.previousRing() || r2 == r1.previousArcRing();
        } else {
            // anywhere else
            return r2 == r1.nextArcRing() || r2 == r1.previousArcRing() || r2 == r1.nextRing() || r2 == r1.previousRing();
        }
    }

    protected abstract String getIndicator();

    @Override
    public void draw(Graphics g, Rectangle board, int iring, int iarc, int itoken) {
        Rectangle bounds = getBounds(board, iring, iarc, itoken);
        g.setColor(Color.yellow);
        g.fillOval(bounds.x, bounds.y, bounds.width, bounds.height);
        g.setColor(Color.black);
        g.drawOval(bounds.x, bounds.y, bounds.width, bounds.height);

        g.setColor(Color.black);
        PanelRenderer.drawCenteredString(g, getIndicator(), bounds, new Font("Arial", Font.PLAIN, 18));
    }
}
