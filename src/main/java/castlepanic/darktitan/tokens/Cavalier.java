package castlepanic.darktitan.tokens;

import castlepanic.Game;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Token;

public class Cavalier extends Hero {
    public Cavalier() {
        super("Cavalier");
    }

    public void doAction(Game game) {
        boolean moved = false;
        if(game.getInput().askYesNoQuestion("Do you want to move the " + name + " first?")) {
            move(game);
            moved = true;
        }
        Monster monster = null;

        game.getInput().resetNoSelect();
        if(ring.collectTokens(Monster.class).size() > 0) {
            game.getInput().addMessage("Choose a monster to fight or Cancel");
            while (monster == null && !game.getInput().getNoSelection()) {
                monster = game.getInput().chooseToken(Monster.class);
            }
        }
        game.getInput().resetNoSelect();

        // fight monster
        if(monster != null) {
            int damage = monster.getHealth();
            monster.damage(game.getInput(), 2); // damage monster for 2
            if(damage >= 2 || (monster instanceof BoomMonster && monster.getHealth() <= 0)) {
                // cavalier killed
                game.getInput().addMessage(name + " was slayed.");
                ring.removeToken(this);
                this.ring = null;
            }

            game.getInput().redraw();

            // still can move
            if(!moved && ring != null && game.getInput().askYesNoQuestion("Do you want to move the " + name + " now?")) {
                move(game);
            }
        }
    }

    protected String getIndicator() {
        return "+";
    }

    public Token clone() {
        return new Cavalier();
    }
}
