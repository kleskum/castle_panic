package castlepanic.darktitan.tokens;

import castlepanic.Game;
import castlepanic.Input;
import castlepanic.base.tokens.Monster;

public abstract class SupportToken extends Hero {
    protected int maxHealth;
    private int health;

    public SupportToken(String name, int health) {
        super(name);
        this.maxHealth = health;
        this.health = health;
    }

    public int getHealth() {
        return health;
    }

    public void damage(Input input, int dmg) {
        health -= dmg;
        if(health <= 0) {
            input.addMessage(name + " destroyed.");
            ring.removeToken(this);
            this.ring = null;
        }
    }

    @Override
    public void doAction(Game game) {
        // nothing special at this point
    }

    @Override
    public void move(Game game) {
        super.move(game);
        if(ring == ring.getArc().getInnerMostRing()) {
            ring.removeToken(this);
            this.ring = null;
            benefit(game);
        } else {
            battle(game.getInput());
        }
    }

    protected abstract void benefit(Game game);

    public void battle(Input input) {
        if (ring.collectTokens(Monster.class).size() > 0) {
            // fight a monster
            Monster monster2 = null;
            input.addMessage("Choose the monster for the " + this.getName() + " to attack.");
            while (monster2 == null) {
                monster2 = input.chooseToken(Monster.class);
            }
            // now have them fight!
            fight(input, monster2);
            input.redraw();
        }
    }

    private void fight(Input input, Monster monster2) {
        this.damage(input, 1);
        monster2.damage(input, 1);
    }

    @Override
    protected String getIndicator() {
        return Integer.toString(health);
    }
}
