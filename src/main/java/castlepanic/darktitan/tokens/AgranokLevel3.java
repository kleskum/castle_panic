package castlepanic.darktitan.tokens;

import castlepanic.Game;
import castlepanic.Input;
import castlepanic.component.Token;

public class AgranokLevel3 extends Agranok {
    public AgranokLevel3(Game game, int health) {
        super(game, 3, health);
        description = "<html><b>" + name + "</b><br>" +
                "<b>1-3</b> Return 1 Herald in the discard pile to the Forest.<br>" +
                "<b>4-6</b> Move 1 additional space.</html>";
    }

    @Override
    public void doMoveEffect(Input input) {
        if(getRing() != null && getRing() != getRing().getArc().getInnerMostRing()) {
            if (game.getDie().roll(input) <= 3) {
                input.addMessage(name + " sends 1 Herald from the discard to the forest.");
                // return 1 herald from the discard pile to the forest in the same arc as Agranok
                ReturnMonsterEffect returnMonsterEffect = new ReturnMonsterEffect("Herald", getRing().getArc().getIndex() + 1);
                returnMonsterEffect.doAction(game);
            } else {
                // move 1 additional space
                input.addMessage(name + " moves 1 additional space.");
                super.move(input);
            }
        }
    }

    @Override
    public Token clone() {
        return new AgranokLevel3(game, maxHealth);
    }
}
