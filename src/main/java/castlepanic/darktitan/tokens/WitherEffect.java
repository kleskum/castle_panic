package castlepanic.darktitan.tokens;

import castlepanic.Game;
import castlepanic.base.tokens.Effect;
import castlepanic.component.Card;
import castlepanic.component.Token;

public class WitherEffect extends Effect {
    public WitherEffect() {
        super("Wither");
        description = "Banish the top card of the Castle deck.";
    }

    public void doAction(Game game) {
        Card card = game.getDeck().popFromDraw();
        card.banish(game.getInput());
    }

    @Override
    public Token clone() {
        return new WitherEffect();
    }
}
