package castlepanic.darktitan.tokens;

import castlepanic.Game;
import castlepanic.base.cards.AttackCard;
import castlepanic.base.cards.SpecialAttackCard;
import castlepanic.base.cards.SpecialCard;
import castlepanic.base.tokens.BoulderEffect;
import castlepanic.base.tokens.Effect;
import castlepanic.base.tokens.GroundMonster;
import castlepanic.component.Card;
import castlepanic.component.Player;
import castlepanic.component.Token;

public class HeraldEffect extends Effect {
    public HeraldEffect() {
        super("Herald");
    }

    private boolean hasHitCard(Player player) {
        for(Card card : player.getHand()) {
            if(card instanceof AttackCard && card.getDescription().toLowerCase().contains("hit")) {
                return true;
            }
        }
        return false;
    }

    private boolean hasSpecialCard(Player player) {
        for(Card card : player.getHand()) {
            if(card instanceof SpecialCard || card instanceof SpecialAttackCard) {
                return true;
            }
        }
        return false;
    }

    private void discardHitCard(Game game) {
        for(Player player : game.getPlayers()) {
            if(hasHitCard(player)) {
                // request card
                Card card = null;
                while (card == null || !(card instanceof AttackCard && card.getDescription().toLowerCase().contains("hit"))) {
                    game.getInput().addMessage("Choose a hit card to discard.");
                    card = game.getInput().requestCard();
                }

                // discard
                player.discard(card);
            }
        }
    }

    private void discardSpecialCard(Game game) {
        for(Player player : game.getPlayers()) {
            if(hasSpecialCard(player)) {
                // request card
                Card card = null;
                while (card == null || (!(card instanceof SpecialCard) && !(card instanceof SpecialAttackCard))) {
                    game.getInput().addMessage("Choose a special card to discard.");
                    card = game.getInput().requestCard();
                }

                // discard
                player.discard(card);
            }
        }
    }

    public void doAction(Game game) {
        GroundMonster herald = new GroundMonster("Herald", 2);
        game.getAgranok().getHeralds().add(herald);
        switch(game.getAgranok().getHeralds().size()) {
            case 1:
                discardHitCard(game);
                break;
            case 2:
                discardSpecialCard(game);
                break;
            case 3:
                // roll for boulder
                BoulderEffect boulderEffect = new BoulderEffect();
                boulderEffect.doAction(game);

                // place agranok in the forest
                game.getAgranok().place(game.getBoard().getArcs().get(game.getDie().roll(game.getInput())-1).getOuterMostRing());

                for(GroundMonster monster : game.getAgranok().getHeralds()) {
                    game.getBag().discardMonster(herald);
                }
                break;
            default:
                // place herald in the forest and perform agranok action
                herald.place(game.getBoard().getArcs().get(game.getDie().roll(game.getInput())-1).getOuterMostRing());
                if(game.getAgranok().getHealth() > 0) {
                    game.getAgranok().doMoveEffect(game.getInput());
                }
                break;
        }
    }

    @Override
    public Token clone() {
        return new HeraldEffect();
    }
}
