package castlepanic.darktitan.tokens;

import castlepanic.Game;
import castlepanic.base.board.Arc;
import castlepanic.base.board.Ring;
import castlepanic.base.tokens.Wall;
import castlepanic.component.Token;

public class StonemasonsCart extends SupportToken {
    public StonemasonsCart(int health) {
        super("Stonemason's Cart", health);
    }

    @Override
    protected void benefit(Game game) {
        // build walls
        for(int i = 0; i < getHealth() && needsWalls(game); i++) {
            game.getInput().addMessage("Choose a ring to build a wall.");
            Ring ring = null;
            while(ring == null || ring != ring.getArc().getInnerMostRing() || ring.findTokenType(Wall.class) != null) {
                ring = game.getInput().chooseRing();
            }
            Wall wall = new Wall();
            wall.place(ring);
            game.getInput().redraw();
        }
    }

    private boolean needsWalls(Game game) {
        for(Arc arc : game.getBoard().getArcs()) {
            if(arc.getInnerMostRing().findTokenType(Wall.class) == null) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Token clone() {
        return new StonemasonsCart(maxHealth);
    }
}
