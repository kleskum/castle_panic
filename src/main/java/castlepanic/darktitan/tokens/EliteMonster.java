package castlepanic.darktitan.tokens;

import castlepanic.Input;
import castlepanic.base.tokens.GroundMonster;
import castlepanic.component.Die;
import castlepanic.component.Token;

public class EliteMonster extends GroundMonster {
    private Die die = new Die(6);

    public EliteMonster(String name, int health) {
        super(name, health);
    }

    @Override
    public int hit(Input input) {
        if(die.roll(input) <= 2) {
            input.addMessage("Missed the " + name + "!");
            return 0;
        } else {
            return super.hit(input);
        }
    }

    @Override
    public Token clone() {
        return new EliteMonster(name, maxHealth);
    }
}
