package castlepanic.darktitan.tokens;

import castlepanic.Game;
import castlepanic.base.tokens.Monster;
import castlepanic.component.Token;

public class ReserveSquad extends SupportToken {
    public ReserveSquad(int health) {
        super("Reserve Squad", health);
    }

    @Override
    protected void benefit(Game game) {
        // damage monsters
        for(int i = 0; i < getHealth() && hasMonsters(game); i++) {
            game.getInput().addMessage("Choose a monster to damage.");
            Monster monster = null;
            while(monster == null) {
                monster = game.getInput().chooseToken(Monster.class);
            }
            monster.damage(game.getInput(), 1);
            game.getInput().redraw();
        }
    }

    private boolean hasMonsters(Game game) {
        return game.getBoard().collectTokens(Monster.class).size() > 0;
    }

    @Override
    public Token clone() {
        return new ReserveSquad(maxHealth);
    }
}
