package castlepanic.darktitan.tokens;

import castlepanic.Game;
import castlepanic.base.cards.AttackCard;
import castlepanic.base.tokens.Effect;
import castlepanic.component.Card;
import castlepanic.component.Player;
import castlepanic.component.Token;

import java.awt.*;

public class ColorPlagueEffect extends Effect {
    private Color myColor;
    private String colorName;

    public ColorPlagueEffect(String colorName, Color color) {
        super("Plague! " + colorName);
        this.myColor = color;
        this.colorName = colorName;
        description = "All Players must discard all " + colorName + " color cards.";
    }

    @Override
    public void doAction(Game game) {
        // loop through players
        for (Player player : game.getPlayers()) {
            // loop through cards
            for (int i = player.getHand().size() - 1; i >= 0; i--) {
                Card card = player.getHand().get(i);

                // check for attack cards
                if (card instanceof AttackCard) {
                    // check for match
                    AttackCard attack = (AttackCard)card;
                    if (attack.getTypes().size() == 1 && attack.containsColor(myColor) && attack.getName().contains(colorName)) {
                        // discard card
                        player.discard(attack);
                    }
                }
            }
        }
    }

    @Override
    public Token clone() {
        return new ColorPlagueEffect(colorName, myColor);
    }
}
