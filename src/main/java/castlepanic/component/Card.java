package castlepanic.component;

import castlepanic.Game;
import castlepanic.Input;

public abstract class Card {
    protected Expansion expansion = Expansion.CastlePanic;
    protected String name = "";
    protected String description;
    protected boolean banished = false;

    protected Card() {

    }

    protected Card(String name) {
        this.name = name;
    }

    public Expansion getExpansion() {
        return expansion;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public abstract boolean play(Game game);

    public abstract Card clone();

    public boolean isBanished() {
        return banished;
    }

    public void banish(Input input) {
        input.addMessage("Card banished!");
        banished = true;
    }
}
