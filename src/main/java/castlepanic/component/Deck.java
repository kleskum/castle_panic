package castlepanic.component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Stack;

public class Deck {
    private Random random = new Random();
    private Stack<Card> myDrawPile = new Stack<Card>();
    private List<Card> discardPile = new ArrayList<Card>();

    public Deck() {
    }

    public List<Card> getDiscardPile() {
        return discardPile;
    }

    public int getDrawCount() {
        return myDrawPile.size();
    }

    public int getDiscardCount() {
        return discardPile.size();
    }

    public int getCombinedCount() {
        return myDrawPile.size() + discardPile.size();
    }

    public void pushOnDraw(Card card) {
        myDrawPile.push(card);
    }

    public void gather() {
        while (discardPile.size() > 0) {
            myDrawPile.push(discardPile.get(0));
            discardPile.remove(0);
        }
    }

    public void shuffle() {
        for (int j = 0; j < 3; j++) {
            List<Card> cards = new ArrayList<Card>(myDrawPile);
            myDrawPile.clear();
            while (cards.size() > 0) {
                int i = random.nextInt(cards.size());
                myDrawPile.push(cards.get(i));
                cards.remove(i);
            }
        }
    }

    public Card popFromDraw() {
        if (myDrawPile.size() == 0) {
            myDrawPile = new Stack<Card>();
            for(Card card : discardPile) {
                myDrawPile.add(card);
            }
            discardPile = new ArrayList<Card>();
            shuffle();
        }
        return myDrawPile.pop();
    }

    public void takeFromDiscard(Card card) {
        discardPile.remove(card);
    }

    public void putInDiscard(Card card) {
        discardPile.add(card);
    }

    public List<Card> removeCards(String name) {
        List<Card> cards = new ArrayList<Card>();
        for(int i = myDrawPile.size()-1; i >= 0; i--) {
            Card card = myDrawPile.get(i);
            if(card.getName().equals(name)) {
                myDrawPile.remove(card);
                cards.add(card);
            }
        }
        return cards;
    }
}
