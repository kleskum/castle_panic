package castlepanic.component;

import castlepanic.Game;
import castlepanic.SlayListener;
import castlepanic.base.tokens.Monster;
import castlepanic.wizardstower.tokens.MegaMonsterBoss;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Bag implements SlayListener {
    private Random random = new Random();
    private List<Token> myMonsterTokens = new ArrayList<Token>();
    private List<Token> usedTokens = new ArrayList<Token>();
    private List<Token> discardedTokens = new ArrayList<Token>();

    public Bag() {
    }

    public Integer getTokenCount() {
        return myMonsterTokens.size();
    }

    public void addToken(Token token) {
        myMonsterTokens.add(token);
    }

    public Token pick() {
        if (myMonsterTokens.size() > 0) {
            int index = random.nextInt(myMonsterTokens.size());
            Token token = myMonsterTokens.get(index);
            myMonsterTokens.remove(index);
            usedTokens.add(token);
            return token;
        } else {
            return null;
        }
    }

    public void gather() {
        while (usedTokens.size() > 0) {
            if (usedTokens.get(0) instanceof Monster) {
                Monster monster = (Monster)usedTokens.get(0);
                monster.reset();
            }
            addToken(usedTokens.get(0));
            usedTokens.remove(0);
        }
    }

    public void mix() {
        for (int j = 0; j < 3; j++) {
            List<Token> tokens = new ArrayList<Token>(myMonsterTokens);
            myMonsterTokens.clear();
            while (tokens.size() > 0) {
                int i = random.nextInt(tokens.size());
                myMonsterTokens.add(tokens.get(i));
                tokens.remove(i);
            }
        }
    }

    public Monster pickRandomDicardedMonster() {
        List<Monster> availableMonsters = new ArrayList<Monster>();
        for(Token token : discardedTokens) {
            if(token instanceof Monster && !(token instanceof MegaMonsterBoss) && ((Monster)token).getHealth() <= 0) {
                availableMonsters.add((Monster)token);
            }
        }
        if(availableMonsters.size() > 0) {
            Monster monster = availableMonsters.get(random.nextInt(availableMonsters.size()));
            discardedTokens.remove(monster);
            return monster;
        } else {
            return null;
        }
    }

    public Monster findDiscardedMonsterToken(String name) {
        Monster find = null;
        for (Token token : discardedTokens) {
            if (token instanceof Monster) {
                Monster monster = (Monster)token;
                if (monster.getName().equals(name)) {
                    find = monster;
                }
            }
        }
        if (find != null) {
            discardedTokens.remove(find);
        }
        return find;
    }

    public Monster findMonsterToken(String name) {
        Monster find = null;
        for (Token token : myMonsterTokens) {
            if (token instanceof Monster) {
                Monster monster = (Monster)token;
                if (monster.getName().equals(name)) {
                    find = monster;
                }
            }
        }
        if (find != null) {
            usedTokens.add(find);
            myMonsterTokens.remove(find);
        }
        return find;
    }

    public void discardMonster(Monster monster) {
        discardedTokens.add(monster);
    }

    public void monsterSlayed(Game game, Monster monster) {
        discardMonster(monster);
    }
}
