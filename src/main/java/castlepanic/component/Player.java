package castlepanic.component;

import java.util.ArrayList;
import java.util.List;

public class Player {
    private Deck myDeck;
    private Deck wizardDeck;
    private int maxDraw;
    private List<Card> hand = new ArrayList<Card>();
    private Integer score = 0;

    public Player(Deck deck, Deck wizardDeck, int max) {
        myDeck = deck;
        this.wizardDeck = wizardDeck;
        maxDraw = max;
    }

    public Integer getScore() {
        return score;
    }

    public List<Card> getHand() {
        return hand;
    }

    public void drawUp() {
        while(hand.size() < maxDraw) {
            draw();
        }
    }

    public int getMaxDraw() {
        return maxDraw;
    }

    public void setMaxDraw(int maxDraw) {
        this.maxDraw = maxDraw;
    }

    public void deal(Card card) {
        hand.add(card);
    }

    public void draw() {
        hand.add(myDeck.popFromDraw());
    }

    public void drawWizard() {
        hand.add(wizardDeck.popFromDraw());
    }

    public void discard(Card card) {
        hand.remove(card);
        if(!card.isBanished()) {
            switch (card.expansion) {
                case CastlePanic:
                    myDeck.putInDiscard(card);
                    break;
                case WizardsTower:
                    wizardDeck.putInDiscard(card);
                    break;
            }
        }
    }

    public void discardAll() {
        while (hand.size() > 0) {
            discard(hand.get(0));
        }
    }

    public void scavenge(Card card) {
        hand.add(card);
        switch (card.expansion) {
            case CastlePanic:
                myDeck.takeFromDiscard(card);
                break;
            case WizardsTower:
                wizardDeck.takeFromDiscard(card);
                break;
        }
    }

    public void resetScore() {
        score = 0;
    }

    public void increaseScore(int points) {
        score += points;
    }
}

