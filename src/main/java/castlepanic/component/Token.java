package castlepanic.component;

import castlepanic.Game;
import castlepanic.PanelRenderer;
import castlepanic.base.board.Ring;

import java.awt.*;

public abstract class Token {
    protected String name;
    protected Ring ring = null;

    public Token(String name) {
        this.name = name;
    }

    public void place(Ring ring) {
        if(this.ring != null) {
            this.ring.removeToken(this);
        }
        this.ring = ring;
        this.ring.addToken(this);
    }

    public String getName() {
        return name;
    }

    public Ring getRing() {
        return ring;
    }

    public abstract void doAction(Game game);

    public abstract Token clone();

    protected Rectangle getBounds(Rectangle board, int iring, int iarc, int itoken) {
        Point center = new Point(board.x + board.width / 2, board.y + board.height / 2);
        int radius = board.width / 2;

        int arcSize = 360 / getRing().getArc().getBoard().getArcs().size();

        int arcDistance = radius / getRing().getArc().getRings().size();
        int arcPosition = (getRing().getArc().getRings().size() - iring - 1) * arcDistance + arcDistance / 2;

        int arcPerToken = arcSize / getRing().getTokens().size();
        int tokenPosition = (iarc * arcSize) + (arcPerToken * itoken) + (arcPerToken / 2);

        Point pt = PanelRenderer.getBoardPoint(center, tokenPosition, arcPosition);
        int monsterSize = arcDistance / 2;

        return new Rectangle(pt.x - monsterSize / 2, pt.y - monsterSize / 2, monsterSize, monsterSize);
    }

    public void draw(Graphics g, Rectangle board, int iring, int iarc, int itoken) {

    }
}

