package castlepanic.component;

import castlepanic.Input;

import java.util.Random;

public class Die {
    private Random random = new Random();
    private int numSides;

    public Die(int sides) {
        numSides = sides;
    }

    public int roll(Input input) {
        int num = random.nextInt(numSides) + 1;
        input.addMessage("Rolled a " + num);
        return num;
    }
}
