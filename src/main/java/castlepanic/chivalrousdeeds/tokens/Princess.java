package castlepanic.chivalrousdeeds.tokens;

import castlepanic.DestroyListener;
import castlepanic.Game;
import castlepanic.base.tokens.Structure;
import castlepanic.base.tokens.Tower;
import castlepanic.component.Card;
import castlepanic.component.Deck;
import castlepanic.component.Token;
import castlepanic.darktitan.tokens.Hero;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Princess extends Hero implements DestroyListener {
    private Random random = new Random();
    private Deck deck = new Deck();
    private List<Card> hand = new ArrayList<Card>();

    public Princess() {
        super("Princess");
    }

    public Deck getDeck() {
        return deck;
    }

    public List<Card> getHand() {
        return hand;
    }

    public void doAction(Game game) {

    }

    public Token clone() {
        return new Princess();
    }

    protected String getIndicator() {
        return "^";
    }

    @Override
    public void move(Game game) {
        List<Tower> towers = game.getBoard().collectTokens(Tower.class);
        if(towers.size() == 0) {
            getRing().removeToken(this);
        } else {
            int num = random.nextInt(towers.size());
            place(towers.get(num).getRing());
        }
        game.getInput().redraw();
    }

    public void structureDestroyed(Game game, Structure structure) {
        deck.getDiscardPile().add(hand.remove(0));
        if(structure.getRing() == this.getRing()) {
            move(game); // need to move
            deck.popFromDraw().banish(game.getInput());
        }
    }
}
