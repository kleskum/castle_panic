﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace CastlePanic
{
    public class Game
    {
        private Board myBoard = null;
        private Die myDie = null;

        private Deck myDeck = new Deck();
        private Bag myBag = new Bag();
        private List<Player> myPlayers = new List<Player>();        
        private Input myInput = null;
        private Renderer myRenderer = null;
        private GameConfig myConfig = new GameConfig();

        private Player currentPlayer = null;
        private List<Card> savedCards = new List<Card>();

        private bool gameOver = true;

        public Game(int numPlayers, Input input, Renderer renderer)
        {   
            // build board
            myBoard = new Board(myConfig);
            myDie = new Die(myConfig.DieSides);

            // save inputs
            myInput = input;
            myRenderer = renderer;

            // add monster tokens
            myConfig.AddMonsterTokens(this);           
 
            // add cards
            myConfig.AddCards(this);


            // set number of players
            ChangePlayers(numPlayers);

            // render
            renderer.Render(this);            
        }

        public Board Board
        {
            get { return myBoard; }
        }

        public Deck Deck
        {
            get { return myDeck; }
        }

        public Bag Bag
        {
            get { return myBag; }
        }

        public Player[] Players
        {
            get { return myPlayers.ToArray(); }
        }

        public Die Die
        {
            get { return myDie; }
        }

        public Input Input
        {
            get { return myInput; }
        }

        public GameConfig Configuration
        {
            get { return myConfig; }
        }

        public Card[] SavedCards
        {
            get { return savedCards.ToArray(); }
        }

        public Player CurrentPlayer
        {
            get { return currentPlayer; }
        }

        public void ChangePlayers(int numPlayers)
        {
            // force game over
            EndGame();

            // clear players
            myPlayers.Clear();

            // add players
            int maxCards = myConfig.GetMaxCardsPerPlayer(numPlayers);
            for (int i = 0; i < numPlayers; i++)
            {
                Player player = new Player(Deck, maxCards);
                myPlayers.Add(player);
            }            
        }

        public void Setup()
        {
            // reset game
            gameOver = false;

            // start with first player
            currentPlayer = myPlayers[0];

            // remove cards from players
            foreach (Player player in Players)
            {
                player.DiscardAll();
                player.Score = 0;
            }

            // remove tokens from board
            Board.Clear();

            // gather up cards and shuffle
            Deck.Gather();
            Deck.Shuffle();

            // gather up tokens and mix
            Bag.Gather();
            Bag.Mix();

            // deal out cards
            for (int i = 0; i < Configuration.GetMaxCardsPerPlayer(Players.Length); i++)
            {
                foreach (Player player in Players)
                {
                    Card card = Deck.PopFromDraw();
                    player.Deal(card);
                }
            }

            // add walls and towers to castle ring
            foreach (Arc arc in Board.Arcs)
            {
                Wall wall = new Wall();
                wall.Place(arc.Rings[arc.Rings.Length - 1]);
                Tower tower = new Tower();
                tower.Place(arc.Rings[arc.Rings.Length - 1]);
            }

            // start monsters in archer ring
            string[] monsters = Configuration.StartMonsters;
            for (int i = 0; i < monsters.Length; i++)
            {
                Bag.FindMonsterToken(monsters[i]).Place(Board.Arcs[i % Board.Arcs.Length].Rings[1]);
            }

            // render
            myRenderer.Render(this);
        }

        public void PlayGame()
        {
            // set up the game
            Setup();

            // play until game over
            while (!GameOver())
            {
                // 1. Draw Up
                CurrentPlayer.DrawUp();
                myRenderer.Render(this);
                if (GameOver()) break;

                // 2. Discard (Optional)
                int numDiscards = Configuration.GetAllowedDiscards(Players.Length);
                for (int i = 0; i < numDiscards; i++)
                {
                    Input.AddMessage("Choose a card to discard (optional).");
                    Card discard = Input.RequestCardToDiscard();
                    if (discard != null)
                    {
                        if (MessageBox.Show("Are you sure you want to discard this card?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            CurrentPlayer.Discard(discard);
                            CurrentPlayer.Draw();
                            myRenderer.Render(this);
                        }
                        else
                        {
                            i--;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                myRenderer.Render(this);
                if (GameOver()) break;

                // 3. Trade
                if (Players.Length > 1)
                {
                    Trade trade = Input.RequestTrade();
                    if (trade != null)
                    {
                        currentPlayer.Discard(trade.MyCard);
                        trade.OtherPlayer.Discard(trade.OtherCard);
                        currentPlayer.Scavenge(trade.OtherCard);
                        trade.OtherPlayer.Scavenge(trade.MyCard);
                    }
                }
                myRenderer.Render(this);
                if (GameOver()) break;

                // 4. Play Cards
                Input.AddMessage("Choose a card to play.");
                Card card = Input.RequestCardToPlay();
                while (card != null)
                {
                    bool success = PlayCard(card);
                    if (!success)
                    {
                        Input.AddMessage("You can't do that.");
                    }
                    myRenderer.Render(this);

                    Input.AddMessage("Choose a card to play.");
                    card = Input.RequestCardToPlay();
                }
                if (GameOver()) break;

                // 5. Move Monsters
                AllMoveEffect move = new AllMoveEffect(1);
                move.DoAction(this);
                myRenderer.Render(this);
                if (GameOver()) break;

                // 6. Draw Monsters
                int numMonsters = Configuration.MonsterDrawCount;
                foreach (Card savedCard in savedCards)
                {
                    if (savedCard is MissingCard)
                    {
                        numMonsters = 0;
                    }
                }
                for (int i = 0; i < numMonsters; i++)
                {
                    DrawMonsterToken();
                    myRenderer.Render(this);
                }
                if (GameOver()) break;

                // End Turn
                EndTurn();
                myRenderer.Render(this);
            }

            // game over
            Input.AddMessage("Game ended.");
            myRenderer.Render(this);
        }

        public void EndGame()
        {
            gameOver = true;
        }

        public bool GameOver()
        {
            if (gameOver)
            {
                return true;
            }

            // check if monsters remain
            int monsterCount = 0;
            foreach (Arc arc in Board.Arcs)
            {
                foreach (Ring ring in arc.Rings)
                {
                    foreach (Token token in ring.Tokens)
                    {
                        if (token is Monster)
                        {
                            monsterCount++;
                        }
                    }
                }
            }
            if (monsterCount == 0 && Bag.TokenCount == 0)
            {
                return true;
            }

            // check if towers remain
            bool haveTowers = false;
            foreach (Arc arc in Board.Arcs)
            {
                Ring ring = arc.Rings[Configuration.RingNames.Length - 1];
                foreach (Token token in ring.Tokens)
                {
                    if (token is Tower)
                    {
                        haveTowers = true;
                    }
                }
            }
            if (!haveTowers)
            {
                return true;
            }

            // game isn't over
            return false;
        }
        
        public void DrawMonsterToken()
        {
            Token token = Bag.Pick();
            if (token == null)
            {
                return;
            }

            if (token is Effect)
            {
                // resolve effect
                Effect effect = token as Effect;
                Input.AddMessage("-- Drew " + effect.Name);
                effect.DoAction(this);
            }
            else if (token is Monster)
            {
                // add monster to forest
                Monster monster = token as Monster;
                Input.AddMessage("Drew " + monster.Name);
                int arcIndex = Die.Roll();
                Input.AddMessage("Rolled a " + arcIndex);
                monster.Place(Board.Arcs[arcIndex - 1].Rings[0]);
                       
                if (token is MonsterBoss)
                {
                    // do boss action
                    MonsterBoss boss = token as MonsterBoss;
                    Input.AddMessage("!!BOSS!!");
                    boss.DoAction(this);
                }
            }            
        }

        public bool PlayCard(Card card)
        {
            bool success = false;            

            if (card is AttackCard)
            {
                // choose monster
                Input.AddMessage("Choose a monster to hit.");
                Monster monster = Input.ChooseMonster();
                if (monster == null)
                {
                    return false;
                }

                AttackCard attack = card as AttackCard;

                int ringIndex = monster.Ring.Index;
                bool castleAttack = false;
                if (card is SpecialAttackCard && ringIndex == monster.Ring.Arc.Rings.Length - 1)
                {
                    // how to use if in castle?
                    castleAttack = Input.ChooseCastleAttack();
                }

                if (card is SpecialAttackCard && !castleAttack)
                {
                    // use special on card
                    SpecialAttackCard special = card as SpecialAttackCard;
                    success = special.PlaySpecial(this, monster);
                }
                else
                {
                    // use normal attack
                    success = attack.Play(this, monster);
                }
            }
            else if (card is MaterialCard)
            {
                // check if a material card already in play
                bool found = false;
                foreach (Card saved in savedCards)
                {
                    if (saved is MaterialCard)
                    {
                        found = true;
                    }
                }

                Ring ring = null;
                if (found || card is FortifyWallCard)
                {
                    // choose ring
                    Input.AddMessage("Choose a castle ring.");
                    ring = Input.ChooseRing();
                }

                MaterialCard material = card as MaterialCard;
                success = material.Play(this, ring);
            }
            else if (card is SpecialCard)
            {
                // play special
                SpecialCard special = card as SpecialCard;
                success = special.Play(this);
            }

            if (success)
            {
                // save the card until turn is over
                currentPlayer.Discard(card);
            }

            return success;
        }

        public void SaveCard(Card card)
        {
            savedCards.Add(card);
        }

        public void ClearCard(Card card)
        {
            savedCards.Remove(card);
        }

        public void EndTurn()
        {
            foreach (Card card in savedCards)
            {
                if (card is MaterialCard)
                {
                    // put the material card back in your hand, it wasn't used
                    currentPlayer.Deal(card);
                    Deck.TakeFromDiscard(card);
                }
                else if (card is NiceShotCard)
                {
                    // put the nice shot card back in your hand, it wasn't used
                    currentPlayer.Deal(card);
                    Deck.TakeFromDiscard(card);
                }
            }

            // clear out the cards
            savedCards.Clear();

            // untar any characters
            foreach (Arc arc in Board.Arcs)
            {
                foreach (Ring ring in arc.Rings)
                {
                    foreach (Token token in ring.Tokens)
                    {
                        if (token is Monster)
                        {
                            ((Monster)token).Tarred = false;
                        }
                    }
                }
            }

            // turn to next player
            int playerIndex = myPlayers.IndexOf(currentPlayer) + 1;
            if (playerIndex >= myPlayers.Count)
            {
                playerIndex = 0;
            }
            currentPlayer = myPlayers[playerIndex];
        }
    }
}
