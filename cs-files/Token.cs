﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public abstract class Token
    {
        public abstract Token Clone();
    }
}
