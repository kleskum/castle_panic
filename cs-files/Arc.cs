﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace CastlePanic
{
    public class Arc
    {
        private Board myBoard;
        private int myNumber;
        private Color myColor;
        private List<Ring> myRings = new List<Ring>();

        public Arc(GameConfig config, Board board, int number, Color color)
        {
            myBoard = board;
            myNumber = number;
            myColor = color;

            // add rings
            foreach (string name in config.RingNames)
            {
                myRings.Add(new Ring(this, name));
            }            
        }        

        public Board Board
        {
            get { return myBoard; }
        }

        public int Index
        {
            get { return new List<Arc>(myBoard.Arcs).IndexOf(this); }
        }

        public int Number
        {
            get { return myNumber; }
        }

        public Color Color
        {
            get { return myColor; }
        }

        public Ring[] Rings
        {
            get { return myRings.ToArray(); }
        }
    }
}
