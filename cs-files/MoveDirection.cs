﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public enum MoveDirection
    {
        Normal,
        Clockwise,
        CounterClockwise
    }
}
