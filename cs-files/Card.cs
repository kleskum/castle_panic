﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public abstract class Card
    {
        protected string myDescription = "";

        public string Description
        {
            get { return myDescription; }
        }

        public abstract Card Clone();
    }
}
