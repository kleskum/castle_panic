﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public interface Input
    {
        void AddMessage(string msg);
        Card RequestCardToDiscard();
        Trade RequestTrade();
        Card RequestCardToPlay();
        Monster ChooseMonster();
        bool ChooseCastleAttack();
        Ring ChooseRing();
        Card ChooseCardFromDiscard();
    }
}
