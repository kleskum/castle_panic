﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace CastlePanic
{
    public class Board
    {
        private List<Arc> myArcs = new List<Arc>();

        public Board(GameConfig config)
        {
            int number = 1;
            foreach (Color color in config.ArcColors)
            {
                for (int i = 0; i < config.ArcsPerColor; i++)
                {
                    myArcs.Add(new Arc(config, this, number, color));
                    number++;
                }
            }
        }

        public Arc[] Arcs
        {
            get { return myArcs.ToArray(); }
        }

        public void Clear()
        {
            foreach (Arc arc in Arcs)
            {
                foreach (Ring ring in arc.Rings)
                {
                    ring.Clear();
                }
            }
        }
    }
}
