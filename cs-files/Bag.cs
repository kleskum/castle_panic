﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class Bag
    {
        private Random random = new Random();
        private List<Token> myMonsterTokens = new List<Token>();
        private List<Token> usedTokens = new List<Token>();

        public Bag()
        {
        }

        public int TokenCount
        {
            get { return myMonsterTokens.Count; }
        }

        public void AddToken(Token token)
        {
            myMonsterTokens.Add(token);
        }

        public Token Pick()
        {
            if (myMonsterTokens.Count > 0)
            {
                int index = random.Next(myMonsterTokens.Count);
                Token token = myMonsterTokens[index];
                myMonsterTokens.RemoveAt(index);
                usedTokens.Add(token);
                return token;
            }
            else
            {
                return null;
            }
        }

        public void Gather()
        {
            while (usedTokens.Count > 0)
            {
                if (usedTokens[0] is Monster)
                {
                    Monster monster = usedTokens[0] as Monster;
                    monster.Reset();
                }
                AddToken(usedTokens[0]);
                usedTokens.RemoveAt(0);
            }
        }

        public void Mix()
        {
            for (int j = 0; j < 3; j++)
            {
                List<Token> tokens = new List<Token>(myMonsterTokens);
                myMonsterTokens.Clear();
                while (tokens.Count > 0)
                {
                    int i = random.Next(tokens.Count);
                    myMonsterTokens.Add(tokens[i]);
                    tokens.RemoveAt(i);
                }
            }
        }

        public Monster FindMonsterToken(string name)
        {
            Monster find = null;
            foreach (Token token in myMonsterTokens)
            {
                if (token is Monster)
                {
                    Monster monster = token as Monster;
                    if (monster.Name == name)
                    {
                        find = monster;
                    }
                }
            }
            if (find != null)
            {
                usedTokens.Add(find);
                myMonsterTokens.Remove(find);
            }
            return find;
        }
    }
}
