﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace CastlePanic
{
    public partial class PanelRenderer : UserControl, Renderer
    {
        private Game myGame;

        public PanelRenderer()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            this.Resize += new EventHandler(PanelRenderer_Resize);
        }

        void PanelRenderer_Resize(object sender, EventArgs e)
        {
            Invalidate();
        }

        private Point GetBoardPoint(Point center, int degrees, int radius)
        {
            Point centerPoint = new Point(0, 0);
            Point result = new Point(0, 0);
            double angle = degrees * Math.PI / 180;
            result.Y = center.Y + (int)Math.Round(radius * Math.Sin(angle));
            result.X = center.X + (int)Math.Round(radius * Math.Cos(angle));
            return result;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            Graphics g = e.Graphics;
            if (myGame != null)
            {
                GameForm gameForm = this.Parent as GameForm;
                Monitor.Enter(gameForm.Lock);

                using (Font left = new Font(FontFamily.GenericSansSerif, 12))
                {
                    g.DrawString(myGame.Bag.TokenCount.ToString(), left, Brushes.Black, 10, 10);
                    g.DrawString(myGame.Deck.DrawCount.ToString(), left, Brushes.Black, 10, 40);
                }

                int maxD = Math.Min(this.Width, this.Height) - 120;

                Rectangle board = new Rectangle(this.Width / 2 - maxD / 2, 10, maxD, maxD);
                Point center = new Point(board.X + board.Width / 2, board.Y + board.Height / 2);

                int radius = maxD / 2;
                int arcSize = 360 / myGame.Board.Arcs.Length;

                // fill board
                g.FillEllipse(Brushes.DarkGreen, board);

                // fill arcs
                int arcAngle = 0;
                foreach (Arc arc in myGame.Board.Arcs)
                {
                    using (Brush brush = new SolidBrush(arc.Color))
                    {
                        int arcDistance = radius / arc.Rings.Length;
                        int arcPosition = arcDistance;
                        int arcWidth = board.Width - arcPosition * 2;
                        Rectangle level = new Rectangle(board.X + arcPosition, board.Y + arcPosition, arcWidth, arcWidth);
                        g.FillPie(brush, level, arcAngle, arcSize);

                        arcPosition += (arc.Rings.Length - 2) * arcDistance;
                        arcWidth = board.Width - arcPosition * 2;
                        level = new Rectangle(board.X + arcPosition, board.Y + arcPosition, arcWidth, arcWidth);
                        g.FillPie(Brushes.LightGray, level, arcAngle, arcSize);
                    }

                    arcAngle += arcSize;
                }

                // draw arcs
                arcAngle = 0;
                foreach (Arc arc in myGame.Board.Arcs)
                {
                    g.DrawPie(Pens.Black, board, arcAngle, arcSize);

                    int arcDistance = radius / arc.Rings.Length;
                    int arcPosition = arcDistance;
                    foreach (Ring ring in arc.Rings)
                    {
                        int arcWidth = board.Width - arcPosition * 2;
                        if (arcWidth > 0)
                        {
                            Rectangle level = new Rectangle(board.X + arcPosition, board.Y + arcPosition, arcWidth, arcWidth);
                            g.DrawArc(Pens.Black, level, arcAngle, arcSize);
                        }
                        arcPosition += arcDistance;
                    }

                    arcAngle += arcSize;
                }

                // draw tokens
                int iarc = 0;
                foreach (Arc arc in myGame.Board.Arcs)
                {
                    int iring = 0;
                    foreach (Ring ring in arc.Rings)
                    {
                        int itoken = 0;
                        int wallCount = 0;
                        foreach (Token token in ring.Tokens)
                        {
                            if (token is Monster)
                            {
                                Monster monster = token as Monster;

                                int arcDistance = radius / arc.Rings.Length;
                                int monsterSize = arcDistance / 2;
                                int arcPosition = (arc.Rings.Length - iring - 1) * arcDistance + arcDistance / 2;

                                int arcPerToken = arcSize / ring.Tokens.Length;
                                int tokenPosition = (iarc * arcSize) + (arcPerToken * itoken) + (arcPerToken / 2);

                                Point pt = GetBoardPoint(center, tokenPosition, arcPosition);

                                Rectangle bounds = new Rectangle(pt.X - monsterSize / 2, pt.Y - monsterSize / 2,
                                    monsterSize, monsterSize);

                                if (monster.Tarred)
                                {
                                    g.FillEllipse(Brushes.Black, bounds.X - 3, bounds.Y - 3,
                                        bounds.Width + 6, bounds.Height + 6);
                                }
                                g.FillEllipse(Brushes.Gray, bounds);
                                g.DrawEllipse(Pens.Black, bounds);

                                if (token is MonsterBoss)
                                {
                                    using (Pen pen = new Pen(Color.Yellow, 2))
                                    {
                                        g.DrawEllipse(pen, bounds);
                                    }
                                }

                                using (Font font = new Font(FontFamily.GenericSansSerif, 7))
                                {
                                    StringFormat format = new StringFormat();
                                    format.Alignment = StringAlignment.Center;
                                    format.LineAlignment = StringAlignment.Center;
                                    g.DrawString(monster.Health.ToString(), font, Brushes.Black, bounds, format);
                                }
                            }
                            else if (token is Wall)
                            {
                                if (wallCount == 0)
                                {
                                    using (Pen pen = new Pen(Color.Gray, 4))
                                    {
                                        int arcDistance = radius / arc.Rings.Length;
                                        int arcPosition = arcDistance * iring;
                                        int arcWidth = board.Width - arcPosition * 2;
                                        arcAngle = iarc * arcSize;
                                        Rectangle level = new Rectangle(board.X + arcPosition, board.Y + arcPosition, arcWidth, arcWidth);
                                        g.DrawArc(pen, level, arcAngle, arcSize);
                                    }
                                }
                                else
                                {
                                    int arcDistance = radius / arc.Rings.Length;
                                    int towerSize = arcDistance / 3;
                                    int arcPosition = (arc.Rings.Length - iring - 1) * arcDistance + arcDistance + 3;

                                    int tokenPosition = (iarc * arcSize) + arcSize / 2;

                                    Point pt = GetBoardPoint(center, tokenPosition, arcPosition);

                                    g.FillRectangle(Brushes.Brown, pt.X - towerSize / 2, pt.Y - towerSize / 2,
                                        towerSize, towerSize);
                                    g.DrawRectangle(Pens.Black, pt.X - towerSize / 2, pt.Y - towerSize / 2,
                                        towerSize, towerSize);
                                }
                                wallCount++;
                            }
                            else if (token is Tower)
                            {
                                int arcDistance = radius / arc.Rings.Length;
                                int towerSize = arcDistance / 3;
                                int arcPosition = (arc.Rings.Length - iring - 1) * arcDistance + arcDistance / 2 + 5;

                                int tokenPosition = (iarc * arcSize) + arcSize / 2;

                                Point pt = GetBoardPoint(center, tokenPosition, arcPosition);

                                g.FillRectangle(Brushes.Gray, pt.X - towerSize / 2, pt.Y - towerSize / 2,
                                    towerSize, towerSize);
                                g.DrawRectangle(Pens.Black, pt.X - towerSize / 2, pt.Y - towerSize / 2,
                                    towerSize, towerSize);
                            }
                            itoken++;
                        }
                        iring++;
                    }
                    iarc++;
                }

                // draw cards
                int x = 20;
                int y = this.Height - 90;
                int cardWidth = 60;
                int cardHeight = 80;
                foreach (Player player in myGame.Players)
                {
                    using (Font score = new Font(FontFamily.GenericSansSerif, 10))
                    {
                        g.DrawString(player.Score.ToString(), score, Brushes.Black, x, y - 15);
                    }

                    for (int i = 0; i < player.Hand.Length; i++)
                    {
                        Card card = player.Hand[i];
                        DrawCard(g, card, x, y, cardWidth, cardHeight);
                        x += cardWidth + 10;
                    }
                }

                g.FillEllipse(Brushes.Red, x, y, cardWidth, cardWidth);
                g.DrawEllipse(Pens.Black, x, y, cardWidth, cardWidth);

                Monitor.Exit(gameForm.Lock);
            }
        }

        public static void DrawCard(Graphics g, Card card,
            int x, int y, int cardWidth, int cardHeight)
        {
            Rectangle bounds = new Rectangle(x, y, cardWidth, cardHeight);
            g.FillRectangle(Brushes.White, bounds);
            g.DrawRectangle(Pens.Black, bounds);

            if (card is SpecialAttackCard)
            {
                SpecialAttackCard special = card as SpecialAttackCard;
                g.FillRectangle(Brushes.Purple, x + 5, y + 5, cardWidth - 10, 10);

                using (Font font = new Font(FontFamily.GenericSansSerif, 7))
                {
                    StringFormat format = new StringFormat();
                    format.Alignment = StringAlignment.Center;
                    format.LineAlignment = StringAlignment.Center;
                    g.DrawString(special.Name, font, Brushes.Black, bounds, format);
                }
            }
            else if (card is AttackCard)
            {
                AttackCard attack = card as AttackCard;
                int icolor = 0;
                foreach (Color color in attack.Colors)
                {
                    int colorWidth = (cardWidth - 10) / attack.Colors.Length;
                    using (Brush brush = new SolidBrush(color))
                    {
                        g.FillRectangle(brush, x + 5 + icolor * colorWidth, y + 5, colorWidth, 10);
                    }
                    icolor++;
                }
                string name = attack.Name;
                using (Font font = new Font(FontFamily.GenericSansSerif, 7))
                {
                    StringFormat format = new StringFormat();
                    format.Alignment = StringAlignment.Center;
                    format.LineAlignment = StringAlignment.Center;
                    g.DrawString(name, font, Brushes.Black, bounds, format);
                }
            }
            else if (card is MaterialCard)
            {
                MaterialCard material = card as MaterialCard;
                g.FillRectangle(Brushes.Gray, x + 5, y + 5, cardWidth - 10, 10);

                using (Font font = new Font(FontFamily.GenericSansSerif, 7))
                {
                    StringFormat format = new StringFormat();
                    format.Alignment = StringAlignment.Center;
                    format.LineAlignment = StringAlignment.Center;
                    g.DrawString(material.Name, font, Brushes.Black, bounds, format);
                }
            }
            else if (card is SpecialCard)
            {
                SpecialCard special = card as SpecialCard;

                using (Font font = new Font(FontFamily.GenericSansSerif, 7))
                {
                    StringFormat format = new StringFormat();
                    format.Alignment = StringAlignment.Center;
                    format.LineAlignment = StringAlignment.Center;
                    g.DrawString(special.Name, font, Brushes.Black, bounds, format);
                }
            }
        }

        public Card GetCardAt(Point pt)
        {
            int x = 20;
            int y = this.Height - 90;
            int cardWidth = 60;
            int cardHeight = 80;
            foreach (Player player in myGame.Players)
            {
                foreach (Card card in player.Hand)
                {
                    Rectangle bounds = new Rectangle(x, y, cardWidth, cardHeight);
                    if (player == myGame.CurrentPlayer && bounds.Contains(pt))
                    {
                        return card;
                    }
                    x += cardWidth + 10;
                }
            }
            return null;
        }

        public Monster GetMonsterAt(Point point)
        {
            int maxD = Math.Min(this.Width, this.Height) - 120;
            int radius = maxD / 2;
            int arcSize = 360 / myGame.Board.Arcs.Length;
            Rectangle board = new Rectangle(this.Width / 2 - maxD / 2, 10, maxD, maxD);
            Point center = new Point(board.X + board.Width / 2, board.Y + board.Height / 2);

            int iarc = 0;
            foreach (Arc arc in myGame.Board.Arcs)
            {
                int iring = 0;
                foreach (Ring ring in arc.Rings)
                {
                    int itoken = 0;
                    foreach (Token token in ring.Tokens)
                    {
                        if (token is Monster)
                        {
                            Monster monster = token as Monster;

                            int arcDistance = radius / arc.Rings.Length;
                            int monsterSize = arcDistance / 2;
                            int arcPosition = (arc.Rings.Length - iring - 1) * arcDistance + arcDistance / 2;

                            int arcPerToken = arcSize / ring.Tokens.Length;
                            int tokenPosition = (iarc * arcSize) + (arcPerToken * itoken) + (arcPerToken / 2);

                            Point pt = GetBoardPoint(center, tokenPosition, arcPosition);

                            Rectangle bounds = new Rectangle(pt.X - monsterSize / 2, pt.Y - monsterSize / 2,
                                monsterSize, monsterSize);

                            if (bounds.Contains(point))
                            {
                                return monster;
                            }
                        }
                        itoken++;
                    }
                    iring++;
                }
                iarc++;
            }
            return null;
        }

        private bool InPolygonBounds(Point pt, Point[] _pts)
        {
            int minx = int.MaxValue;
            int maxx = int.MinValue;
            int miny = int.MaxValue;
            int maxy = int.MinValue;
            foreach (Point _pt in _pts)
            {
                minx = Math.Min(minx, _pt.X);
                maxx = Math.Max(maxx, _pt.X);
                miny = Math.Min(miny, _pt.Y);
                maxy = Math.Max(maxy, _pt.Y);
            }
            Rectangle bounds = new Rectangle(minx, miny, maxx - miny, maxy - miny);
            return bounds.Contains(pt);
        }

        private bool PolygonContains(Point pt, Point[] _pts)
        {
            int NumberOfPoints = _pts.Length;
            bool isIn = false;
            if (InPolygonBounds(pt, _pts))
            {
                int i = 0, j = 0;
                for (j = NumberOfPoints - 1; i < NumberOfPoints; j = i++)
                {
                    if (
                        (
                         ((_pts[i].Y <= pt.Y) && (pt.Y < _pts[j].Y)) || ((_pts[j].Y <= pt.Y) && (pt.Y < _pts[i].Y))
                        ) &&
                        (pt.X < (_pts[j].X - _pts[i].X) * (pt.Y - _pts[i].Y) / (_pts[j].Y - _pts[i].Y) + _pts[i].X)
                       )
                    {
                        isIn = !isIn;
                    }
                }
            }
            return isIn;
        }

        public Ring GetRingAt(Point point)
        {
            int maxD = Math.Min(this.Width, this.Height) - 120;
            int radius = maxD / 2;
            int arcSize = 360 / myGame.Board.Arcs.Length;
            Rectangle board = new Rectangle(this.Width / 2 - maxD / 2, 10, maxD, maxD);
            Point center = new Point(board.X + board.Width / 2, board.Y + board.Height / 2);
            int arcAngle = 0;
            foreach (Arc arc in myGame.Board.Arcs)
            {
                int arcDistance = radius / arc.Rings.Length;
                Ring ring = arc.Rings[arc.Rings.Length - 1];

                Point[] pts = new Point[] {
                    center, 
                    GetBoardPoint(center, arcAngle, arcDistance),
                    GetBoardPoint(center, arcAngle + arcSize, arcDistance),
                };

                if (PolygonContains(point, pts))
                {
                    return ring;
                }

                arcAngle += arcSize;
            }
            return null;
        }

        public bool GetCancelAt(Point point)
        {
            int x = 20;
            int y = this.Height - 90;
            int cardWidth = 60;
            foreach (Player player in myGame.Players)
            {
                foreach (Card card in player.Hand)
                {
                    x += cardWidth + 10;
                }
            }
            Rectangle bounds = new Rectangle(x, y, cardWidth, cardWidth);
            return bounds.Contains(point);
        }

        public void Render(Game game)
        {
            myGame = game;
            Invalidate();
        }
    }
}
