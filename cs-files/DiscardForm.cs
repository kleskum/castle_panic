﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CastlePanic
{
    public partial class DiscardForm : Form
    {
        private Game myGame;
        private Card myCard = null;

        public DiscardForm(Game game)
        {
            InitializeComponent();
            myGame = game;
            this.DoubleBuffered = true;
        }

        public Card SelectedCard
        {
            get { return myCard; }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;

            if (myGame != null)
            {                
                int x = 20;
                int y = 20;
                int cardWidth = 60;
                int cardHeight = 80;

                int width = 0;
                foreach (Card card in myGame.Deck.DiscardPile)
                {
                    width = Math.Max(x + cardWidth + 30, width);

                    PanelRenderer.DrawCard(g, card, x, y, cardWidth, cardHeight);
                    x += cardWidth + 10;                    
                    if (x > 600)
                    {
                        x = 20;
                        y += cardHeight + 10;
                    }
                }

                int height = y + cardHeight + 20 + 30;

                if (width != this.Width)
                {
                    this.Width = width;
                }
                if (height != this.Height)
                {
                    this.Height = height;
                }
            }            
        }

        private void DiscardForm_MouseClick(object sender, MouseEventArgs e)
        {
            if (myGame != null)
            {
                int x = 20;
                int y = 20;
                int cardWidth = 60;
                int cardHeight = 80;

                foreach (Card card in myGame.Deck.DiscardPile)
                {
                    Rectangle bounds = new Rectangle(x, y, cardWidth, cardHeight);
                    if (bounds.Contains(new Point(e.X, e.Y)))
                    {
                        myCard = card;
                        this.Close();
                        return;
                    }
                    x += cardWidth + 10;
                    if (x > 600)
                    {
                        x = 20;
                        y += cardHeight + 10;
                    }
                }
            }  
        }
    }
}
