﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public interface Renderer
    {
        void Render(Game game);
    }
}
