﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class Player
    {
        private Deck myDeck;
        private int maxDraw;
        private List<Card> myHand = new List<Card>();
        private int myScore = 0;

        public Player(Deck deck, int max)
        {
            myDeck = deck;
            maxDraw = max;
        }

        public Card[] Hand
        {
            get { return myHand.ToArray(); }
        }

        public int Score
        {
            get { return myScore; }
            set { myScore = value; }
        }

        public void DrawUp()
        {
            while(myHand.Count < maxDraw)
            {
                Draw();
            }
        }

        public void Deal(Card card)
        {
            myHand.Add(card);
        }

        public void Draw()
        {
            myHand.Add(myDeck.PopFromDraw());
        }

        public void Discard(Card card)
        {
            myHand.Remove(card);
            myDeck.PutInDiscard(card);
        }

        public void DiscardAll()
        {
            while (myHand.Count > 0)
            {
                Discard(myHand[0]);
            }
        }

        public void Scavenge(Card card)
        {
            myHand.Add(card);
            myDeck.TakeFromDiscard(card);
        }
    }
}
