﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class Deck
    {
        private Random random = new Random();
        private Stack<Card> myDrawPile = new Stack<Card>();
        private List<Card> myDiscardPile = new List<Card>();

        public Deck()
        {
        }

        public int DrawCount
        {
            get { return myDrawPile.Count; }
        }

        public int DiscardCount
        {
            get { return myDiscardPile.Count; }
        }

        public Card[] DiscardPile
        {
            get { return myDiscardPile.ToArray(); }
        }

        public void PushOnDraw(Card card)
        {
            myDrawPile.Push(card);
        }

        public void Gather()
        {
            while (myDiscardPile.Count > 0)
            {
                myDrawPile.Push(myDiscardPile[0]);
                myDiscardPile.RemoveAt(0);
            }
        }

        public void Shuffle()
        {
            for (int j = 0; j < 3; j++)
            {
                List<Card> cards = new List<Card>(myDrawPile);
                myDrawPile.Clear();
                while (cards.Count > 0)
                {
                    int i = random.Next(cards.Count);
                    myDrawPile.Push(cards[i]);
                    cards.RemoveAt(i);
                }
            }
        }

        public Card PopFromDraw()
        {
            if (myDrawPile.Count == 0)
            {
                myDrawPile = new Stack<Card>(myDiscardPile);
                myDiscardPile = new List<Card>();
                Shuffle();
            }
            return myDrawPile.Pop();
        }

        public void TakeFromDiscard(Card card)
        {
            myDiscardPile.Remove(card);
        }

        public void PutInDiscard(Card card)
        {
            myDiscardPile.Add(card);
        }
    }
}
