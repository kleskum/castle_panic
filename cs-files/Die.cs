﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{
    public class Die
    {
        private Random random = new Random();
        private int numSides;

        public Die(int sides)
        {
            numSides = sides;
        }

        public int Roll()
        {
            return random.Next(1, numSides + 1);
        }
    }
}
