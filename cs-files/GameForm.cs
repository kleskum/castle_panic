﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace CastlePanic
{
    public partial class GameForm : Form
    {
        private Game game = null;
        private FormInput input = null;

        private Card selectedCard = null;
        private Monster selectedMonster = null;
        private Ring selectedRing = null;

        private ToolTip tip = new ToolTip();

        private bool startGame = false;
        public string Lock = "lockObj";

        public GameForm()
        {
            InitializeComponent();

            input = new FormInput(this);

            game = new Game(GetNumberPlayers(), input, panelRenderer1);

            Thread thread = new Thread(StartGame);
            thread.Start();
        }

        public Game Game
        {
            get { return game; }
        }

        private int GetNumberPlayers()
        {
            int numPlayers = 1;
            foreach (ToolStripMenuItem item in playersToolStripMenuItem.DropDownItems)
            {
                if (item.Checked)
                {
                    numPlayers = int.Parse(item.Text);
                }
            }
            return numPlayers;
        }

        private delegate void AddMessageDelegate(string msg);
        public void AddMessage(string msg)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new AddMessageDelegate(AddMessage), new object[] { msg });
            }
            else
            {
                listBox1.Items.Add(msg);
                listBox1.SelectedIndex = listBox1.Items.Count - 1;
            }
        }

        private void StartGame()
        {
            while (true)
            {                
                Monitor.Enter(Lock);
                game.PlayGame();
                Monitor.Exit(Lock);

                if (!startGame)
                {
                    lock (this)
                    {
                        Monitor.Wait(this);
                    }
                }
                startGame = false;
            }
        }

        private void panelRenderer1_MouseClick(object sender, MouseEventArgs e)
        {
            lock (input)
            {
                selectedCard = panelRenderer1.GetCardAt(new Point(e.X, e.Y));
                selectedMonster = panelRenderer1.GetMonsterAt(new Point(e.X, e.Y));
                selectedRing = panelRenderer1.GetRingAt(new Point(e.X, e.Y));

                bool selectNothing = panelRenderer1.GetCancelAt(new Point(e.X, e.Y));

                if (selectedCard != null || selectedMonster != null || selectedRing != null || selectNothing)
                {
                    Monitor.Pulse(input);
                }
            }
        }

        public void ClearCardSelection()
        {
            selectedCard = null;
        }
        public void ClearMonsterSelection()
        {
            selectedMonster = null;
        }
        public void ClearRingSelection()
        {
            selectedRing = null;
        }

        public Card GetCardSelection()
        {
            return selectedCard;
        }
        public Player GetCardOwner(Card ownedCard)
        {
            if (ownedCard != null)
            {
                foreach (Player player in game.Players)
                {
                    foreach (Card card in player.Hand)
                    {
                        if (card == selectedCard)
                        {
                            return player;
                        }
                    }
                }
            }
            return null;
        }
        public Monster GetMonsterSelection()
        {
            return selectedMonster;
        }
        public Ring GetRingSelection()
        {
            return selectedRing;
        }

        private void GameForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void panelRenderer1_MouseMove(object sender, MouseEventArgs e)
        {
            Monitor.Enter(Lock);
            Monster monster = panelRenderer1.GetMonsterAt(new Point(e.X, e.Y));
            Card card = panelRenderer1.GetCardAt(new Point(e.X, e.Y));
            if (monster != null)
            {
                tip.Show(monster.Name, panelRenderer1, new Point(e.X + 10, e.Y - 20));
            }
            else if (card != null)
            {
                tip.Show(card.Description, panelRenderer1, new Point(e.X + 10, e.Y - 20));
            }
            else
            {
                tip.Hide(panelRenderer1);
            }
            Monitor.Exit(Lock);
        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lock (input)
            {
                ClearCardSelection();
                ClearMonsterSelection();
                ClearRingSelection();

                game.EndGame();
                startGame = true;
                Monitor.Pulse(input);
            }
            lock (this)
            {
                Monitor.Pulse(this);
            }
        }

        private void numPlayersChanged(object sender, EventArgs e)
        {
            lock (input)
            {
                foreach (ToolStripMenuItem item in playersToolStripMenuItem.DropDownItems)
                {
                    item.Checked = false;
                }
                ((ToolStripMenuItem)sender).Checked = true;
                game.ChangePlayers(GetNumberPlayers());
                Monitor.Pulse(input);
            }
        }
    }
}
