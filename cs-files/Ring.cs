﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CastlePanic
{    
    public class Ring
    {
        private string myName;
        private List<Token> myTokens = new List<Token>();
        private Arc myArc;

        public Ring(Arc arc, string name)
        {
            myArc = arc;
            myName = name;
        }

        public Arc Arc
        {
            get { return myArc; }
        }

        public int Index
        {
            get { return new List<Ring>(myArc.Rings).IndexOf(this); }
        }

        public string Name
        {
            get { return myName; }
        }

        public Token[] Tokens
        {
            get { return myTokens.ToArray(); }
        }

        public void AddToken(Token token)
        {
            myTokens.Add(token);
        }

        public void RemoveToken(Token token)
        {
            myTokens.Remove(token);
        }

        public void Clear()
        {
            myTokens.Clear();
        }
    }
}
