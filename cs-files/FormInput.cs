﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace CastlePanic
{
    public class FormInput : Input
    {
        private GameForm myForm;

        public FormInput(GameForm form)
        {
            myForm = form;
        }

        public void AddMessage(string msg)
        {
            lock (this)
            {
                Monitor.Exit(myForm.Lock);
                myForm.AddMessage(msg);
                Monitor.Enter(myForm.Lock);
            }
        }

        public Card RequestCardToDiscard()
        {
            lock (this)
            {
                Monitor.Exit(myForm.Lock);

                myForm.ClearCardSelection();
                Monitor.Wait(this);
                Card card = myForm.GetCardSelection();
                while(card != null && myForm.GetCardOwner(card) != myForm.Game.CurrentPlayer)
                {
                    Monitor.Wait(this);
                    card = myForm.GetCardSelection();
                }

                Monitor.Enter(myForm.Lock);
                return card;                
            }            
        }

        public Trade RequestTrade()
        {
            lock (this)
            {
                Monitor.Exit(myForm.Lock);

                Monitor.Enter(myForm.Lock);
                return null;
            }
        }

        public Card RequestCardToPlay()
        {            
            lock (this)
            {
                Monitor.Exit(myForm.Lock);

                myForm.ClearCardSelection();
                Monitor.Wait(this);
                Card card = myForm.GetCardSelection();
                while (card != null && myForm.GetCardOwner(card) != myForm.Game.CurrentPlayer)
                {
                    Monitor.Wait(this);
                    card = myForm.GetCardSelection();
                }

                Monitor.Enter(myForm.Lock);
                return card;
            }
        }

        public Monster ChooseMonster()
        {            
            lock (this)
            {
                Monitor.Exit(myForm.Lock);

                myForm.ClearMonsterSelection();
                Monitor.Wait(this);
                Monster monster =  myForm.GetMonsterSelection();

                Monitor.Enter(myForm.Lock);
                return monster;
            }
        }

        public bool ChooseCastleAttack()
        {
            lock (this)
            {
                Monitor.Exit(myForm.Lock);

                DialogResult result =
                    MessageBox.Show("Use castle attack?", "Special", MessageBoxButtons.YesNo);

                bool yesno = false;
                if (result == DialogResult.Yes)
                {
                    yesno = true;
                }
                else
                {
                    yesno = false;
                }

                Monitor.Enter(myForm.Lock);
                return yesno;
            }
        }

        public Ring ChooseRing()
        {            
            lock (this)
            {
                Monitor.Exit(myForm.Lock);

                myForm.ClearRingSelection();
                Monitor.Wait(this);
                Ring ring = myForm.GetRingSelection();

                Monitor.Enter(myForm.Lock);
                return ring;
            }
        }

        public Card ChooseCardFromDiscard()
        {
            lock (this)
            {
                Monitor.Exit(myForm.Lock);

                DiscardForm form = new DiscardForm(myForm.Game);
                form.ShowDialog();
                Card card = form.SelectedCard;

                Monitor.Enter(myForm.Lock);
                return card;
            }
        }
    }
}
