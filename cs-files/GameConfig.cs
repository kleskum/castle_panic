﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace CastlePanic
{
    public class GameConfig
    {
        public Color[] ArcColors
        {
            get
            {
                return new Color[] {
                    Color.Red,
                    Color.Green,
                    Color.Blue
                };
            }
        }

        public int ArcsPerColor
        {
            get { return 2; }
        }

        public string[] RingNames
        {
            get
            {
                return new string[] {
                    "Forest",
                    "Archers",
                    "Knights",
                    "Swordsmen",
                    "Castle"
                };
            }
        }

        public string[] WallMaterials
        {
            get
            {
                return new string[] {
                    "Brick",
                    "Mortar"
                };
            }
        }

        public string[] MonsterNames
        {
            get
            {
                return new string[] {
                    "Goblin",
                    "Orc",
                    "Troll"
                };
            }
        }

        public string[] StartMonsters
        {
            get
            {
                string[] names = MonsterNames;
                int[] counts = new int[names.Length];

                int total = ArcColors.Length * ArcsPerColor;
                int i = 0;
                while (total > 0 && i < counts.Length)
                {
                    counts[i] = (int)Math.Ceiling(total / 2.0);
                    total -= counts[i];
                    i++;
                }

                total = ArcColors.Length * ArcsPerColor;
                List<string> monsters = new List<string>();
                for (int j = 0; j < counts.Length; j++)
                {
                    if (counts[j] > 0)
                    {
                        monsters.Add(names[j]);
                        counts[j]--;
                    }
                    for (int k = j + 1; k < counts.Length; k++)
                    {
                        if (counts[k] > 0)
                        {
                            monsters.Add(names[k]);
                            counts[k]--;
                            j = -1;
                            k = counts.Length;
                        }
                    }
                }

                return monsters.ToArray();
            }
        }

        public int MonsterDrawCount
        {
            get { return 2; }
        }

        public int DieSides
        {
            get { return ArcColors.Length * ArcsPerColor; }
        }

        public int GetMaxCardsPerPlayer(int numPlayers)
        {
            int maxCards = 0;
            switch (numPlayers)
            {
                case 1:
                case 2:
                    maxCards = 6;
                    break;
                case 3:
                case 4:
                case 5:
                    maxCards = 5;
                    break;
                case 6:
                    maxCards = 4;
                    break;
            }
            return maxCards;
        }

        public int GetAllowedDiscards(int numPlayers)
        {
            int numDiscards = 1;
            if (numPlayers == 1)
            {
                numDiscards = 2;
            }
            return numDiscards;
        }

        public void AddCards(Game game)
        {
            string[] ringNames = RingNames;
            string[] noForest =
                new List<string>(ringNames).GetRange(1, ringNames.Length - 1).ToArray();
            string[] typeNames =
                new List<string>(noForest).GetRange(0, noForest.Length - 1).ToArray();
            
            Color[] arcColors = ArcColors;

            string[] wallMaterials = WallMaterials;

            // attack cards
            foreach (Color color in arcColors)
            {
                foreach (string type in typeNames)
                {
                    AddCards(game.Deck, new AttackCard(type, color), 3);
                }
                AddCards(game.Deck, new AttackCard(typeNames, color), 1);
            }
            foreach (string type in typeNames)
            {
                AddCards(game.Deck, new AttackCard(type, arcColors), 1);
            }
            
            // material cards
            foreach (string matName in wallMaterials)
            {
                AddCards(game.Deck, new MaterialCard(matName), 4);
            }

            // special attacks
            AddCards(game.Deck, new BarbarianCard(noForest, arcColors), 1);
            AddCards(game.Deck, new DriveHimBackCard(noForest, arcColors), 1);
            AddCards(game.Deck, new TarCard(ringNames, arcColors), 1);

            // specials
            AddCards(game.Deck, new FortifyWallCard(), 1);
            AddCards(game.Deck, new DrawCardsCard(2), 1);
            AddCards(game.Deck, new NiceShotCard(), 1);
            AddCards(game.Deck, new MissingCard(), 1);
            AddCards(game.Deck, new ScavengeCard(), 1);
        }

        private void AddCards(Deck deck, Card card, int count)
        {
            for (int i = 0; i < count; i++)
            {
                deck.PushOnDraw(card.Clone());
            }
        }

        public void AddMonsterTokens(Game game)
        {
            string[] monsterNames = MonsterNames;
            string goblin = monsterNames[0];
            string orc = monsterNames[1];
            string troll = monsterNames[2];

            Color[] arcColors = ArcColors;

            string[] ringNames = RingNames;
            string[] typeNames =
                new List<string>(ringNames).GetRange(1, ringNames.Length - 2).ToArray();

            // monsters
            AddMonsterTokens(game.Bag, new Monster(goblin, 1), 6);
            AddMonsterTokens(game.Bag, new Monster(orc, 2), 11);
            AddMonsterTokens(game.Bag, new Monster(troll, 3), 10);

            // color moves
            foreach (Color color in arcColors)
            {
                AddMonsterTokens(game.Bag, new ColorMoveEffect(color), 2);
            }

            // arc moves
            AddMonsterTokens(game.Bag, new ArcMoveEffect(true), 1);
            AddMonsterTokens(game.Bag, new ArcMoveEffect(false), 1);

            // plagues
            foreach (string type in typeNames)
            {
                AddMonsterTokens(game.Bag, new PlagueEffect(type), 1);
            }

            // discard
            AddMonsterTokens(game.Bag, new DiscardEffect(1), 1);

            // draw monsters
            AddMonsterTokens(game.Bag, new DrawMonsterEffect(3), 1);
            AddMonsterTokens(game.Bag, new DrawMonsterEffect(4), 1);

            // boulders
            AddMonsterTokens(game.Bag, new BoulderEffect(), 4);

            // boss monsters
            AddMonsterTokens(game.Bag, new GoblinKing(), 1);
            AddMonsterTokens(game.Bag, new OrcWarlord(), 1);
            AddMonsterTokens(game.Bag, new TrollMage(), 1);
            AddMonsterTokens(game.Bag, new Healer(), 1);
        }

        private void AddMonsterTokens(Bag bag, Token token, int count)
        {
            for (int i = 0; i < count; i++)
            {
                bag.AddToken(token.Clone());
            }
        }
    }
}
